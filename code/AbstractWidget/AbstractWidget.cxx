#include "AbstractWidget.h"
#include "MainWindow.h"

AbstractWidget::AbstractWidget(QWidget *parent) :QWidget(parent)
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	m_thread = NULL;
    m_busying = false;
}

AbstractWidget::~AbstractWidget()
{
}

void AbstractWidget::SetBusying(bool b)
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	//mainWnd->SetThreadIsBusy(b);
	m_busying = b;
}

int AbstractWidget::DisplayMsgBox(QString msg, QMessageBox::Icon icon, QList<QMessageBox::StandardButton> buttonList)
{
	QMessageBox msgBox;

	msgBox.setWindowTitle("MainWindow");
	msgBox.setIcon(icon);
	msgBox.setText(msg);
	msgBox.setStyleSheet("QMessageBox{background-color:rgb(22,22,22);}\n QLabel {color:rgb(255,255,255);}\n QPushButton {background-color: rgb(22,22,22); color:white; border-color:rgb(63,63,70); border-style: outset; border-width: 1px; min-width: 8em; padding: 3px;} QPushButton::hover{background-color: rgb(0,102,204);}");

	for (int i = 0; i < buttonList.size(); i++)
		msgBox.addButton(buttonList.at(i));

	return msgBox.exec();
}

//bool AbstractWidget::GetBusying()
//{
//	MainWindow* mainWnd = MainWindow::GetMainWindow();
//	//return mainWnd->GetThreadIsBusy();
//}



