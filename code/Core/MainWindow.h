#ifndef MAINWINDOW_H
#define MAINWINDOW_H
 
#include <QMainWindow>
#include <QCloseEvent>
#include <QMessageBox>
#include <QDesktopServices>
#include <QUrl>
#include <vtkSmartPointer.h>
#include <vtkDICOMImageReader.h>
#include <vtkRenderer.h>
#include "vtkMarchingCubes.h"
#include "vtkPolyDataConnectivityFilter.h"
#include "vtkPolyDataMapper.h"
#include <vtkRenderWindow.h>
#include "vtkActor.h"
#include "vtkProperty.h"
#include "../AbstractData/ImageData.h"
#include "../AbstractData/Mesh.h"
#include "../Filters/SurfaceCreator.h"
#include "../Filters/MeshToImage.h"
#include "../Core/MyViewer.h"
#include "Planner.h"
#include "../Widget/Clipping/ClippingWidget.h"

class InteractorStyleSwitch;
class InteractorStyleSwitch3D;

// Forward Qt class declarations
class Ui_MainWindow;

struct Project
{
	Planner* m_planner;

	QString	m_projectPath;
	QString	m_meshPath;
	QString	m_imagePath;
	QString	m_labelPath;

	Image*	m_image; //Image
	Image*	m_label; //Label
	Mesh*	m_mesh;  //Mesh

	bool m_hasSaved;
};
 
class MainWindow : public QMainWindow
{
  Q_OBJECT

//#if VTK_MAJOR_VERSION > 5
//#define SetInput SetInputData 
//#define AddInput AddInputData 
//#endif


public:
 
  // Constructor/Destructor
  MainWindow();
  ~MainWindow();

  static MainWindow*	GetMainWindow();
  Planner*				GetPlanner();
  
  Ui_MainWindow*		GetUI();
  void					GetCursorPosition(double* pos);
  MyViewer*				GetViewers(int n);
  void					UpdateMesh(Mesh* mesh);

  virtual void closeEvent(QCloseEvent *event);
  int AskIfSaveProject();
  void ClearProject();
  bool AskYesNoCancelQuestion(QString msg, QMessageBox::StandardButton defaultBtn);

  void initializeViewers();
  //void slotTransformMode();

  void RenderAllViewer();

  void ResetAllViewerCameraClippingRange();
  void ResetAllViewer();
  
  //Synchronize image
  void AlignAllImages();

  //Brush property
  BrushProperty GetBrushProperty();

  //Dir
  QString    GetToolsDir();
  QString    GetTempDir();
  QString    GetUsefulDir();

public slots:
	//Basic
	void slotNew();
	void slotOpen();
	void slotPreview();
	void slotUpdate();
	void slotSaveProject(); 
	void slotSaveProject(QString dir);
	void slotSaveXML(QString path);
	void slotSaveImage();
	void slotSaveImage(QString path);
	void slotSaveLabel();
	void slotSaveLabel(QString path);
	void slotSaveMesh();
	void slotSaveMesh(QString path);
	void slotExit();
	bool slotClose();

	void slotClip();
	void slotRemesh();

	void slotChangeCursorPosition();
	void slotChangeCursorPosition(double * xyz);
	void slotChangeCursorPosition(double x, double y, double z, int anchor=-1);
	
	void slotNavigationMode();
	void slotWindowLevelMode();
	void slotPaintBrushMode();
	void slotPolygonMode();

	void slotSagittal();
	void slotCoronal();
	void slotAxial();
	void slotThreeD();
	void slotFourViews();

	virtual void resizeEvent(QResizeEvent * event);

private:
  // Designer form
  Ui_MainWindow *	ui;

  //Visualization
  MyViewer*			m_viewer[4];
  
  //Widget
  ClippingWidget*	m_clippingWidget;
  
  Project			m_project;
  QString			m_toolsDir;
  QString			m_tempDir;
  QString			m_usefulDir;

  //Utilities
  vtkMatrix4x4* m_imageObliqueMatrix;

  BrushProperty m_brushProperty;

};
 
#endif
