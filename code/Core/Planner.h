#ifndef PLANNER_H
#define PLANNER_H

#include <QObject>
#include <QMultiMap>
#include <vtkPlane.h>
#include <vtkImageInterpolator.h>
#include "Define.h"
#include "../AbstractData/ImageData.h"
#include "../AbstractData/Mesh.h"
#include "../AbstractData/Landmark.h"
#include "../AbstractData/GuideTube.h"
#include "../AbstractData/IodineSeed.h"

class Planner : public QObject
{
    Q_OBJECT
public:
    explicit Planner(QObject *parent = 0);
	~Planner();

	void SetDirectory(QString dir);
	void SetPatientInfo(PatientInfo info);
	void SetCuttingPlane(vtkPlane* sagittalPlane,vtkPlane* coronalPlane,vtkPlane* axialPlane);
	void InitializeTemplateSeed();
	int  Read();
	int	 Read(QString xmlFilePath);
	int  Write();

	//Append
	void AppendImage(Image* image);
	void AppendMesh(Mesh* mesh);
	void AppendLandmark(Landmark* landmark);
	void AppendSeed(IodineSeed* seed);
	void AppendTube(GuideTube* tube);
	void AppendSeedInTube(GuideTube* tube, IodineSeed* seed);

	//Remove
	void RemoveImage(Image* image);
	void RemoveMesh(Mesh* mesh);
	void RemoveLandmark(Landmark* landmark);
	void RemoveSeed(IodineSeed* seed);
	void RemoveTube(GuideTube* tube);
	void RemoveSeedInTube(GuideTube* tube, IodineSeed* seed);

	//View
	void ImageDirectionOff();
	void ImageDirectionOn();

	//Interpolation
	void SetInterpolationToNearest();
	void SetInterpolationToCublic();

	//Get
	Image*				GetImageByUniqueName(QString name);
	Mesh*				GetMeshByUniqueName(QString name);
	Mesh*				GetLatestEffectiveMesh();
	Landmark*			GetLandmarkByUniqueName(QString name);
	IodineSeed*			GetSeedByUniqueName(QString name);
	GuideTube*			GetTubeByUniqueName(QString name);
	GuideTube*			GetTubeWhichSeedInside(IodineSeed* seed);
	QList<IodineSeed*>	GetSeedInTubeList(QString tubeName);
	QList<IodineSeed*>	GetSeedInTubeList(GuideTube* tube);
	IodineSeed*			GetTemplateSeed();
	ImageType::Pointer	GetTemplateSeedImage();

	QList<Image*>*		GetImageList();
	QList<Mesh*>*		GetMeshList();
	QList<Landmark*>*	GetLandmarkList();
	QList<IodineSeed*>*	GetSeedList();
	QList<IodineSeed*>	GetPlannedSeedList();
	QList<IodineSeed*>	GetActualSeedList();
	QList<GuideTube*>*	GetTubeList();
	QMultiMap<GuideTube*,IodineSeed*>* GetSeedInTubeMap();
	
	QString				GetDirectory();
	PatientInfo			GetPatientInfo();

	//bool
	bool				ContainsMesh(QString name);
    
signals:
    
public slots:
    
private: 
	//Data Management--------------------------
	QList<Image*>*		m_imageList;
	QList<Mesh*>*		m_meshList;
	QList<Landmark*>*	m_landmarkList;
	QList<IodineSeed*>* m_seedList;
	QList<GuideTube*>*	m_tubeList;
	PatientInfo			m_patientInfo;

	QMultiMap<GuideTube*, IodineSeed*>* m_seedInTubeMap;

	//Accessory
	vtkPlane*			m_plane[3];
	IodineSeed*			m_templateSeed;
	ImageType::Pointer	m_templateSeedImage;

	//Saving Directory
	QString		m_directory;
};

#endif // PLANNER_H
