#include <QtCore>
#include <QtXml/QDomDocument>
#include <QtXml/QDomElement>
#include "Planner.h"

Planner::Planner(QObject *parent) :
    QObject(parent)
{
	m_landmarkList	= new QList<Landmark*>;
	m_imageList		= new QList<Image*>;
	m_meshList		= new QList<Mesh*>;
	m_seedList		= new QList<IodineSeed*>;
	m_tubeList		= new QList<GuideTube*>;
	m_seedInTubeMap = new QMultiMap<GuideTube*, IodineSeed*>;
	
//#if _DEBUG
//	std::string templateSeedPath = "../../Project/resources/iodineSeed/seed_base_volume_80_5.nii.gz";
//#else
//	std::string templateSeedPath = "../../../Project/resources/iodineSeed/seed_base_volume_80_5.nii.gz";
}		

void Planner::InitializeTemplateSeed()
{
	//Create template Seed
	double coord[3] = { 0, 0, 0 };
	double bounds[3] = { 80, 80, 80 };
	double spacing[3] = { 0.5, 0.5, 0.5 };
	m_templateSeed = new IodineSeed;
	m_templateSeed->SetUniqueName("Template_Seed");
	m_templateSeed->SetBaseVolume(this->GetTemplateSeedImage());
	m_templateSeed->SetBounds(bounds);
	m_templateSeed->SetSpacing(spacing);
	m_templateSeed->SetSeedActivity(1);
	m_templateSeed->SetSeedLength(3);
	m_templateSeed->SetShape(2);
	m_templateSeed->SetCutFunctions(m_plane[0], m_plane[1], m_plane[2]);
	m_templateSeed->Create();
	m_templateSeed->SetPosition(coord);
	m_templateSeed->UpdateSeed();
	m_templateSeed->Update();
}

Planner::~Planner()
{
	int imagecount = m_imageList->size();
	for(int i=0;i<imagecount;i++) 
	{
		delete m_imageList->at(0);
		m_imageList->removeFirst();
	}
	delete m_imageList;

	int meshcount = m_meshList->size();
	for(int i=0;i<meshcount;i++) 
	{
		delete m_meshList->at(0);
		m_meshList->removeFirst();
	}
	delete m_meshList;		
	
	int landmarkcount = m_landmarkList->size();
	for (int i = 0; i<landmarkcount; i++)
	{
		delete m_landmarkList->at(0);
		m_landmarkList->removeFirst();
	}
	delete m_landmarkList;

	int seedcount = m_seedList->size();
	for (int i = 0; i<seedcount; i++)
	{
		delete m_seedList->at(0);
		m_seedList->removeFirst();
	}
	delete m_seedList;

	int tubecount = m_tubeList->size();
	for (int i = 0; i<tubecount; i++)
	{
		delete m_tubeList->at(0);
		m_tubeList->removeFirst();
	}
	delete m_tubeList;

	delete m_seedInTubeMap;
}

void Planner::SetCuttingPlane(vtkPlane* sagittalPlane,vtkPlane* coronalPlane,vtkPlane* axialPlane)
{
	m_plane[0] = sagittalPlane;
	m_plane[1] = coronalPlane;
	m_plane[2] = axialPlane;
}

void Planner::SetDirectory(QString dir)
{
	m_directory = dir;
}

QString Planner::GetDirectory()
{
	return m_directory;
}

void Planner::SetPatientInfo(PatientInfo info)
{
	m_patientInfo = info;
}

PatientInfo Planner::GetPatientInfo()
{
	return m_patientInfo;
}

bool Planner::ContainsMesh(QString name)
{
	return m_meshList->contains(GetMeshByUniqueName(name));
}

int Planner::Read(QString xmlFilePath)
{
	QFile xmlFile(xmlFilePath);
	qDebug()<<xmlFilePath;
	
	if(!xmlFile.exists()) 
	{
		qDebug()<<"File not exist";
		return 1;
	}

	QDomDocument document;
	if (!xmlFile.open(QIODevice::ReadOnly|QIODevice::Text))
	{
		qDebug()<<"Error 2";
		return 2;
	}
	else
	{
		QString errorStr;
		int errorLine;
		int errorColumn;

		if (!document.setContent(&xmlFile, false, &errorStr, &errorLine, &errorColumn))
		{
			qDebug() << errorStr <<" "<< errorLine <<" "<<errorColumn;
			return 3;
		}
		xmlFile.close();
	}

	//Read Patient Info
	QDomNode infoElement		= document.elementsByTagName("Patient").at(0);
	m_patientInfo.name			= infoElement.toElement().attribute("Name");
	m_patientInfo.sex			= infoElement.toElement().attribute("Sex");
	m_patientInfo.age			= infoElement.toElement().attribute("Age");
	m_patientInfo.id			= infoElement.toElement().attribute("ID");
	m_patientInfo.description	= infoElement.toElement().attribute("Description");
	m_patientInfo.referenceDose	= infoElement.toElement().attribute("ReferenceDose").toDouble();

	qDebug()<<"Patient Info";
	qDebug()<<m_patientInfo.name;
	qDebug()<<m_patientInfo.sex;
	qDebug()<<m_patientInfo.age;
	qDebug()<<m_patientInfo.id;
	qDebug()<<m_patientInfo.description;

	//Read Image
	QDomNodeList imageElementList = document.elementsByTagName("Image");
	for (int i=0;i<imageElementList.count();i++)
	{
		QDomElement imageElement = imageElementList.at(i).toElement();
		Image* image = new Image;
		image->SetUniqueName(imageElement.attribute("UniqueName"));
		image->SetImage(imageElement.attribute("Path"));
		image->SetLUT(imageElement.attribute("LUT").toInt());
		image->SetWindow(imageElement.attribute("Window").toFloat());
		image->SetLevel(imageElement.attribute("Level").toFloat());
		image->SetVisibility(imageElement.attribute("Visibility").toFloat());
		image->SetOpacity(imageElement.attribute("Opacity").toFloat());
		image->GetReslicer()->GetReslicer(0)->GetProperty()->SetInterpolationType(imageElement.attribute("Interpolation").toInt());
		image->GetReslicer()->GetReslicer(1)->GetProperty()->SetInterpolationType(imageElement.attribute("Interpolation").toInt());
		image->GetReslicer()->GetReslicer(2)->GetProperty()->SetInterpolationType(imageElement.attribute("Interpolation").toInt());

		// Read the user matrix
		double userMatrix[16];
		string Test = imageElement.attribute("UserMatrix").toStdString();
		sscanf_s(imageElement.attribute("UserMatrix").toStdString().c_str(), "%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf",
			&userMatrix[0], &userMatrix[1], &userMatrix[2], &userMatrix[3],
			&userMatrix[4], &userMatrix[5], &userMatrix[6], &userMatrix[7],
			&userMatrix[8], &userMatrix[9], &userMatrix[10], &userMatrix[11]);
		userMatrix[12] = 0;
		userMatrix[13] = 0;
		userMatrix[14] = 0;
		userMatrix[15] = 1;

		vtkTransform* temp = vtkTransform::New();
		temp->GetMatrix()->DeepCopy(userMatrix);

		image->GetReslicer()->GetAdditionalTransform()->SetInput(temp);
		//image->GetReslicer()->GetMeshTransform()->Print(cout);
		this->AppendImage(image);
	}

	//ReadMesh

	QDomNodeList meshElementList = document.elementsByTagName("Mesh");
	for (int i=0;i<meshElementList.count();i++)
	{
		QDomElement meshElement = meshElementList.at(i).toElement();
		Mesh* mesh = new Mesh;
		mesh->SetUniqueName(meshElement.attribute("UniqueName"));
		mesh->SetPolyData(meshElement.attribute("Path"));
		mesh->SetRepresentation(meshElement.attribute("Representation").toInt());
		mesh->SetOpacity(meshElement.attribute("Opacity").toFloat());
		mesh->SetVisibility(meshElement.attribute("Visibility").toFloat());
		mesh->SetColor(meshElement.attribute("Color_R").toFloat(), meshElement.attribute("Color_G").toFloat(), meshElement.attribute("Color_B").toFloat());


		if (meshElement.attribute("RefImage") != "") {
			Image* refImage = this->GetImageByUniqueName(meshElement.attribute("RefImage"));
			if (refImage != NULL) {
				//refImage->GetReslicer()->GetMeshTransform()->Print(cout);
				mesh->GetActor()->SetUserTransform(refImage->GetReslicer()->GetMeshTransform());
				mesh->SetReferenceImageUniqueName(refImage->GetUniqueName());
			}
		}
		this->AppendMesh(mesh);
	}

	//Read Landmark
	QDomNodeList landmarkElementList = document.elementsByTagName("Landmark");
	for (int i=0;i<landmarkElementList.count();i++)
	{
		QDomElement landmarkElement = landmarkElementList.at(i).toElement();
		double pos[3] = { landmarkElement.attribute("Pos_X").toFloat(), landmarkElement.attribute("Pos_Y").toFloat(), landmarkElement.attribute("Pos_Z").toFloat() };
		Landmark* landmark = new Landmark;
		landmark->SetShape(1);
		landmark->SetUniqueName(landmarkElement.attribute("UniqueName"));
		landmark->SetCutFunctions(m_plane[0], m_plane[1], m_plane[2]);
		landmark->Create();
		landmark->SetRadius(landmarkElement.attribute("Radius").toFloat());
		landmark->SetVisibility(landmarkElement.attribute("Visibility").toInt());
		landmark->SetOpacity(landmarkElement.attribute("Opacity").toFloat());
		landmark->SetPosition(pos);
		landmark->SetColor(landmarkElement.attribute("Color_R").toFloat(), landmarkElement.attribute("Color_G").toFloat(), landmarkElement.attribute("Color_B").toFloat());
		landmark->SetPickable(false);


		this->AppendLandmark(landmark);
	}

	//Read GuideTube
	QDomNodeList tubeElementList = document.elementsByTagName("Tube");
	for (int i = 0; i < tubeElementList.count(); i++)
	{
		QDomElement tubeElement = tubeElementList.at(i).toElement();
		double startPos[3] = { tubeElement.attribute("StartPos_X").toFloat(), tubeElement.attribute("StartPos_Y").toFloat(), tubeElement.attribute("StartPos_Z").toFloat() };
		double endPos[3] = { tubeElement.attribute("EndPos_X").toFloat(), tubeElement.attribute("EndPos_Y").toFloat(), tubeElement.attribute("EndPos_Z").toFloat() };
		GuideTube* tube = new GuideTube;
		tube->SetUniqueName(tubeElement.attribute("UniqueName"));
		tube->SetCutFunctions(m_plane[0], m_plane[1], m_plane[2]);
		tube->Create(startPos, endPos);
		tube->SetRadius(tubeElement.attribute("Radius").toFloat());
		tube->SetVisibility(tubeElement.attribute("Visibility").toInt());
		tube->SetOpacity(tubeElement.attribute("Opacity").toFloat());
		tube->SetColor(tubeElement.attribute("Color_R").toFloat(), tubeElement.attribute("Color_G").toFloat(), tubeElement.attribute("Color_B").toFloat());
		tube->GetActor()->SetPickable(false);

		this->AppendTube(tube);
	}

	//Read Seed
	QDomNodeList seedElementList = document.elementsByTagName("Seed");
	for (int i = 0; i<seedElementList.count(); i++)
	{

		QDomElement seedElement = seedElementList.at(i).toElement();
		
		double pos[3] = { seedElement.attribute("Pos_X").toFloat(), seedElement.attribute("Pos_Y").toFloat(), seedElement.attribute("Pos_Z").toFloat() };
		double ori[3] = { seedElement.attribute("Ori_X").toFloat(), seedElement.attribute("Ori_Y").toFloat(), seedElement.attribute("Ori_Z").toFloat() };
		IodineSeed* seed = new IodineSeed();
		seed->SetUniqueName(seedElement.attribute("UniqueName"));
		seed->SetBaseVolume(m_templateSeed->GetBaseVolume());
		seed->SetShape(2);
		seed->SetCutFunctions(m_plane[0], m_plane[1], m_plane[2]);
		seed->Create();
		seed->Copy(m_templateSeed);
		seed->SetPosition(pos);
		seed->SetOrientation(ori);
		seed->SetColor(seedElement.attribute("Color_R").toFloat(), seedElement.attribute("Color_G").toFloat(), seedElement.attribute("Color_B").toFloat());
		seed->Update();
		seed->UpdateSeed();

		this->AppendSeed(seed);
		if (seedElement.attribute("UniqueName").at(0) == 'P') {
			this->AppendSeedInTube(this->GetTubeByUniqueName(seedElement.attribute("InWhichTube")), seed);
			seed->GetActor()->SetPickable(false); // not pickable only for actual seeds.
		}
	}

	return 0;
}

int Planner::Read()
{
	if (m_directory == "") return 1;

	QString path = m_directory + "/Plan.xml";

	return this->Read(path);

}

int Planner::Write()
{
	if (m_directory == "") return 1;

	QDomDocument document;
	
	QDomElement planRoot = document.createElement("Brachytherapy_Plan");
	document.appendChild(planRoot);
	
	//Write Patient Info into XML
	QDomElement infoRoot = document.createElement("Patient_Information");
	planRoot.appendChild(infoRoot);
	
	QDomElement infoElement = document.createElement("Patient");
	infoElement.setAttribute("Name",m_patientInfo.name);
	infoElement.setAttribute("Sex",m_patientInfo.sex);
	infoElement.setAttribute("Age",m_patientInfo.age);
	infoElement.setAttribute("ID",m_patientInfo.id);
	infoElement.setAttribute("Description",m_patientInfo.description);
	infoElement.setAttribute("ReferenceDose", m_patientInfo.referenceDose);
	infoRoot.appendChild(infoElement);

	//Write Image into XML
	QDomElement imageRoot = document.createElement("Image_Collection");
	planRoot.appendChild(imageRoot);

	for (int i=0; i<m_imageList->size();i++)
	{
		Image* image = m_imageList->at(i);		
		QString path = m_directory + "/" + image->GetUniqueName() + ".nii.gz";
		QDomElement imageElement = document.createElement("Image");
		imageElement.setAttribute("UniqueName", image->GetUniqueName());
		imageElement.setAttribute("Window", image->GetWindow());
		imageElement.setAttribute("Level", image->GetLevel());
		imageElement.setAttribute("LUT", image->GetLUT());
		imageElement.setAttribute("Visibility",image->GetVisibility());
		imageElement.setAttribute("Opacity",image->GetOpacity());
		imageElement.setAttribute("Interpolation", image->GetReslicer()->GetReslicer(0)->GetProperty()->GetInterpolationType());
		imageElement.setAttribute("Path", path);

		vtkMatrix4x4* additionalMatrix = image->GetTransform()->GetMatrix();
		
		// Record the user matrix due to additional transform
		char userMatrix[400];
		sprintf_s(userMatrix, "%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f",
			additionalMatrix->GetElement(0, 0), additionalMatrix->GetElement(0, 1), additionalMatrix->GetElement(0, 2), additionalMatrix->GetElement(0, 3),
			additionalMatrix->GetElement(1, 0), additionalMatrix->GetElement(1, 1), additionalMatrix->GetElement(1, 2), additionalMatrix->GetElement(1, 3),
			additionalMatrix->GetElement(2, 0), additionalMatrix->GetElement(2, 1), additionalMatrix->GetElement(2, 2), additionalMatrix->GetElement(2, 3)
			);

		QString qUserMatrix = QString::fromStdString(userMatrix);
		imageElement.setAttribute("UserMatrix", qUserMatrix);

		imageRoot.appendChild(imageElement);
	}

	//Write Mesh into XML
	QDomElement meshRoot = document.createElement("Mesh_Collection");
	planRoot.appendChild(meshRoot);

	for (int i=0; i<m_meshList->size();i++)
	{
		Mesh* mesh = m_meshList->at(i);		
		QDomElement meshElement = document.createElement("Mesh");
		QString path = m_directory + "/" + mesh->GetUniqueName() + ".stl";
		meshElement.setAttribute("UniqueName", mesh->GetUniqueName());
		meshElement.setAttribute("Representation", mesh->GetRepresentation());
		meshElement.setAttribute("Color_R", QString::number(mesh->GetColor()[0]));
		meshElement.setAttribute("Color_G", QString::number(mesh->GetColor()[1]));
		meshElement.setAttribute("Color_B", QString::number(mesh->GetColor()[2]));
		meshElement.setAttribute("Opacity", mesh->GetOpacity());
		meshElement.setAttribute("RefImage", mesh->GetReferenceImageUniqueName());
		meshElement.setAttribute("Visibility",mesh->GetVisibility());
		meshElement.setAttribute("Path", path);
		meshRoot.appendChild(meshElement);
	}

	//Write Landmark into XML
	QDomElement landmarkRoot = document.createElement("Landmark_Collection");
	planRoot.appendChild(landmarkRoot);

	for (int i=0; i<m_landmarkList->size();i++)
	{
		Landmark* landmark = m_landmarkList->at(i);	
		QDomElement landmarkElement = document.createElement("Landmark");
		landmarkElement.setAttribute("UniqueName", landmark->GetUniqueName());
		landmarkElement.setAttribute("Pos_X", QString::number(landmark->GetPosition()[0]));
		landmarkElement.setAttribute("Pos_Y", QString::number(landmark->GetPosition()[1]));
		landmarkElement.setAttribute("Pos_Z", QString::number(landmark->GetPosition()[2]));
		landmarkElement.setAttribute("Radius",QString::number(landmark->GetRadius()));
		landmarkElement.setAttribute("Visibility",QString::number(landmark->GetVisibility()));
		landmarkElement.setAttribute("Opacity",QString::number(landmark->GetOpacity()));
		landmarkElement.setAttribute("Color_R", QString::number(landmark->GetColor()[0]));
		landmarkElement.setAttribute("Color_G", QString::number(landmark->GetColor()[1]));
		landmarkElement.setAttribute("Color_B", QString::number(landmark->GetColor()[2]));
		landmarkRoot.appendChild(landmarkElement);
	}

	//Write Seed into XML
	QDomElement seedRoot = document.createElement("Seed_Collection");
	planRoot.appendChild(seedRoot);

	for (int i = 0; i<m_seedList->size(); i++)
	{
		IodineSeed* seed = m_seedList->at(i);
		QDomElement seedElement = document.createElement("Seed");
		seedElement.setAttribute("UniqueName", seed->GetUniqueName());
		seedElement.setAttribute("Pos_X", QString::number(seed->GetPosition()[0]));
		seedElement.setAttribute("Pos_Y", QString::number(seed->GetPosition()[1]));
		seedElement.setAttribute("Pos_Z", QString::number(seed->GetPosition()[2]));
		seedElement.setAttribute("Ori_X", QString::number(seed->GetOrientation()[0]));
		seedElement.setAttribute("Ori_Y", QString::number(seed->GetOrientation()[1]));
		seedElement.setAttribute("Ori_Z", QString::number(seed->GetOrientation()[2]));
		seedElement.setAttribute("Color_R", QString::number(seed->GetColor()[0]));
		seedElement.setAttribute("Color_G", QString::number(seed->GetColor()[1]));
		seedElement.setAttribute("Color_B", QString::number(seed->GetColor()[2]));

		if (seedElement.attribute("UniqueName").at(0) == 'P')
			seedElement.setAttribute("InWhichTube", this->GetTubeWhichSeedInside(seed)->GetUniqueName());
		seedRoot.appendChild(seedElement);
	}

	//Write GuideTube into XML
	QDomElement tubeRoot = document.createElement("Tube_Collection");
	planRoot.appendChild(tubeRoot);

	for (int i = 0; i<m_tubeList->size(); i++)
	{
		GuideTube* tube = m_tubeList->at(i);
		QDomElement tubeElement = document.createElement("Tube");
		tubeElement.setAttribute("UniqueName", tube->GetUniqueName());
		tubeElement.setAttribute("StartPos_X",	QString::number(tube->GetStartPosition()[0]));
		tubeElement.setAttribute("StartPos_Y",	QString::number(tube->GetStartPosition()[1]));
		tubeElement.setAttribute("StartPos_Z",	QString::number(tube->GetStartPosition()[2]));
		tubeElement.setAttribute("EndPos_X",	QString::number(tube->GetEndPosition()[0]));
		tubeElement.setAttribute("EndPos_Y",	QString::number(tube->GetEndPosition()[1]));
		tubeElement.setAttribute("EndPos_Z",	QString::number(tube->GetEndPosition()[2]));
		tubeElement.setAttribute("Radius",		QString::number(tube->GetRadius()));
		tubeElement.setAttribute("Visibility",	QString::number(tube->GetVisibility()));
		tubeElement.setAttribute("Opacity",		QString::number(tube->GetOpacity()));
		tubeElement.setAttribute("Color_R",		QString::number(tube->GetColor()[0]));
		tubeElement.setAttribute("Color_G",		QString::number(tube->GetColor()[1]));
		tubeElement.setAttribute("Color_B",		QString::number(tube->GetColor()[2]));
		tubeRoot.appendChild(tubeElement);
	}

	//Write
	QString xmlPath = m_directory + "/Plan.xml";
	QFile xmlFile(xmlPath);

	if(!xmlFile.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		return 2;
	}
	else
	{
		QTextStream stream (&xmlFile);
		stream <<document.toString();
		xmlFile.close();
	}


	//Save Image
	for (int i=0;i<m_imageList->size();i++)
	{
		Image* image = m_imageList->at(i);
		QString path = m_directory + "/" + image->GetUniqueName() + ".nii.gz";

		NiftiWriterType::Pointer writer = NiftiWriterType::New();
		writer->SetFileName(path.toStdString().c_str());
		writer->SetInput(m_imageList->at(i)->GetReslicer()->GetInputImage());
		writer->SetUseInputMetaDataDictionary(true);
		writer->UpdateLargestPossibleRegion();
		writer->Write();
		//image->SaveImageAsNifti(path);
	}

	//Save Mesh
	for (int i=0;i<m_meshList->size();i++)
	{
		Mesh* mesh = m_meshList->at(i);
		QString path = m_directory + "/" + mesh->GetUniqueName() + ".stl";

		mesh->SavePolyData(path);
	}

	return 0;
}

Image* Planner::GetImageByUniqueName( QString name )
{
	for (int i=0;i<m_imageList->size();i++)
	{
		Image* image = m_imageList->at(i);

		if(image->GetUniqueName()==name)
			return image;
	}

	return NULL;
}

Mesh* Planner::GetMeshByUniqueName( QString name )
{
	for (int i=0;i<m_meshList->size();i++)
	{
		Mesh* mesh = m_meshList->at(i);

		if(mesh->GetUniqueName()==name)
			return mesh;
	}

	return NULL;
}

Mesh* Planner::GetLatestEffectiveMesh()
{
	// mesh that represent the data surface 
	if (m_meshList->size() == 0)
	{
		return NULL;
	}
	for (int i = m_meshList->size() - 1; i >= 0; i--)
	{
		Mesh* mesh = m_meshList->at(i);
		if (mesh->GetUniqueName() != "Centerline" && mesh->GetUniqueName() != "Capped" && mesh->GetUniqueName() != "DrillHoleCenterline"
			&& mesh->GetUniqueName() != "SplinePoints" && mesh->GetUniqueName() != "DrillHoleRegion"
			&& mesh->GetUniqueName() != "OpenCastPoints" && mesh->GetUniqueName() != "OpenCastPoints2"
			&& mesh->GetUniqueName() != "OpenedTube1" && mesh->GetUniqueName() != "OpenedTube2" && mesh->GetUniqueName() != "OpenedTube3")
		{
			if (mesh->GetUniqueName() == "Flare")
				mesh->GetActor()->SetPickable(0);
			qDebug() << "Get Mesh Name:"<< mesh->GetUniqueName();
			return mesh;
		}
	}
	return NULL;
}

Landmark* Planner::GetLandmarkByUniqueName( QString name )
{
	for (int i=0;i<m_landmarkList->size();i++)
	{
		Landmark* landmark = m_landmarkList->at(i);

		if(landmark->GetUniqueName()==name)
			return landmark;
	}

	return NULL;
}

IodineSeed* Planner::GetSeedByUniqueName(QString name)
{
	for (int i = 0; i<m_seedList->size(); i++)
	{
		IodineSeed* seed = m_seedList->at(i);

		if (seed->GetUniqueName() == name)
			return seed;
	}

	return NULL;
}

GuideTube* Planner::GetTubeByUniqueName(QString name)
{
	for (int i = 0; i<m_tubeList->size(); i++)
	{
		GuideTube* tube = m_tubeList->at(i);

		if (tube->GetUniqueName() == name)
			return tube;
	}

	return NULL;
}

IodineSeed* Planner::GetTemplateSeed()
{
	return m_templateSeed;
}

QList<Image*>* Planner::GetImageList()
{
	return m_imageList;
}

QList<Mesh*>* Planner::GetMeshList()
{
	return m_meshList;
}

QList<Landmark*>* Planner::GetLandmarkList()
{
	return m_landmarkList;
}

QList<IodineSeed*>* Planner::GetSeedList()
{
	return m_seedList;
}

QList<IodineSeed*> Planner::GetPlannedSeedList()
{
	QList<IodineSeed*> plannedSeedList;

	for (int i = 0; i < m_seedList->size(); i++)
	{
		if (m_seedList->at(i)->GetUniqueName().at(0) == 'P')
			plannedSeedList.append(m_seedList->at(i));
	}

	return plannedSeedList;
}

QList<IodineSeed*> Planner::GetActualSeedList()
{
	QList<IodineSeed*> plannedSeedList;

	for (int i = 0; i < m_seedList->size(); i++)
	{
		if (m_seedList->at(i)->GetUniqueName().at(0) == 'A')
			plannedSeedList.append(m_seedList->at(i));
	}

	return plannedSeedList;
}

QList<GuideTube*>* Planner::GetTubeList()
{
	return m_tubeList;
}

GuideTube* Planner::GetTubeWhichSeedInside(IodineSeed* seed)
{
	return m_seedInTubeMap->key(seed);
}

QMultiMap<GuideTube*, IodineSeed*>* Planner::GetSeedInTubeMap()
{
	return m_seedInTubeMap;
}

void Planner::AppendImage(Image* image)
{
	if (m_imageList->size() == 0 && image != NULL) {
		m_imageList->append(image);
	}
	else if (m_imageList->last() == this->GetImageByUniqueName("Label")) {
		m_imageList->insert(m_imageList->size() - 1, image);
	}
	else
		m_imageList->append(image);
}

void Planner::AppendMesh(Mesh* mesh)
{
	m_meshList->append(mesh);
}

void Planner::AppendLandmark(Landmark* landmark)
{
	m_landmarkList->append(landmark);
}

void Planner::AppendSeed(IodineSeed* seed)
{
	m_seedList->append(seed);
}

void Planner::AppendTube(GuideTube* tube)
{
	m_tubeList->append(tube);
}

void Planner::AppendSeedInTube(GuideTube* tube, IodineSeed* seed)
{
	m_seedInTubeMap->insert(tube, seed);
}

void Planner::RemoveImage(Image* image)
{
	m_imageList->removeOne(image);
	delete image;
}

void Planner::RemoveMesh(Mesh* mesh)
{
	m_meshList->removeOne(mesh);
	delete mesh;
}

void Planner::RemoveLandmark(Landmark* landmark)
{
	m_landmarkList->removeOne(landmark);
}

void Planner::RemoveSeed(IodineSeed* seed)
{
	//Remove seed from tube list
	GuideTube* tube = m_seedInTubeMap->key(seed);
	m_seedInTubeMap->remove(tube, seed);

	m_seedList->removeOne(seed);
	delete seed;
}

void Planner::RemoveTube(GuideTube* tube)
{
	//Remove seed in tube list
	while (this->GetSeedInTubeList(tube).size() > 0)
	{
		qDebug() << this->GetSeedInTubeList(tube).size();
		this->RemoveSeed(this->GetSeedInTubeList(tube).at(0));
	}
		

	m_tubeList->removeOne(tube);
	delete tube;
}

void Planner::RemoveSeedInTube(GuideTube* tube, IodineSeed* seed)
{
	m_seedInTubeMap->remove(tube,seed);
}

void Planner::ImageDirectionOff()
{
	Image* baseImage = this->GetImageByUniqueName("CT");
	
	// Test if image exist
	if (!baseImage) 
		baseImage = this->GetImageByUniqueName("MRI");
	if (!baseImage) 
		baseImage = this->GetImageByUniqueName("CT+MRI");
	if (!baseImage)
		// if these three images not found, return
		return;

	baseImage->GetReslicer()->DirectionOff();
	for (int i = 0; i < this->GetImageList()->size(); i++)
		if (this->GetImageList()->at(i) != baseImage) {
			this->GetImageList()->at(i)->GetReslicer()->GetObliqueTransform()->GetMatrix()->DeepCopy(baseImage->GetObliqueTransform()->GetMatrix());
			this->GetImageList()->at(i)->GetReslicer()->Update();
		}
}

void Planner::ImageDirectionOn()
{
	Image* baseImage = this->GetImageByUniqueName("CT");

	// Test if image exist
	if (!baseImage)
		baseImage = this->GetImageByUniqueName("MRI");
	if (!baseImage)
		baseImage = this->GetImageByUniqueName("CT+MRI");
	if (!baseImage)
		// if these three images not found, return
		return;

	baseImage->GetReslicer()->DirectionOn();
	for (int i = 0; i < this->GetImageList()->size(); i++)
		if (this->GetImageList()->at(i) != baseImage) {
			this->GetImageList()->at(i)->GetReslicer()->GetObliqueTransform()->Identity();
			this->GetImageList()->at(i)->GetReslicer()->Update();
		}
}

void Planner::SetInterpolationToNearest()
{
	for (int i = 0; i < this->m_imageList->size(); i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (m_imageList->at(i) != this->GetImageByUniqueName("Label"))
				m_imageList->at(i)->GetReslicer()->GetReslicers()[j]->GetProperty()->SetInterpolationTypeToNearest();
		}
	}
}

void Planner::SetInterpolationToCublic()
{
	for (int i = 0; i < this->m_imageList->size(); i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (m_imageList->at(i) != this->GetImageByUniqueName("Label"))
				m_imageList->at(i)->GetReslicer()->GetReslicers()[j]->GetProperty()->SetInterpolationTypeToCubic();
		}
	}
}

QList<IodineSeed*> Planner::GetSeedInTubeList(GuideTube* tube)
{
	return m_seedInTubeMap->values(tube);
}

QList<IodineSeed*> Planner::GetSeedInTubeList(QString tubeName)
{
	GuideTube* tube = this->GetTubeByUniqueName(tubeName);
	return this->GetSeedInTubeList(tube);
}

ImageType::Pointer Planner::GetTemplateSeedImage()
{
	return m_templateSeedImage;
}
