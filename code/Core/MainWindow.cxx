#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QFileDialog>
#include <QDir>
#include <QDebug>
#include <QDomDocument>
#include <QDomElement>
#include <QMessageBox>
#include <QLayout>
#include "vtkSmartPointer.h"
#include "vtkSTLWriter.h"
#include "vtkXMLPolyDataWriter.h"
#include "vtkPolyData.h"
#include "vtkCellData.h"
#include "vtkPointData.h"
#include "ImageData.h"
#include <vtkvmtkPolyDataSurfaceRemeshing.h>

#include "InteractorStyleSwitch.h"
#include "InteractorStyleSwitch3D.h"

MainWindow::MainWindow()
{
  this->ui = new Ui_MainWindow;
  this->ui->setupUi(this);

  m_project.m_planner	= NULL;
  m_project.m_imagePath	= "";
  m_project.m_labelPath	= "";
  m_project.m_meshPath	= "";
  m_project.m_image		= NULL;
  m_project.m_mesh		= NULL;
  m_project.m_label		= NULL;
  m_project.m_hasSaved	= true;

  m_brushProperty.labelID = 1;
  m_brushProperty.shape = 0;
  m_brushProperty.size = 10;
  m_brushProperty.isVolumetric = false;
  
  //Utilities
  m_imageObliqueMatrix = vtkMatrix4x4::New();
  m_imageObliqueMatrix->Identity();

  ui->CursorXDoubleSpinBox->setVisible(false);
  ui->CursorYDoubleSpinBox->setVisible(false);
  ui->CursorZDoubleSpinBox->setVisible(false);
  ui->label_6->setVisible(false);
  ui->label_7->setVisible(false);
  ui->label_8->setVisible(false);

  // Set up action signals and slots
  connect(ui->actionNew,			SIGNAL(triggered()), this, SLOT(slotNew()));
  connect(ui->actionOpen,			SIGNAL(triggered()), this, SLOT(slotOpen()));
  connect(ui->actionClose,			SIGNAL(triggered()), this, SLOT(slotClose()));
  connect(ui->actionExit,			SIGNAL(triggered()), this, SLOT(slotExit()));
  connect(ui->actionSaveProject,	SIGNAL(triggered()), this, SLOT(slotSaveProject()));
  connect(ui->actionSaveImage,		SIGNAL(triggered()), this, SLOT(slotSaveImage()));
  connect(ui->actionSaveMesh,		SIGNAL(triggered()), this, SLOT(slotSaveMesh()));
  connect(ui->actionSaveLabel,		SIGNAL(triggered()), this, SLOT(slotSaveLabel()));
  connect(ui->previewBtn,			SIGNAL(clicked()),	 this, SLOT(slotPreview()));
  connect(ui->updateBtn,			SIGNAL(clicked()),	 this, SLOT(slotUpdate()));
  
  connect(ui->actionClip,			SIGNAL(triggered()), this, SLOT(slotClip()));
  connect(ui->actionRemesh,			SIGNAL(triggered()), this, SLOT(slotRemesh()));

  //Cursor Position
  connect(ui->CursorXDoubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(slotChangeCursorPosition()), Qt::QueuedConnection);
  connect(ui->CursorYDoubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(slotChangeCursorPosition()), Qt::QueuedConnection);
  connect(ui->CursorZDoubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(slotChangeCursorPosition()), Qt::QueuedConnection);

  //Interaction
  connect(ui->actionNavigation,		SIGNAL(triggered()),	this, SLOT(slotNavigationMode()));
  connect(ui->actionWindowLevel,	SIGNAL(triggered()),	this, SLOT(slotWindowLevelMode()));
  connect(ui->actionPaintBrush,		SIGNAL(triggered()),	this, SLOT(slotPaintBrushMode()));
  connect(ui->actionPolygon,		SIGNAL(triggered()),	this, SLOT(slotPolygonMode()));
 
  //Viewer
  connect(ui->actionSagittal,	SIGNAL(triggered()), this, SLOT(slotSagittal()));
  connect(ui->actionCoronal,	SIGNAL(triggered()), this, SLOT(slotCoronal()));
  connect(ui->actionAxial,		SIGNAL(triggered()), this, SLOT(slotAxial()));
  connect(ui->actionThreeD,		SIGNAL(triggered()), this, SLOT(slotThreeD()));
  connect(ui->actionFourViews,	SIGNAL(triggered()), this, SLOT(slotFourViews()));

  m_clippingWidget = new ClippingWidget;
  //ui->moduleWidget->setWidget(m_clippingWidget);

  m_project.m_planner = new Planner;
  this->initializeViewers();
  m_project.m_planner->SetCuttingPlane(m_viewer[0]->GetPlane(), m_viewer[1]->GetPlane(), m_viewer[2]->GetPlane());
}
 
MainWindow::~MainWindow()
{
	this->ClearProject();
}

MainWindow* MainWindow::GetMainWindow()
{
	foreach(QWidget* w, QApplication::topLevelWidgets())
	{
		if (w->objectName() == "MainWindow")
		{
			return (MainWindow*)w;
		}
	}
	return NULL;
}

Planner* MainWindow::GetPlanner()
{
	return m_project.m_planner;
}

Ui_MainWindow* MainWindow::GetUI()
{
	return ui;
}

void MainWindow::GetCursorPosition(double* pos)
{
	pos[0] = ui->CursorXDoubleSpinBox->value();
	pos[1] = ui->CursorYDoubleSpinBox->value();
	pos[2] = ui->CursorZDoubleSpinBox->value();
}

MyViewer* MainWindow::GetViewers(int n)
{
	return m_viewer[n];
}

void MainWindow::UpdateMesh(Mesh* mesh)
{
	m_project.m_planner->AppendMesh(mesh);
}

void MainWindow::slotChangeCursorPosition()
{
	//QCoreApplication::processEvents();
	double pos[3];
	pos[0] = ui->CursorXDoubleSpinBox->value();
	pos[1] = ui->CursorYDoubleSpinBox->value();
	pos[2] = ui->CursorZDoubleSpinBox->value();

	for (int i = 0; i < 4; i++)
		m_viewer[i]->SetCursorPosition(pos);

	for (int i = 0; i < m_project.m_planner->GetImageList()->size(); i++)
		m_project.m_planner->GetImageList()->at(i)->SetPosition(pos);

	for (int i = 0; i < m_project.m_planner->GetLandmarkList()->size(); i++)
		m_project.m_planner->GetLandmarkList()->at(i)->Update();

	for (int i = 0; i < m_project.m_planner->GetTubeList()->size(); i++)
		m_project.m_planner->GetTubeList()->at(i)->Update();

	this->RenderAllViewer();
}

void MainWindow::slotChangeCursorPosition(double* xyz)
{
	this->slotChangeCursorPosition(xyz[0], xyz[1], xyz[2]);
}

void MainWindow::slotChangeCursorPosition(double x, double y, double z, int anchor)
{
	disconnect(ui->CursorXDoubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(slotChangeCursorPosition()));
	disconnect(ui->CursorYDoubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(slotChangeCursorPosition()));
	disconnect(ui->CursorZDoubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(slotChangeCursorPosition()));

	ui->CursorXDoubleSpinBox->setValue(x);
	ui->CursorYDoubleSpinBox->setValue(y);
	ui->CursorZDoubleSpinBox->setValue(z);

	double pos[3];
	pos[0] = x;
	pos[1] = y;
	pos[2] = z;

	for (int i = 0; i < 4; i++)
		m_viewer[i]->SetCursorPosition(pos);

	for (int i = 0; i < m_project.m_planner->GetImageList()->size(); i++)
		m_project.m_planner->GetImageList()->at(i)->SetPosition(pos, anchor);

	for (int i = 0; i < m_project.m_planner->GetLandmarkList()->size(); i++)
		m_project.m_planner->GetLandmarkList()->at(i)->Update();

	for (int i = 0; i < m_project.m_planner->GetTubeList()->size(); i++)
		m_project.m_planner->GetTubeList()->at(i)->Update();

	this->RenderAllViewer();

	connect(ui->CursorXDoubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(slotChangeCursorPosition()), Qt::QueuedConnection);
	connect(ui->CursorYDoubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(slotChangeCursorPosition()), Qt::QueuedConnection);
	connect(ui->CursorZDoubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(slotChangeCursorPosition()), Qt::QueuedConnection);

}

void MainWindow::slotNavigationMode()
{
	ui->actionNavigation->setChecked(true);
	ui->actionWindowLevel->setChecked(false);
	ui->actionPaintBrush->setChecked(false);
	ui->actionPolygon->setChecked(false);
	ui->actionTransform->setChecked(false);
	ui->actionSmartPolygon->setChecked(false);

	for (int i = 0; i < 3 ; i++)
	{
		m_viewer[i]->GetMyStyle()->SetInteractorStyleToNavigation();
	}
}

void MainWindow::slotWindowLevelMode()
{
	ui->actionWindowLevel->setChecked(true);
	ui->actionNavigation->setChecked(false);
	ui->actionPaintBrush->setChecked(false);
	ui->actionPolygon->setChecked(false);
	ui->actionTransform->setChecked(false);
	ui->actionSmartPolygon->setChecked(false);

	for (int i = 0; i < 3; i++)
	{
		m_viewer[i]->GetMyStyle()->SetInteractorStyleToWindowLevel();
	}
}

void MainWindow::slotPaintBrushMode()
{
	ui->actionPaintBrush->setChecked(true);
	ui->actionNavigation->setChecked(false);
	ui->actionWindowLevel->setChecked(false);
	ui->actionPolygon->setChecked(false);
	ui->actionTransform->setChecked(false);
	ui->actionSmartPolygon->setChecked(false);

	for (int i = 0; i < 3 ; i++)
	{
		m_viewer[i]->GetMyStyle()->SetInteractorStyleToPaintBrush();
	}
}

void MainWindow::slotPolygonMode()
{
	ui->actionPolygon->setChecked(true);
	ui->actionPaintBrush->setChecked(false);
	ui->actionNavigation->setChecked(false);
	ui->actionWindowLevel->setChecked(false);
	ui->actionTransform->setChecked(false);
	ui->actionSmartPolygon->setChecked(false);

	for (int i = 0; i < 3; i++)
	{
		m_viewer[i]->GetMyStyle()->SetInteractorStyleToPolygonDraw();
	}
}

void MainWindow::ResetAllViewer()
{
	m_imageObliqueMatrix->Identity();
	for (int i = 0; i < 4; i++)
	{
		m_viewer[i]->ResetCamera();
		m_viewer[i]->Render();
	}
}

void MainWindow::AlignAllImages()
{
	for (int i = 0; i < m_project.m_planner->GetImageList()->size(); i++)
	{
		double pos[3];
		pos[0] = ui->CursorXDoubleSpinBox->value();
		pos[1] = ui->CursorYDoubleSpinBox->value();
		pos[2] = ui->CursorZDoubleSpinBox->value();
		m_project.m_planner->GetImageList()->at(i)->SetPosition(pos);
		m_project.m_planner->GetImageList()->at(i)->GetReslicer()->GetObliqueTransform()->SetMatrix(m_imageObliqueMatrix);
	}
	this->RenderAllViewer();

}

BrushProperty MainWindow::GetBrushProperty()
{
	return m_brushProperty;
}

void MainWindow::ResetAllViewerCameraClippingRange()
{
	for (int i = 0; i < 4; i++)
	{
		m_viewer[i]->GetDataRenderer()->ResetCamera();
		m_viewer[i]->GetDataRenderer()->ResetCameraClippingRange();
		m_viewer[i]->GetAnnotationRenderer()->ResetCameraClippingRange();
		m_viewer[i]->Render();
	}
}

void MainWindow::RenderAllViewer()
{
	for (int i = 0; i < 4; i++)
		m_viewer[i]->Render();
}

QString MainWindow::GetToolsDir()
{
	return m_toolsDir;
}

QString MainWindow::GetTempDir()
{
	return m_tempDir;
}

QString MainWindow::GetUsefulDir()
{
	return m_usefulDir;
}

void MainWindow::slotExit()
{
	if(!this->slotClose())
		qApp->exit(); 
}

int MainWindow::AskIfSaveProject()
{
	//QMessageBox::StandardButton reply = QMessageBox::question(this, QMainWindow::windowTitle(), 
	//                              tr("Would you like to save the project before quit?"),
	//                              QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);

	QMessageBox msgBox;
	msgBox.setWindowTitle(QMainWindow::windowTitle());
	msgBox.setIcon(QMessageBox::Question);
	msgBox.setText(tr("Would you like to save the project before quit?"));
	msgBox.addButton(QMessageBox::Yes);
	msgBox.addButton(QMessageBox::No);
	msgBox.addButton(QMessageBox::Cancel);

	int reply = msgBox.exec();

	if (reply == QMessageBox::Yes)
	{
		this->slotSaveProject();
	}
	return reply;
}

void MainWindow::ClearProject()
{
	if (m_project.m_mesh)
	{
		m_viewer[3]->RemoveActor(m_project.m_mesh->GetActor(),true);
		m_project.m_planner->RemoveMesh(m_project.m_mesh);
		m_project.m_mesh = NULL;
	}

	if (m_project.m_image)
	{
		m_viewer[0]->RemoveViewProp(m_project.m_image->GetImageViewProp(0), true);
		m_viewer[1]->RemoveViewProp(m_project.m_image->GetImageViewProp(1), true);
		m_viewer[2]->RemoveViewProp(m_project.m_image->GetImageViewProp(2), true);

		m_project.m_planner->RemoveImage(m_project.m_image);
		m_project.m_image = NULL;
	}

	if (m_project.m_label)
	{
		m_viewer[0]->RemoveViewProp(m_project.m_label->GetImageViewProp(0), true);
		m_viewer[1]->RemoveViewProp(m_project.m_label->GetImageViewProp(1), true);
		m_viewer[2]->RemoveViewProp(m_project.m_label->GetImageViewProp(2), true);

		m_project.m_planner->RemoveImage(m_project.m_label);
		m_project.m_label = NULL;
	}

	m_project.m_imagePath = "";
	m_project.m_meshPath = "";
	m_project.m_labelPath = "";

	m_project.m_hasSaved = true;

	delete m_viewer[0];
	delete m_viewer[1];
	delete m_viewer[2];
	delete m_viewer[3];
	
	m_project.m_planner = new Planner;
	this->initializeViewers();
	m_project.m_planner->SetCuttingPlane(m_viewer[0]->GetPlane(), m_viewer[1]->GetPlane(), m_viewer[2]->GetPlane());

}

bool MainWindow::AskYesNoCancelQuestion(QString msg, QMessageBox::StandardButton defaultBtn)
{
	// return 0: Continue; 1: Discard; 2: Cancel
	QMessageBox msgBox;
	msgBox.setWindowTitle("Warning");
	msgBox.setIcon(QMessageBox::Warning);
	msgBox.setText(msg);
	msgBox.setStyleSheet("QMessageBox{background-color:rgb(22,22,22);}\n QLabel {color:rgb(255,255,255);}\n QPushButton {background-color: rgb(22,22,22); color:white; border-color:rgb(63,63,70); border-style: outset; border-width: 1px; min-width: 8em; padding: 3px;} QPushButton::hover{background-color: rgb(0,102,204);}");
	msgBox.addButton(QMessageBox::Yes);			//0
	msgBox.addButton(QMessageBox::No);		//1
	msgBox.addButton(QMessageBox::Cancel);
	msgBox.setDefaultButton(defaultBtn);

	int ret = msgBox.exec();
	if (ret == QMessageBox::Cancel)
	{
		return 2;
	}
	if (ret == QMessageBox::Yes)		//Add on for clipping
		return 0;

	if (ret == QMessageBox::No)		//Redo all steps
		return 1;
}

void MainWindow::initializeViewers()
{
	for (int i = 0; i < 4; i++)
		m_viewer[i] = new MyViewer;

	try {
		ui->sagittalView->SetRenderWindow(m_viewer[0]->GetRenderWindow());
		ui->coronalView->SetRenderWindow(m_viewer[1]->GetRenderWindow());
		ui->axialView->SetRenderWindow(m_viewer[2]->GetRenderWindow());
		ui->threeDView->SetRenderWindow(m_viewer[3]->GetRenderWindow());
	}
	catch (...) {
		cerr << "Initialize viewer failed";
	}

	for (int i = 0; i < 4; i++)
	{
		m_viewer[i]->SetOrientation(i);
		m_viewer[i]->Initialize();
		m_viewer[i]->GetMyStyle()->SetViewers(m_viewer);
		m_viewer[i]->GetMyStyle()->SetInteractorStyleToNavigation();
		m_viewer[i]->GetMyStyle3D()->SetViewers(m_viewer);
		m_viewer[i]->GetMyStyle3D()->SetInteractorStyleTo3DNavigation();
	}
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	if (m_project.m_hasSaved)
	{
		event->accept();
	}
	else
	{
		int reply = this->AskIfSaveProject();
		if (reply == QMessageBox::Cancel)
		{
			event->ignore();
			return;
		}
	}
}

bool MainWindow::slotClose()
{
	if (m_project.m_hasSaved == false)
	{
		if (this->AskIfSaveProject() == QMessageBox::Cancel)
			return 1;
	}

	this->ClearProject();
	return 0;
}

void MainWindow::slotClip()
{
	ui->moduleWidget->setWidget(m_clippingWidget);
	
	//m_project.m_planner->AppendMesh(m_project.m_mesh);
}

void MainWindow::slotRemesh()
{
	std::cout << "To remeshing.";
	double TargetEdgeLength = 1;
	double targetArea = 0.25 * sqrt(3.0) * pow(TargetEdgeLength, 2); //1/2 * sqrt(3)/2 * sin(60 deg)
	if (m_project.m_mesh)
	{
		m_viewer[3]->RemoveActor(m_project.m_mesh->GetActor(), true);
	}

	vtkSmartPointer<vtkvmtkPolyDataSurfaceRemeshing> remesher = vtkSmartPointer<vtkvmtkPolyDataSurfaceRemeshing>::New();
	remesher->SetInputData(m_project.m_mesh->GetPolyData());
	remesher->SetElementSizeModeToTargetArea();
	remesher->SetTargetArea(targetArea);
	remesher->SetTargetAreaFactor(1.0);
	remesher->SetTriangleSplitFactor(5.0);
	remesher->SetMaxArea(1E16);
	remesher->SetMinArea(0.0);
	remesher->SetNumberOfIterations(10);
	remesher->SetNumberOfConnectivityOptimizationIterations(20);
	remesher->SetRelaxation(0.5);
	remesher->SetMinAreaFactor(0.5);
	remesher->SetAspectRatioThreshold(1.2);
	remesher->SetInternalAngleTolerance(0.0);
	remesher->SetNormalAngleTolerance(0.2);
	remesher->SetCollapseAngleThreshold(0.2);
	remesher->SetPreserveBoundaryEdges(0);
	remesher->Update();

	m_project.m_mesh->GetPolyData()->DeepCopy(remesher->GetOutput());

	m_viewer[3]->AddActor(m_project.m_mesh->GetActor(), true);
}

void MainWindow::slotNew()
{
	if (this->slotClose())
		return;

	QStringList pathList = QFileDialog::getOpenFileNames(this, tr("Open File"));

	if (pathList.size() == 0)
		return;		
	cout << "path List size: "<<pathList.size();
	qDebug() << "Path: " << pathList;
	m_project.m_image = new Image;
	m_project.m_image->SetUniqueName("CT");			//or MRI //originally: Image

	if(pathList.size()==1)
		m_project.m_image->SetImage(pathList.at(0));
	else
	{
		m_project.m_image->SetImage(&pathList);
	}
	
	m_project.m_planner->AppendImage(m_project.m_image);

	m_viewer[0]->AddViewProp(m_project.m_image->GetImageViewProp(0), true);
	m_viewer[1]->AddViewProp(m_project.m_image->GetImageViewProp(1), true);
	m_viewer[2]->AddViewProp(m_project.m_image->GetImageViewProp(2), true);

	this->slotPreview();

	//Set first image as cursor position
	double pos[3];
	m_project.m_planner->GetImageList()->at(0)->GetPosition(pos);
	ui->CursorXDoubleSpinBox->setValue(pos[0]);
	ui->CursorYDoubleSpinBox->setValue(pos[1]);
	ui->CursorZDoubleSpinBox->setValue(pos[2]);
	this->AlignAllImages();

	//Set Viewer Camera
	//for (int i = 0; i < 3; i++)
	//  m_viewer[i]->SetCameraMatrix(m_planner->GetImageList()->at(0)->GetMatrix());


	//Set orientation
	m_viewer[0]->InitializeOrientationText();
	m_viewer[1]->InitializeOrientationText();
	m_viewer[2]->InitializeOrientationText();
	m_viewer[3]->createOrientationMarker("m");

	//m_viewer[3]->create3DInteractionButton();

	for (int i = 0; i < 4; i++)
	{
		m_viewer[i]->ResetCamera();

		if (i < 3)
			m_viewer[i]->GetMyStyle()->SetInteractorStyleToNavigation();
		else
			m_viewer[i]->GetMyStyle3D()->SetInteractorStyleTo3DNavigation();
	}

	this->ResetAllViewerCameraClippingRange();
	this->RenderAllViewer();
}

void MainWindow::slotOpen()
{
	if (this->slotClose())
		return;

	QString fileName = QFileDialog::getOpenFileName(this, tr("Open Project"),
		"/home",
		tr("Project (*.xml)"));

	QFile xmlFile(fileName);

	if (!xmlFile.exists())
	{
		qDebug()<<"Error 1";
		return;
	}

	QDomDocument document;
	if (!xmlFile.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		qDebug()<<"Error 2";
		return;
	}
	else
	{
		QString errorStr;
		int errorLine;
		int errorColumn;

		if (!document.setContent(&xmlFile, false, &errorStr, &errorLine, &errorColumn))
		{
			qDebug() << errorStr << " " << errorLine << " " << errorColumn;
			return ;
		}
		xmlFile.close();
	}
	//Read Path
	QDomNode infoElement	= document.elementsByTagName("Path").at(0);
	m_project.m_imagePath	= infoElement.toElement().attribute("CT");
	m_project.m_labelPath	= infoElement.toElement().attribute("Label");
	m_project.m_meshPath	= infoElement.toElement().attribute("Mesh");
	m_project.m_projectPath = fileName;

	qDebug() << "ImagePath: " << m_project.m_imagePath;
	qDebug() << "LabelPath: " << m_project.m_labelPath;
	qDebug() << "MeshPath: " << m_project.m_meshPath;

	m_project.m_image = new Image;
	m_project.m_image->SetUniqueName("CT");
	m_project.m_image->SetImage(m_project.m_imagePath);

	m_project.m_label = new Image;
	m_project.m_label->SetUniqueName("Label");
	m_project.m_label->SetImage(m_project.m_labelPath);	
	
	m_project.m_mesh = new Mesh;
	m_project.m_mesh->SetUniqueName("Mesh");
	m_project.m_mesh->SetPolyData(m_project.m_meshPath);
	m_project.m_mesh->Update();

	m_project.m_planner->AppendImage(m_project.m_image);
	m_project.m_planner->AppendImage(m_project.m_label);
	m_project.m_planner->AppendMesh(m_project.m_mesh);

	//Visualize
	m_viewer[0]->AddViewProp(m_project.m_image->GetImageViewProp(0), true);
	m_viewer[1]->AddViewProp(m_project.m_image->GetImageViewProp(1), true);
	m_viewer[2]->AddViewProp(m_project.m_image->GetImageViewProp(2), true);

	m_viewer[0]->AddViewProp(m_project.m_label->GetImageViewProp(0), true);
	m_viewer[1]->AddViewProp(m_project.m_label->GetImageViewProp(1), true);
	m_viewer[2]->AddViewProp(m_project.m_label->GetImageViewProp(2), true);

	m_viewer[3]->AddActor(m_project.m_mesh->GetActor(), true);

	this->ui->threeDView->GetRenderWindow()->Render();

	//Set first image as cursor position
	double pos[3];
	m_project.m_planner->GetImageList()->at(0)->GetPosition(pos);
	ui->CursorXDoubleSpinBox->setValue(pos[0]);
	ui->CursorYDoubleSpinBox->setValue(pos[1]);
	ui->CursorZDoubleSpinBox->setValue(pos[2]);
	this->AlignAllImages();

	//Set Viewer Camera
	//for (int i = 0; i < 3; i++)
	//  m_viewer[i]->SetCameraMatrix(m_planner->GetImageList()->at(0)->GetMatrix());


	//Set orientation
	m_viewer[0]->InitializeOrientationText();
	m_viewer[1]->InitializeOrientationText();
	m_viewer[2]->InitializeOrientationText();
	m_viewer[3]->createOrientationMarker("m");

	//m_viewer[3]->create3DInteractionButton();

	for (int i = 0; i < 4; i++)
	{
		m_viewer[i]->ResetCamera();

		if (i < 3)
			m_viewer[i]->GetMyStyle()->SetInteractorStyleToNavigation();
		else
			m_viewer[i]->GetMyStyle3D()->SetInteractorStyleTo3DNavigation();
	}
	this->ResetAllViewerCameraClippingRange();
	this->RenderAllViewer();

}

void MainWindow::slotPreview()
{
	if (m_project.m_image == NULL)
		return;

	SurfaceCreator surfaceCreator;
	surfaceCreator.SetInputData(m_project.m_image->GetImageData());
	surfaceCreator.SetMarchingCubesValue(ui->valueSpinBox->value());
	//surfaceCreator.SetResamplingFactor(0.5);
	surfaceCreator.SetSmootherPassBand(ui->spinboxPassBand->value());
	surfaceCreator.SetSmootherIteration(ui->spinBoxIterations->value());
	surfaceCreator.SetSmootherAngle(ui->spinboxFeatureAngle->value());
	surfaceCreator.SetSpecificId(ui->spinboxConnection->value());
	surfaceCreator.Update();

	MeshToImage meshToImage;
	meshToImage.SetInputData(surfaceCreator.GetOutput());
	meshToImage.SetReferenceImage(m_project.m_image->GetItkImage());
	meshToImage.SetImageInsideValue(1);
	meshToImage.Update();

	//Mesh
	if (m_project.m_mesh)
	{
		m_viewer[3]->RemoveActor(m_project.m_mesh->GetActor(), true);
		m_project.m_planner->RemoveMesh(m_project.m_mesh);
		m_project.m_mesh = NULL;
	}
	
	m_project.m_mesh = new Mesh;
	m_project.m_mesh->SetUniqueName("Mesh");
	m_project.m_mesh->SetPolyData(surfaceCreator.GetOutput());
	m_project.m_mesh->Update();

	//Label
	if (m_project.m_label)
	{
		m_viewer[0]->RemoveViewProp(m_project.m_label->GetImageViewProp(0), true);
		m_viewer[1]->RemoveViewProp(m_project.m_label->GetImageViewProp(1), true);
		m_viewer[2]->RemoveViewProp(m_project.m_label->GetImageViewProp(2), true);
		m_project.m_planner->RemoveImage(m_project.m_label);
		m_project.m_label = NULL;
	}

	m_project.m_label = new Image;
	m_project.m_label->SetUniqueName("Label");
	m_project.m_label->SetImage(meshToImage.GetOutput());
	m_project.m_label->SetOpacity(0.3);
	m_project.m_label->SetLUT(5);
	m_project.m_label->SetWindow(3);
	m_project.m_label->SetLevel(1.5);

	m_project.m_label->GetReslicer()->GetReslicer(0)->GetProperty()->SetInterpolationTypeToNearest();
	m_project.m_label->GetReslicer()->GetReslicer(1)->GetProperty()->SetInterpolationTypeToNearest();
	m_project.m_label->GetReslicer()->GetReslicer(2)->GetProperty()->SetInterpolationTypeToNearest();
	
	m_project.m_planner->AppendImage(m_project.m_image);
	m_project.m_planner->AppendImage(m_project.m_label);
	m_project.m_planner->AppendMesh(m_project.m_mesh);

	//// Lock the label image's transform to its base image;
	//m_project.m_label->GetReslicer()->GetResliceTransform()->Identity();
	//m_project.m_label->GetReslicer()->GetResliceTransform()->Concatenate(m_project.m_image->GetReslicer()->GetResliceTransform());
	//m_project.m_label->GetReslicer()->GetMeshTransform()->Identity();
	//m_project.m_label->GetReslicer()->GetMeshTransform()->Concatenate(m_project.m_image->GetReslicer()->GetMeshTransform());

	this->AlignAllImages();

	//Visualize
	m_viewer[0]->AddViewProp(m_project.m_label->GetImageViewProp(0), true);
	m_viewer[1]->AddViewProp(m_project.m_label->GetImageViewProp(1), true);
	m_viewer[2]->AddViewProp(m_project.m_label->GetImageViewProp(2), true);
	m_viewer[3]->AddActor(m_project.m_mesh->GetActor(), true);
	this->ui->threeDView->GetRenderWindow()->Render();

	this->ResetAllViewerCameraClippingRange();

	m_project.m_hasSaved = false;
}

void MainWindow::slotUpdate()
{
	if (m_project.m_image == NULL)
		return;

	//Mesh
	if (m_project.m_mesh)
	{
		m_viewer[3]->RemoveActor(m_project.m_mesh->GetActor(), true);
		m_project.m_planner->RemoveMesh(m_project.m_mesh);
		m_project.m_mesh = NULL;
	}

	SurfaceCreator surfaceCreator;
	surfaceCreator.SetInputData(m_project.m_label->GetImageData());
	surfaceCreator.SetMarchingCubesValue(1);				//label only 1 and 0
	//surfaceCreator.SetResamplingFactor(0.5);
	surfaceCreator.SetSmootherPassBand(ui->spinboxPassBand->value());
	surfaceCreator.SetSmootherIteration(ui->spinBoxIterations->value());
	surfaceCreator.SetSmootherAngle(ui->spinboxFeatureAngle->value());
	surfaceCreator.SetSpecificId(ui->spinboxConnection->value());
	surfaceCreator.Update();

	m_project.m_mesh = new Mesh;
	m_project.m_mesh->SetUniqueName("Mesh");
	m_project.m_mesh->SetPolyData(surfaceCreator.GetOutput());
	//surfaceCreator.GetOutput()->Print(std::cout);
	m_project.m_mesh->Update();

	m_viewer[3]->AddActor(m_project.m_mesh->GetActor());
	//this->ui->threeDView->GetRenderWindow()->Render();

	this->ResetAllViewerCameraClippingRange();

	m_project.m_hasSaved = false;
}

void MainWindow::slotSaveProject()
{
	QString projectDir = QFileDialog::getExistingDirectory(this, tr("Save Project"));
	
	if (projectDir == "") return;
	this->slotSaveProject(projectDir);
}

void MainWindow::slotSaveProject(QString dir)
{
	m_project.m_projectPath = dir + "/Project.xml";
	m_project.m_imagePath	= dir + "/Image.nii.gz";
	m_project.m_meshPath	= dir + "/Mesh.stl";
	m_project.m_labelPath	= dir + "/Label.nii.gz";

	this->slotSaveImage(m_project.m_imagePath);
	this->slotSaveMesh(m_project.m_meshPath);
	this->slotSaveLabel(m_project.m_labelPath);
	this->slotSaveXML(m_project.m_projectPath);

	QDesktopServices::openUrl(QUrl(m_project.m_projectPath));
	m_project.m_hasSaved = true;
}

void MainWindow::slotSaveXML(QString path)
{
	//Create a file for easy opening analysis
	QDomDocument document;

	QDomElement xmlRoot = document.createElement("MarchingCube");		//
	document.appendChild(xmlRoot);

	//Write Patient Info into XML
	QDomElement infoRoot = document.createElement("Project");
	xmlRoot.appendChild(infoRoot);

	QDomElement infoElement = document.createElement("Path");
	infoElement.setAttribute("Image",  m_project.m_imagePath);
	infoElement.setAttribute("Label", m_project.m_labelPath);
	infoElement.setAttribute("Mesh", m_project.m_meshPath);
	infoRoot.appendChild(infoElement);

	//Write
	QFile xmlFile(path);

	if (!xmlFile.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		return;
	}
	else
	{
		QTextStream stream(&xmlFile);
		stream << document.toString();
		xmlFile.close();
	}

	m_project.m_projectPath = path;
}

void MainWindow::slotSaveMesh()
{
	if (m_project.m_mesh == NULL) return;

	QString fileName = QFileDialog::getSaveFileName(this, tr("Save Mesh"),
		QDir::currentPath() + "/Mesh" + QString::number(ui->valueSpinBox->value()),
		tr("STereoLithography (*.stl);; VTK (*.vtp)"));

	if (fileName == "")
		return;
	
	this->slotSaveMesh(fileName);
}

void MainWindow::slotSaveMesh(QString path)
{
	if (path == "")				  return;
	if (m_project.m_mesh == NULL) return;

	m_project.m_mesh->SavePolyData(path);
}

void MainWindow::slotSaveImage()
{
	if (m_project.m_image == NULL)	return;

	QString niiFilePath = QFileDialog::getSaveFileName(this, tr("Save Image"),
		QDir::currentPath() + "/image" + QString::number(ui->valueSpinBox->value()) + ".nii.gz",
		tr("Nifti (*.nii.gz)"));

	if (niiFilePath == "")
		return;

	this->slotSaveImage(niiFilePath);
}

void MainWindow::slotSaveImage(QString path)
{
	if (path == "")					return;
	if (m_project.m_image == NULL)	return;

	m_project.m_image->SaveImageAsNifti(path);
}

void MainWindow::slotSaveLabel()
{
	if (m_project.m_label == NULL)	return;

	QString niiFilePath = QFileDialog::getSaveFileName(this, tr("Save Label"),
		QDir::currentPath() + "/label" + QString::number(ui->valueSpinBox->value()) + ".nii.gz",
		tr("Nifti (*.nii.gz)"));

	if (niiFilePath == "")
		return;

	this->slotSaveLabel(niiFilePath);
}

void MainWindow::slotSaveLabel(QString path)
{
	if (path == "")					return;
	if (m_project.m_label == NULL)	return;
	
	m_project.m_label->SaveImageAsNifti(path);
}

void MainWindow::slotSagittal()
{
	ui->viewGridLayout->removeWidget(ui->axialFrame);
	ui->viewGridLayout->removeWidget(ui->sagittalFrame);
	ui->viewGridLayout->removeWidget(ui->coronalFrame);
	ui->viewGridLayout->removeWidget(ui->threeDFrame);

	ui->axialFrame->setHidden(true);
	ui->coronalFrame->setHidden(true);
	ui->threeDFrame->setHidden(true);
	ui->sagittalFrame->setHidden(true);

	ui->viewGridLayout->addWidget(ui->sagittalFrame);
	ui->sagittalFrame->setHidden(false);

	//Update GUI
	ui->actionSagittal->setChecked(true);
	ui->actionCoronal->setChecked(false);
	ui->actionAxial->setChecked(false);
	ui->actionThreeD->setChecked(false);
	ui->actionFourViews->setChecked(false);

	for (int i = 0; i < 4; i++)
		m_viewer[i]->ResizeViewer();

	//this->RenderAllViewer();
}

void MainWindow::slotCoronal()
{
	ui->viewGridLayout->removeWidget(ui->axialFrame);
	ui->viewGridLayout->removeWidget(ui->sagittalFrame);
	ui->viewGridLayout->removeWidget(ui->coronalFrame);
	ui->viewGridLayout->removeWidget(ui->threeDFrame);

	ui->axialFrame->setHidden(true);
	ui->coronalFrame->setHidden(true);
	ui->threeDFrame->setHidden(true);
	ui->sagittalFrame->setHidden(true);

	ui->viewGridLayout->addWidget(ui->coronalFrame);
	ui->coronalFrame->setHidden(false);

	//Update GUI
	ui->actionSagittal->setChecked(false);
	ui->actionCoronal->setChecked(true);
	ui->actionAxial->setChecked(false);
	ui->actionThreeD->setChecked(false);
	ui->actionFourViews->setChecked(false);

	for (int i = 0; i < 4; i++)
		m_viewer[i]->ResizeViewer();

	//this->RenderAllViewer();
}

void MainWindow::slotAxial()
{
	ui->viewGridLayout->removeWidget(ui->axialFrame);
	ui->viewGridLayout->removeWidget(ui->sagittalFrame);
	ui->viewGridLayout->removeWidget(ui->coronalFrame);
	ui->viewGridLayout->removeWidget(ui->threeDFrame);

	ui->axialFrame->setHidden(true);
	ui->coronalFrame->setHidden(true);
	ui->threeDFrame->setHidden(true);
	ui->sagittalFrame->setHidden(true);

	ui->viewGridLayout->addWidget(ui->axialFrame);
	ui->axialFrame->setHidden(false);

	//Update GUI
	ui->actionSagittal->setChecked(false);
	ui->actionCoronal->setChecked(false);
	ui->actionAxial->setChecked(true);
	ui->actionThreeD->setChecked(false);
	ui->actionFourViews->setChecked(false);

	for (int i = 0; i < 4; i++)
		m_viewer[i]->ResizeViewer();

	//this->RenderAllViewer();
}

void MainWindow::slotThreeD()
{
	ui->viewGridLayout->removeWidget(ui->axialFrame);
	ui->viewGridLayout->removeWidget(ui->sagittalFrame);
	ui->viewGridLayout->removeWidget(ui->coronalFrame);
	ui->viewGridLayout->removeWidget(ui->threeDFrame);

	ui->axialFrame->setHidden(true);
	ui->coronalFrame->setHidden(true);
	ui->threeDFrame->setHidden(true);
	ui->sagittalFrame->setHidden(true);

	ui->viewGridLayout->addWidget(ui->threeDFrame);
	ui->threeDFrame->setHidden(false);

	//Update GUI
	ui->actionSagittal->setChecked(false);
	ui->actionCoronal->setChecked(false);
	ui->actionAxial->setChecked(false);
	ui->actionThreeD->setChecked(true);
	ui->actionFourViews->setChecked(false);

	for (int i = 0; i < 4; i++)
		m_viewer[i]->ResizeViewer();

	//this->RenderAllViewer();
}

void MainWindow::slotFourViews()
{
	ui->viewGridLayout->removeWidget(ui->axialFrame);
	ui->viewGridLayout->removeWidget(ui->sagittalFrame);
	ui->viewGridLayout->removeWidget(ui->coronalFrame);
	ui->viewGridLayout->removeWidget(ui->threeDFrame);

	ui->axialFrame->setHidden(true);
	ui->coronalFrame->setHidden(true);
	ui->sagittalFrame->setHidden(true);

	ui->threeDFrame->setHidden(true);

	ui->viewGridLayout->addWidget(ui->coronalFrame, 0, 0);
	ui->viewGridLayout->addWidget(ui->sagittalFrame, 0, 1);
	ui->viewGridLayout->addWidget(ui->axialFrame, 1, 0);
	ui->viewGridLayout->addWidget(ui->threeDFrame, 1, 1);

	ui->axialFrame->setHidden(false);
	ui->coronalFrame->setHidden(false);
	ui->sagittalFrame->setHidden(false);
	ui->threeDFrame->setHidden(false);

	//Update GUI
	ui->actionSagittal->setChecked(false);
	ui->actionCoronal->setChecked(false);
	ui->actionAxial->setChecked(false);
	ui->actionThreeD->setChecked(false);
	ui->actionFourViews->setChecked(true);

	for (int i = 0; i < 4; i++)
		m_viewer[i]->ResizeViewer();

	//this->RenderAllViewer();
}

void MainWindow::resizeEvent(QResizeEvent * event)
{
	QMainWindow::resizeEvent(event);

	for (int i = 0; i < 4; i++)
		m_viewer[i]->ResizeViewer();
}