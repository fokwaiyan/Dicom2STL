#ifndef MESHTOIMAGE_H
#define MESHTOIMAGE_H

#include "AbstractFilter.h"
#include "itkImage.h"
#include "itkMesh.h"
#include "itkTriangleMeshToBinaryImageFilter.h"
#include "vtkImageData.h"
#include "vtkMath.h"
#include "vtkPolyDataToitkMesh.h"
#include "vtkTriangleFilter.h"

#if VTK_MAJOR_VERSION <= 5
#define SetInputData SetInput
#define AddInputData AddInput
#endif

class MeshToImage : public AbstractFilter
{
	Q_OBJECT

public:
	//Constructor/Destructor
	MeshToImage(QObject* parent = NULL);
	~MeshToImage();
	
	//Setter
	void SetInputData(vtkPolyData* mesh);
	void SetImageInsideValue(int value);
	void SetReferenceImage(ImageType::Pointer refImage);

	//Getter
	ImageType::Pointer GetOutput();


public slots:
	virtual int Update();

private :
	int						m_imageInsideValue;
	vtkPolyData*			m_input;
	ImageType::Pointer		m_refImage;
	vtkPolyDataToitkMesh*	m_polyDataToitkMesh;
	vtkTriangleFilter*		m_triangleFilter;
	ImageType::Pointer		m_output;
};

#endif
