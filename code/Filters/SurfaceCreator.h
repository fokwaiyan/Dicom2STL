#ifndef SURFACECREATOR_H
#define SURFACECREATOR_H

#include "AbstractFilter.h"
#include <vtkMath.h>
#include <vtkImageData.h>
#include <vtkImageResample.h>
#include <vtkDiscreteMarchingCubes.h>
#include <vtkMarchingCubes.h>
#include <vtkPolyData.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkSmartPointer.h>
#include <vtkWindowedSincPolyDataFilter.h>
#include <vtkThreshold.h>
#include "vtkDataSetSurfaceFilter.h"
#include <vtkTriangleFilter.h>
#include <vtkConnectivityFilter.h>

class SurfaceCreator : public AbstractFilter
{
	Q_OBJECT

//#if VTK_MAJOR_VERSION > 5
//#define SetInput SetInputData
//#define AddInput AddInputData
//#endif

public:
	//Constructor/Destructor
    SurfaceCreator(QObject* parent = NULL );
	~SurfaceCreator();

	//Setter
    void SetInputData(vtkImageData*input);
	void SetResamplingFactor(double factor);
    void SetMarchingCubesValue(int value); 
	void SetDiscrete(bool b);
	void SetLargestConnected(bool b);			//for backward compatibility
	void SetSpecificId(int specificId);			//to choose connected region
	void SetSmootherPassBand(double s);	
	void SetSmootherIteration(int i);
	void SetSmootherAngle(double a);

	//Getter
    vtkPolyData* GetOutput();
    
public slots:
	virtual int Update();

private:
	//Parameter
	double			m_factor; 
    int				m_marchingCubesValue;
	bool			m_isDiscrete;
	bool			m_isSpecificId;
	double			m_smootherPassBand;
	double			m_smootherAngle;
	int				m_smootherIterations;
	int				m_specificId;
	
	//I/O
    vtkImageData*	m_input;
    vtkPolyData*	m_output;

	//Filter
	vtkImageResample*				m_resample;
	vtkMarchingCubes*				m_surface;
	//vtkPolyDataConnectivityFilter*	m_connectivityFilter;
	vtkConnectivityFilter*			m_connectivityFilter;
	vtkThreshold*					m_threshold;
	vtkWindowedSincPolyDataFilter*	m_smoother;
	vtkDataSetSurfaceFilter*		m_surfaceFilter;
	vtkTriangleFilter*				m_triangleFilter;
};

#endif