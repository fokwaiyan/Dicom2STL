#include "SurfaceCreator.h"
#include <QDebug>
#include <vtkGeometryFilter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkSortDataArray.h>
#include <QMap>
#include <vtkIdTypeArray.h>
#include <vtkDataArray.h>
#include <vtkPointData.h>

#if VTK_MAJOR_VERSION > 5
#define SetInput SetInputData
#define AddInput AddInputData
#endif

SurfaceCreator::SurfaceCreator(QObject* parent) : AbstractFilter(parent)
{
	//Default value
	m_resample				= NULL;
	m_surface				= NULL;
	m_connectivityFilter	= NULL;
	m_surfaceFilter			= NULL;
	m_smoother				= NULL;
	m_threshold				= NULL;
	m_input					= NULL;
	m_output				= NULL;
	m_factor				= 0.4;
	m_marchingCubesValue	= -700; //skin
	m_isDiscrete			= false;
	m_isSpecificId			= true;
}

SurfaceCreator::~SurfaceCreator(void)
{
	if (m_resample) m_resample->Delete();
	if (m_surface) m_surface->Delete();
	if (m_connectivityFilter) m_connectivityFilter->Delete();
	if (m_surfaceFilter) m_surfaceFilter->Delete();
	if (m_smoother) m_smoother->Delete();
	if (m_threshold) m_threshold->Delete();
}

void SurfaceCreator::SetDiscrete(bool b)
{
	m_isDiscrete = b;
}

void SurfaceCreator::SetResamplingFactor(double factor)
{
	m_factor = factor;
}

void SurfaceCreator::SetLargestConnected(bool b)
{
	if (b)
	{
		m_specificId = 0;
		m_isSpecificId = true;
	}
	else
		m_isSpecificId = false;
}

void SurfaceCreator::SetSpecificId(int specificId)
{
	m_specificId = specificId;
	m_isSpecificId = true;
}

void SurfaceCreator::SetInputData(vtkImageData* input)
{
	m_input = input;
}

void SurfaceCreator::SetMarchingCubesValue(int value)
{
	m_marchingCubesValue = value;
}

void SurfaceCreator::SetSmootherPassBand(double s)
{
	m_smootherPassBand = s;
}

void SurfaceCreator::SetSmootherIteration(int i)
{
	m_smootherIterations = i;
}

void SurfaceCreator::SetSmootherAngle(double a)
{
	m_smootherAngle = a;
}

int SurfaceCreator::Update()
{
	if (m_input == NULL) { emit Process(100); emit Finished("No Input Image"); return 1; }

	emit Process(10);
	m_resample = vtkImageResample::New();
	m_resample->SetAxisMagnificationFactor(0,m_factor);
	m_resample->SetAxisMagnificationFactor(1,m_factor);
	m_resample->SetAxisMagnificationFactor(2,m_factor);
	m_resample->SetInput(m_input);
	m_resample->Update();

	emit Process(30);
	
	if (m_isDiscrete){
		double* range = m_resample->GetOutput()->GetScalarRange();
		int numValue = vtkMath::Round(range[1] - range[0]) +1;
		
		m_surface = vtkDiscreteMarchingCubes::New();
		m_surface->SetNumberOfContours(numValue);
		m_surface->GenerateValues(numValue, range[0], range[1]);
	}
	else{
		m_surface = vtkMarchingCubes::New();
		m_surface->SetValue(0, m_marchingCubesValue);
	}

	m_surface->SetInput(m_resample->GetOutput());
	m_surface->ComputeNormalsOff();
	m_surface->ComputeGradientsOff();
	m_surface->SetComputeScalars(m_isDiscrete);
	m_surface->Update();

	emit Process(50);
	//largest connected
	m_connectivityFilter = vtkConnectivityFilter::New();
	m_connectivityFilter->SetInput(m_surface->GetOutput());
	m_connectivityFilter->ColorRegionsOn();
	m_connectivityFilter->SetExtractionModeToAllRegions();
	//m_connectivityFilter->SetExtractionModeToSpecifiedRegions();
	//m_connectivityFilter->AddSpecifiedRegion(m_specificId);
	m_connectivityFilter->Update();

	cout << "No threshold, number of points: " << m_connectivityFilter->GetOutput()->GetNumberOfPoints() << endl;

	if (m_isSpecificId)
	{
		m_threshold = vtkThreshold::New();
		m_threshold->SetInput(m_connectivityFilter->GetOutput());
		m_threshold->ThresholdBetween(m_specificId - 0.1, m_specificId + 0.1);
		m_threshold->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, 0);
		m_threshold->Update();

		//vtkSmartPointer<vtkGeometryFilter> geomFilter = vtkSmartPointer<vtkGeometryFilter>::New();
		//geomFilter->SetInputConnection(m_threshold->GetOutputPort());
		//geomFilter->Update();
		//cout << "After thres number of points: " << geomFilter->GetOutput()->GetNumberOfPoints() << endl;

		m_surfaceFilter = vtkDataSetSurfaceFilter::New();
		m_surfaceFilter->SetInput(m_threshold->GetOutput());
		m_surfaceFilter->Update();
	}
	else
	{
		m_surfaceFilter->SetInput((vtkDataObject *)m_connectivityFilter->GetOutput());
	}

	//m_surfaceFilter = vtkDataSetSurfaceFilter::New();
	//m_surfaceFilter->SetInput((vtkDataObject *)m_connectivityFilter->GetOutput());
	//m_surfaceFilter->Update();

	emit Process(70);
	m_smoother = vtkWindowedSincPolyDataFilter::New();
	m_smoother->SetInput(m_surfaceFilter->GetOutput());
	m_smoother->SetNumberOfIterations(m_smootherIterations);
	m_smoother->BoundarySmoothingOff();
	m_smoother->FeatureEdgeSmoothingOff();
	m_smoother->SetFeatureAngle(m_smootherAngle);
	m_smoother->SetPassBand(m_smootherPassBand);
	m_smoother->NonManifoldSmoothingOn();
	m_smoother->NormalizeCoordinatesOn();
	m_smoother->Update();

	m_triangleFilter = vtkTriangleFilter::New();
	m_triangleFilter->SetInputConnection(m_smoother->GetOutputPort());
	m_triangleFilter->Update();

	m_output = m_triangleFilter->GetOutput();
	//cout << "Number of points " << m_output->GetNumberOfPoints() << endl;

	if (m_output == NULL) { emit Process(100); emit Finished("No Output Mesh"); return true; }

	emit Process(100);
	emit Finished();
	return 0;
}

vtkPolyData* SurfaceCreator::GetOutput()
{
	return m_output;
}


  

