
#include <iostream>
#include "vtkPolyDataToitkMesh.h"
#include "itkTriangleCell.h"

#ifndef vtkDoubleType
#define vtkDoubleType double
#endif

#ifndef vtkFloatingPointType
# define vtkFloatingPointType vtkFloatingPointType
typedef float vtkFloatingPointType;
#endif

vtkPolyDataToitkMesh::vtkPolyDataToitkMesh(QObject* parent) : AbstractFilter(parent)
{
  m_input = NULL;
  m_output = TriangleMeshType::New();
}

vtkPolyDataToitkMesh::~vtkPolyDataToitkMesh()
{
}

void vtkPolyDataToitkMesh::SetInput(vtkPolyData * polydata)
{
  m_input = polydata;
}

vtkPolyDataToitkMesh::TriangleMeshType::Pointer vtkPolyDataToitkMesh::GetOutput()
{
  return m_output;
}

int vtkPolyDataToitkMesh::Update()
{
  //
  // Transfer the points from the vtkPolyData into the itk::Mesh
  //
  const unsigned int numberOfPoints = m_input->GetNumberOfPoints();
  vtkPoints * vtkpoints =  m_input->GetPoints();
 
  m_output->GetPoints()->Reserve( numberOfPoints );

  for(unsigned int p =0; p < numberOfPoints; p++)
    {

    vtkDoubleType * apoint = vtkpoints->GetPoint( p );
    m_output->SetPoint( p, TriangleMeshType::PointType( apoint ));

    // Need to convert the point to PoinType
    TriangleMeshType::PointType pt;
    for(unsigned int i=0;i<3; i++)
      {
       pt[i] = apoint[i];
       }
     m_output->SetPoint( p, pt);

    }
  //
  // Transfer the cells from the vtkPolyData into the itk::Mesh
  //
  vtkCellArray * triangleStrips = m_input->GetStrips();

  vtkIdType  * cellPoints;
  vtkIdType    numberOfCellPoints;

  //
  // First count the total number of triangles from all the triangle strips.
  //
  unsigned int numberOfTriangles = 0;

  triangleStrips->InitTraversal();
  while( triangleStrips->GetNextCell( numberOfCellPoints, cellPoints ) )
    {
    numberOfTriangles += numberOfCellPoints-2;
    }

   vtkCellArray * polygons = m_input->GetPolys();
  
   polygons->InitTraversal();

   while( polygons->GetNextCell( numberOfCellPoints, cellPoints ) )
     {
     if( numberOfCellPoints == 3 )
       {
        numberOfTriangles ++;
       }
     }

   //
  // Reserve memory in the itk::Mesh for all those triangles
  //
   m_output->GetCells()->Reserve( numberOfTriangles );

  // 
  // Copy the triangles from vtkPolyData into the itk::Mesh
  //
  //

   typedef TriangleMeshType::CellType   CellType;

   typedef itk::TriangleCell< CellType > TriangleCellType;

  // first copy the triangle strips
   int cellId = 0;
   triangleStrips->InitTraversal();
   while( triangleStrips->GetNextCell( numberOfCellPoints, cellPoints ) )
     {
     unsigned int numberOfTrianglesInStrip = numberOfCellPoints - 2;

	 TriangleMeshType::PointIdentifier pointIds[3];
     pointIds[0] = cellPoints[0];
     pointIds[1] = cellPoints[1];
     pointIds[2] = cellPoints[2];
       
     for( unsigned int t=0; t < numberOfTrianglesInStrip; t++ )
       {
        TriangleMeshType::CellAutoPointer c;
        TriangleCellType * tcell = new TriangleCellType;
        tcell->SetPointIds( pointIds );
        c.TakeOwnership( tcell );
        m_output->SetCell( cellId, c );
        cellId++;
        pointIds[0] = pointIds[1];
        pointIds[1] = pointIds[2];
        pointIds[2] = cellPoints[t+3];
       }
       
     }

   // then copy the triangles 
   polygons->InitTraversal();
   while( polygons->GetNextCell( numberOfCellPoints, cellPoints ) )
     {
     if( numberOfCellPoints !=3 ) // skip any non-triangle.
       {
       continue;
       }
     TriangleMeshType::CellAutoPointer c;
     TriangleCellType * t = new TriangleCellType;
     t->SetPointIds( (TriangleMeshType::PointIdentifier*)cellPoints );
     c.TakeOwnership( t );
     m_output->SetCell( cellId, c );
     cellId++;
     } 
  
   return 0;
}
