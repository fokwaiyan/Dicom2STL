#ifndef CLIPPINGWIDGET_H
#define CLIPPINGWIDGET_H

#include "Define.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"
#include "vtkPolydata.h"
#include "vtkBoxWidget2.h"
#include "vtkPlane.h"
#include "vtkPlaneWidget.h"
#include "vtkWidgetRepresentation.h"
#include "vtkCommand.h"
#include "vtkBox.h"
#include <vtkBoxRepresentation.h>
//Project
#include "../code/AbstractWidget/AbstractWidget.h"
#include "Planner.h"
//#include "ui_ClippingWidget.h"


namespace Ui{
	class ClippingWidget;
}

class ClippingWidget : public AbstractWidget
{
	Q_OBJECT

public:
	explicit ClippingWidget(QWidget* parent = 0);
	~ClippingWidget();

	//function
	void ClippingInteraction(vtkObject* caller, long unsigned int vtkNotUsed(eventId), void* vtkNotUsed(callData));
	vtkPolyData* GetOutput();
	
	public slots:

	void BoxClip();
	void PlaneClip();
	//void ContourClip();

	void ClipAway();
	void DoneClipping();
	void ExitClipping();
	void DoItAgain();
	void FlipPlane();

protected:
	int ClipMode;
	enum ClipModes
	{
		Plane = 0,
		Box = 1,
		Contour = 2,
	};

private:
	Ui::ClippingWidget* ui;
		
	void SetBoxWidget();
	void SetPlaneWidget();
	void CheckifExist();

	vtkBoxWidget2*	m_boxWidget;
	
	vtkPlaneWidget* m_planeWidget;
	vtkPlane*		m_plane;
	vtkPolyData*	m_mesh;

	unsigned long m_clipTag;
	
};

#endif //CLIPPINGWIDGET_H