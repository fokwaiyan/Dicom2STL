#include "ClippingWidget.h"
#include "ui_ClippingWidget.h"
#include "MainWindow.h"

ClippingWidget::ClippingWidget(QWidget* parent) :
	AbstractWidget(parent), ui(new Ui::ClippingWidget)
{
	ui->setupUi(this);
	ui->contourPushButton->setVisible(false);
	connect(ui->boxPushButton, SIGNAL(clicked()), this, SLOT(BoxClip()));
	connect(ui->planePushButton, SIGNAL(clicked()), this, SLOT(PlaneClip()));
	connect(ui->redoPushButton, SIGNAL(clicked()), this, SLOT(DoItAgain()));
	//connect(ui->contourPushButton, SIGNAL(clicked()), this, SLOT(ContourClip()));

	connect(ui->clipPushButton, SIGNAL(clicked()), this, SLOT(ClipAway()));
	connect(ui->donePushButton, SIGNAL(clicked()), this, SLOT(DoneClipping()));
	connect(ui->exitPushButton, SIGNAL(clicked()), this, SLOT(ExitClipping()));
}

ClippingWidget::~ClippingWidget()
{
	delete ui;
}

void ClippingWidget::ClippingInteraction(vtkObject* caller, long unsigned int vtkNotUsed(eventId), void* vtkNotUsed(callData))
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	MyViewer* myViewer = mainWnd->GetViewers(3);
	// Get the keyPress
	std::string key = myViewer->GetInteractor()->GetKeySym();
	std::cout << key << endl;
	
	// Handle a "normal" key
	if (key == "Delete")
	{
		this->ClipAway();
	}
	if (key == "Return")
	{
		this->DoneClipping();
	}
	if (key == "Escape")
	{
		this->ExitClipping();
	}
	else if (key == "BackSpace")
	{
		this->DoItAgain();
	}
	if (key == "Tab")
	{
		if (ClipMode == Plane)
		{
			this->FlipPlane();
		}
		else
			return;
	}
}


vtkPolyData* ClippingWidget::GetOutput()
{
	return m_mesh;
}

void ClippingWidget::BoxClip()
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	MyViewer* myViewer = mainWnd->GetViewers(3);
	ClipMode = Box;
	this->CheckifExist();
	this->SetBoxWidget();
	m_clipTag = myViewer->GetInteractor()->AddObserver(vtkCommand::KeyPressEvent, this, &ClippingWidget::ClippingInteraction, 1.0);
}

void ClippingWidget::PlaneClip()
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	MyViewer* myViewer = mainWnd->GetViewers(3);
	ClipMode = Plane;
	this->CheckifExist();
	this->SetPlaneWidget();
	m_clipTag = myViewer->GetInteractor()->AddObserver(vtkCommand::KeyPressEvent, this, &ClippingWidget::ClippingInteraction, 1.0);
}

//void ClippingWidget::ContourClip()
//{
//	qDebug() << "Contour Clip";
//	this->CheckifExist();
//}
//	
void ClippingWidget::ClipAway()		//Delete
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	MyViewer* myViewer = mainWnd->GetViewers(3);
	Planner* planner = mainWnd->GetPlanner();
	Mesh* mesh = planner->GetMeshByUniqueName("Clipped");
	vtkSmartPointer<vtkClipPolyData> clipper = vtkSmartPointer<vtkClipPolyData>::New();

	switch (ClipMode)
	{
	case Plane:		
		m_planeWidget->GetPlane(m_plane);
		
		clipper->SetClipFunction(m_plane);
		clipper->SetInputData(mesh->GetPolyData());
		clipper->InsideOutOn();
		clipper->Update();

		mesh->SetPolyData(clipper->GetOutput());
		myViewer->GetRenderWindow()->Render();
		break;
	case Box:
		vtkSmartPointer<vtkTransform> t = vtkSmartPointer<vtkTransform>::New();
		vtkSmartPointer<vtkBox> implicitCube = vtkSmartPointer<vtkBox>::New();
		vtkBoxRepresentation::SafeDownCast(m_boxWidget->GetRepresentation())->GetTransform(t);
		t->Inverse();

		implicitCube->SetBounds(mesh->GetActor()->GetBounds());
		implicitCube->SetTransform(t);

		clipper->SetInputData(mesh->GetPolyData());
		clipper->SetClipFunction(implicitCube);
		clipper->Update();

		mesh->SetPolyData(clipper->GetOutput());
		myViewer->GetRenderWindow()->Render();
		break;
	}
}

void ClippingWidget::DoneClipping()		//Return
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	MyViewer* myViewer = mainWnd->GetViewers(3);
	switch (ClipMode)
	{
	case Plane:
		m_planeWidget->Off();
		m_planeWidget->Delete();
		break;
	case Box:
		m_boxWidget->Off();
		Mesh* mesh = mainWnd->GetPlanner()->GetMeshByUniqueName("Clipped");
		mainWnd->UpdateMesh(mesh);
		//m_boxWidget->Delete();
		break; 
	}
		myViewer->GetRenderWindow()->RemoveObserver(m_clipTag);
		myViewer->GetRenderWindow()->Render();
}

void ClippingWidget::ExitClipping()		//Escape
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	MyViewer* myViewer = mainWnd->GetViewers(3);
	Planner* planner = mainWnd->GetPlanner();
	//if (planner->ContainsMesh("Clipped"))
	//{
	//	planner->RemoveMesh("Clipped");
	//}
	switch (ClipMode)
	{
	case Box:
		m_boxWidget->Off();
		break;
	case Plane:
		m_planeWidget->Off();
		break;
	}
	//mainWnd->updateMeshOverview();
	//mainWnd->visualizeMeshByUniqueName(planner->GetLatestEffectiveMesh()->GetUniqueName());
	myViewer->GetInteractor()->RemoveObserver(m_clipTag);
	myViewer->GetRenderWindow()->Render();
}

void ClippingWidget::DoItAgain()		//Backspace
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	MyViewer* myViewer = mainWnd->GetViewers(3);
	Planner* planner = mainWnd->GetPlanner();
	myViewer->RemoveActor(planner->GetMeshByUniqueName("Clipped")->GetActor());
	planner->RemoveMesh(planner->GetMeshByUniqueName("Clipped"));

	Mesh* toclip = new Mesh;
	qDebug() << "Do It Again:" << planner->GetLatestEffectiveMesh()->GetUniqueName();
	toclip->SetPolyData(planner->GetLatestEffectiveMesh()->GetPolyData());
	toclip->SetUniqueName("Clipped");
	toclip->SetColor(234.0 / 255.0, 189.0 / 255.0, 157.0 / 255.0);
	planner->AppendMesh(toclip);

	myViewer->GetDataRenderer()->AddActor(planner->GetMeshByUniqueName("Clipped")->GetActor());//clipped
	myViewer->GetRenderWindow()->Render();
}

void ClippingWidget::FlipPlane()		//Tab, only for plane
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	MyViewer* myViewer = mainWnd->GetViewers(3);
	m_planeWidget->GetPlane(m_plane);
	double* normal = m_plane->GetNormal();
	normal[0] = -normal[0];
	normal[1] = -normal[1];
	normal[2] = -normal[2];

	m_planeWidget->SetNormal(normal);
	m_planeWidget->Modified();
	myViewer->GetRenderWindow()->Render();
}

void ClippingWidget::SetBoxWidget()
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	MyViewer* myViewer = mainWnd->GetViewers(3);
	Planner* planner = mainWnd->GetPlanner();
	Mesh* boxClipMesh = planner->GetMeshByUniqueName("Clipped");
	
	m_boxWidget = vtkBoxWidget2::New();
	m_boxWidget->SetInteractor(myViewer->GetInteractor());
	m_boxWidget->GetRepresentation()->SetPlaceFactor(1);				// Default is 0.5
	m_boxWidget->GetRepresentation()->PlaceWidget(boxClipMesh->GetActor()->GetBounds());
	m_boxWidget->SetCurrentRenderer(myViewer->GetDataRenderer());
	m_boxWidget->On();
}

void ClippingWidget::SetPlaneWidget()
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	MyViewer* myViewer = mainWnd->GetViewers(3);
	Planner* planner = mainWnd->GetPlanner();
	Mesh* planeClipMesh = planner->GetMeshByUniqueName("Clipped");
	
	m_plane = vtkPlane::New();

	m_planeWidget = vtkPlaneWidget::New();
	m_planeWidget->SetInteractor(myViewer->GetInteractor());
	m_planeWidget->SetRepresentationToWireframe();
	m_planeWidget->SetInputData(planeClipMesh->GetPolyData());
	m_planeWidget->GetPlane(m_plane);
	m_planeWidget->SetResolution(20);
	m_planeWidget->SetCurrentRenderer(myViewer->GetDataRenderer());
	m_planeWidget->PlaceWidget();
	m_planeWidget->On();
}

void ClippingWidget::CheckifExist()
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	MyViewer* myViewer = mainWnd->GetViewers(3);
	Planner* planner = mainWnd->GetPlanner();

	QString hints;
	bool isNew = false;

	if (planner->ContainsMesh("Clipped"))
	{
		int ret = mainWnd->AskYesNoCancelQuestion("Object has already been clipped. \"Yes\" to continue or \"No\" to discard previous clipped.", QMessageBox::Ok);
		if (ret == 2)
			return;

		if (ret == 0)		//Add on for clipping
			hints = hints + "Please continue to clip.\n\n";

		if (ret == 1)		//Redo all steps
		{
			planner->RemoveMesh(planner->GetMeshByUniqueName("Clipped"));
			//for (int i = 0; i < planner->GetMeshListSize(); i++) 
			//{
			//	qDebug() << "Mesh List" << i << ":" << planner->GetMeshList()->at(i)->GetUniqueName() << endl;
			//}
			//hints = hints + "Previous clipping discarded. \n\n";
			isNew = true;
		}
	}
	else
		isNew = true;

	if (isNew)
	{
		Mesh* toClip = new Mesh;
		myViewer->GetDataRenderer()->AddActor(toClip->GetActor());
				
		toClip->SetPolyData(planner->GetMeshByUniqueName("Mesh")->GetPolyData());
		toClip->SetUniqueName("Clipped");
		toClip->SetColor(234.0 / 255.0, 189.0 / 255.0, 157.0 / 255.0);

		planner->AppendMesh(toClip);
		planner->GetLatestEffectiveMesh()->GetPolyData()->Print(cout);
	}

	//mainWnd->updateMeshOverview();
	//mainWnd->visualizeMeshByUniqueName("Clipped");
	myViewer->GetDataRenderer()->RemoveActor(planner->GetMeshByUniqueName("Mesh")->GetActor());
	myViewer->GetRenderWindow()->Render();
	

}

