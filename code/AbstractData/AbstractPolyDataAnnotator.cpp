/*
Author:		Wong, Matthew Lun
Date:		26th, May 2016
Occupation:	Chinese University of Hong Kong,
Department of Imaging and Inteventional Radiology,
Junior Research Assistant


The abstract data class is designed to hold data structure typically used in a medical viewer.
This class holds annotator polydata, which automatically generate 2D views based on 3 surfaces.


Wong Matthew Lun
Copyright (C) 2016
*/

#include "AbstractPolyDataAnnotator.h"

AbstractPolyDataAnnotator::AbstractPolyDataAnnotator() : AbstractPolyData()
{
	for (int i = 0; i < 3; i++)
	{
		m_2Dactors[i] = vtkSmartPointer<vtkActor>::New();
		
	}
}

AbstractPolyDataAnnotator::~AbstractPolyDataAnnotator() 
{
}

void AbstractPolyDataAnnotator::SetCutFunctions(vtkSmartPointer<vtkPlane>* inPlanes)
{
	for (int i = 0; i < 3; i++)
	{
		m_cutPlanes[i] = inPlanes[i];

	}
}

void AbstractPolyDataAnnotator::SetCutFunctions(vtkSmartPointer<vtkPlane> sx, vtkSmartPointer<vtkPlane> sy, vtkSmartPointer<vtkPlane> sz)
{
	m_cutPlanes[0] = sx;
	m_cutPlanes[1] = sy;
	m_cutPlanes[2] = sz;
}

void AbstractPolyDataAnnotator::SetCutFunctions(int i, vtkSmartPointer<vtkPlane> inPlane)
{
	m_cutPlanes[i] = inPlane;
}

vtkSmartPointer<vtkActor>* AbstractPolyDataAnnotator::Get2DActors()
{
	return m_2Dactors;
}

vtkSmartPointer<vtkActor> AbstractPolyDataAnnotator::Get2DActor(int index)
{
	// Error check
	if (index >= 3) {
		return NULL;
	}
	return m_2Dactors[index];
}

void AbstractPolyDataAnnotator::SetVisibility(bool vis)
{
	AbstractPolyData::SetVisibility(vis);
	for (int i = 0; i<3; i++)
	{
		m_2Dactors[i]->SetVisibility(vis);
	}
}

void AbstractPolyDataAnnotator::SetOpacity(double opacity)
{
	m_actor->GetProperty()->SetOpacity(opacity);
	for (int i = 0; i<3; i++)
	{
		m_2Dactors[i]->GetProperty()->SetOpacity(opacity);
	}
}

void AbstractPolyDataAnnotator::SetActorState(int state)
{
	if (!m_polyData)
		throw POLYDATA_MISSING_ERROR;
	if (!m_actor)
		throw ACTORS_NOT_INITIALIZED_ERROR;
	if (!m_2Dactors[0] || !m_2Dactors[1] || !m_2Dactors[2])
		throw ACTORS_2D_MISSING_ERROR;

	if (this->m_actorState != state) {
		this->m_actorState = state;
		try {
			switch (state)
			{
			case HOVERED:
				for (int i = 0; i < 3; i++)
					this->m_2Dactors[i]->GetProperty()->SetColor(this->m_hoverColor);
				this->m_actor->GetProperty()->SetColor(this->m_hoverColor);
				break;
			case SELECTED:
				for (int i = 0; i < 3; i++)
					this->m_2Dactors[i]->GetProperty()->SetColor(this->m_selectedColor);
				this->m_actor->GetProperty()->SetColor(this->m_selectedColor);
				break;
			case IDLE:
				for (int i = 0; i < 3; i++)
					this->m_2Dactors[i]->GetProperty()->SetColor(this->m_color);
				this->m_actor->GetProperty()->SetColor(this->m_color);
				break;
			default:
				break;
			}
		}
		catch (...) {
			return;
		}
	}
}

bool AbstractPolyDataAnnotator::GetVisibility()
{
	return m_actor->GetVisibility();
}

double AbstractPolyDataAnnotator::GetOpacity()
{
	return m_actor->GetProperty()->GetOpacity();
}

void AbstractPolyDataAnnotator::Update()
{
	AbstractPolyData::Update();

	for (int i = 0; i < 3; i++)
	{
		m_cutters[i]->Update();
		m_boundaryStrips[i]->Update();
		m_polyData2D[i]->SetPoints(m_boundaryStrips[i]->GetOutput()->GetPoints());
		m_polyData2D[i]->SetPolys(m_boundaryStrips[i]->GetOutput()->GetLines());
		m_triangleFilter[i]->Update();
	}
}

vtkSmartPointer<vtkPolyData> AbstractPolyDataAnnotator::GetPolyDataOutput(int n)
{
	// Legacy function, do not use
	int index = n - 1;
	return m_triangleFilter[index]->GetOutput();
}

vtkSmartPointer<vtkActor> AbstractPolyDataAnnotator::GetActor(int n)
{
	if (n == 3) {
		return m_actor;
	}
	else {
		return m_2Dactors[n];
	}
}

vtkSmartPointer<vtkActor> AbstractPolyDataAnnotator::GetActor()
{
	return AbstractPolyData::GetActor();
}

void AbstractPolyDataAnnotator::InstallPipeline()
{
	this->InitializeSource();
	AbstractPolyData::InstallPipeline();
}

void AbstractPolyDataAnnotator::InitializeActors()
{
	// This assumes source already exist
	// if source does not exist

	if (!this->m_polyData) {
		throw POLYDATA_MISSING_ERROR;
	}

	// if cutting planes were not set
	if (!this->m_cutPlanes[0] || !this->m_cutPlanes[1] || !this->m_cutPlanes[2]) {
		throw CUT_PLANES_MISSING_ERROR;
	}

	vtkSmartPointer<vtkStripper> boundaryStrips[3];
	vtkSmartPointer<vtkPolyDataMapper> polyDataMapper[3];

	for (int i = 0; i < 3; i++)
	{
		// Generate cutter functions for 2D actors
		// Used transform filter instead of m_polydata so the update follows actor state
		m_cutters[i] = vtkSmartPointer<vtkCutter>::New();
		m_cutters[i]->SetCutFunction(m_cutPlanes[i]);
		m_cutters[i]->SetInputConnection(0, this->m_transformFilter->GetOutputPort());
		m_cutters[i]->Update();

		m_boundaryStrips[i] = vtkStripper::New();
		m_boundaryStrips[i]->SetInputConnection(m_cutters[i]->GetOutputPort());
		m_boundaryStrips[i]->Update();

		m_polyData2D[i] = vtkPolyData::New();
		m_polyData2D[i]->SetPoints(m_boundaryStrips[i]->GetOutput()->GetPoints());
		m_polyData2D[i]->SetPolys(m_boundaryStrips[i]->GetOutput()->GetLines());

		m_triangleFilter[i] = vtkTriangleFilter::New();

#if VTK_MAJOR_VERSION <= 5
		m_triangleFilter[i]->SetInput(m_polyData2D[i]);
#else
		m_triangleFilter[i]->SetInputData(m_polyData2D[i]);
#endif
		m_triangleFilter[i]->Update();

		polyDataMapper[i] = vtkPolyDataMapper::New();
		polyDataMapper[i]->SetInputConnection(m_triangleFilter[i]->GetOutputPort());
		polyDataMapper[i]->SetScalarVisibility(false);
		polyDataMapper[i]->Update();

		m_2Dactors[i]->SetMapper(polyDataMapper[i]);
	}


	AbstractPolyData::InitializeActors();
}

