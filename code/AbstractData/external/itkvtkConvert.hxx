/*
This code is the header of the itkvtkConvert class

Author: Wong Matthew Lun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Copyright (C) 2015	Wong Matthew Lun
*/
#ifndef ITKVTKCONVERT_H
#define ITKVTKCONVERT_H

// Debug tag, disable this if you don't have boost
//#define BOOST_BUILD

#if VTK_MAJOR_VERSION  < 6
#define SetInputData SetInput
#endif

#include <iostream>
#include <ctime>
#include <math.h>
#include <itkFlipImageFilter.h>
#include <itkVTKImageToImageFilter.h>
#include <itkImageToVTKImageFilter.h>
#include <itkSmartPointer.h>
#include <itkMacro.h>
#include <itkChangeInformationImageFilter.h>
#include <itkImageFileWriter.h>
#include <vtkImageReslice.h>
#include <vtkMatrix3x3.h>
#include <vtkMatrix4x4.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkMath.h>
#include <vtkImageActor.h>
#include <vtkObject.h>
#include <vtkPointSource.h>
#include <vtkPlane.h>
#include <vtkImageFlip.h>
#include <vtkImageResliceMapper.h>

// DEBUG Timer
#ifdef BOOST_BUILD
#include <boost/timer.hpp>
#endif

// sprintf
#ifdef _WIN64
#define sprintf sprintf_s
#endif

// Typedefs
typedef itk::Matrix<double, 3, 3> d33itkMatrixType;
typedef itk::Matrix<double, 4, 4> d44itkMatrixType;
typedef vtkSmartPointer<vtkMatrix3x3> d33vtkMatrixType;
typedef vtkSmartPointer<vtkMatrix4x4> d44vtkMatrixType;

template <typename TInputImage>
class itkvtkConvert : public itk::Object
{
public:
	itkvtkConvert();
	~itkvtkConvert() {

	};

	typedef itkvtkConvert					Self;
	typedef itk::Object						SuperClass;
	typedef itk::SmartPointer<Self>			Pointer;
	typedef itk::SmartPointer<const Self>	ConstPointer;

#ifdef _WIN64
	itkNewMacro(itkvtkConvert);
#else
	static itkvtkConvert::Pointer New() {
		return new itkvtkConvert();
	}
#endif
	itkTypeMacro(itkvtkConvert, itk::Object);
	//itkCloneMacro(itkvtkConvert);

	typedef TInputImage	InputImageType;
	typedef itk::ImageToVTKImageFilter<TInputImage> itkConnectorType;

	void DirectionOn();
	void DirectionOff();
	void SetObliqueNormal(double*);
	void SetObliqueNormal(double, double, double);
	void SetInput(itk::SmartPointer<TInputImage>);
	void SetAdditionalTransform(vtkSmartPointer<vtkTransform>);
	void SetObliqueTransform(vtkSmartPointer<vtkTransform>);
	void SetPosition(double[3], bool anchor = true);
	void ImageWheelForward();
	void ImageWheelBackward();
	void ForceSaveCurrentObliqueTransform();
	//void SetResliceAxes(double[4][4]);
	//void SetResliceAxes(double[16]);


	itk::SmartPointer<TInputImage>			GetInputImage();
	itk::SmartPointer<TInputImage>			GetOutputImage();
	vtkSmartPointer<vtkImageSlice>*			GetReslicers();
	vtkSmartPointer<vtkImageSlice>			GetReslicer(int);
	vtkSmartPointer<vtkPlane>*				GetReslicePlanes();
	vtkSmartPointer<vtkPlane>				GetReslicePlane(int);
	vtkSmartPointer<vtkPlane>*				GetResliceObliqueInversedPlanes();
	vtkSmartPointer<vtkPlane>				GetResliceObliqueInversedPlane(int);
	vtkSmartPointer<vtkImageResliceMapper>* GetReslicerMappers();
	vtkSmartPointer<vtkImageResliceMapper>	GetReslicerMapper(int);
	vtkSmartPointer<vtkTransform>			GetResliceTransform();
	vtkSmartPointer<vtkTransform>			GetResliceBaseTransform();
	vtkSmartPointer<vtkTransform>			GetInverseResliceTransform();
	vtkSmartPointer<vtkTransform>			GetMeshTransform();
	vtkSmartPointer<vtkTransform>			GetObliqueTransform();
	vtkSmartPointer<vtkImageSlice>			GetViewProp(int);
	vtkImageData*							GetVtkImage();
	vtkTransform*							GetAdditionalTransform();
	void	GetITKIndexFromVTKImageActorPoint(double* pointOnImageActor, int* out);
	void	GetITKPointFromVTKImageActorPoint(double* pointOnImageActor, double* out);
	void	GetVTKPointFromVTKImageActorPoint(double * pointOnImageActor, double* out);
	void	GetCurrentPos(double*);
	int*	GetITKPointFromITKIndex(int* indexInITKImage);
	int*	GetITKIndexFromITKPoint(double* pointInITKImage);
	int*	GetVTKIndexFromITKIndex(int* indexInITKImage);
	double* GetVTKPointFromITKIndex(double* indexInItkImage);
	double  GetSliceSpacing();

	void UpdateConnector();
	void Update();
	void Modified();
	void Transformed();

private:
	// Private Getable variables
	typename itkConnectorType::Pointer m_connector;
	vtkSmartPointer<vtkImageResliceMapper> m_resliceMapper[3];
	vtkSmartPointer<vtkImageSlice>	m_reslicers[3];
	vtkSmartPointer<vtkPlane>		m_reslicePlanes[3];
	vtkSmartPointer<vtkPlane>		m_reslicePlanesInversed[3];
	vtkSmartPointer<vtkTransform>	m_obliqueResliceTransform;
	vtkSmartPointer<vtkTransform>	m_resliceBaseTransform;
	vtkSmartPointer<vtkTransform>	m_additionalTransform;
	vtkSmartPointer<vtkTransform>	m_resliceTransform;
	vtkSmartPointer<vtkTransform>	m_meshTransform;
	itk::SmartPointer<TInputImage>	m_inItkImage;
	itk::SmartPointer<TInputImage>	m_outItkImage;
	d33vtkMatrixType m_resliceNormals;

	vtkImageData* m_vtkImageData;
	double m_curpos[3];
	double m_sliceSpacing;





	// Private in-getable variables
	vtkSmartPointer<vtkTransform> m_imActAdjustTransform;
	vtkSmartPointer<vtkTransform> m_usedTransform;
	vtkSmartPointer<vtkTransform> m_obliqueTransformHolder_;
	vtkSmartPointer<vtkTransform> m_worldToITKIndex_;
	vtkSmartPointer<vtkTransform> m_worldToITKCoord_;
	vtkSmartPointer<vtkTransform> m_worldToModel_;
	d44vtkMatrixType m_indexToPoint_;
	d44vtkMatrixType m_pointToIndex_;
	d44vtkMatrixType m_invertAdditionalTransform_;
	d44vtkMatrixType m_invertResliceBaseTransform_;
	d44vtkMatrixType m_invertResliceTransform_;
	d44vtkMatrixType m_invertTranslation_;
	d44vtkMatrixType m_invertImActAdjustTransform_;
	d33itkMatrixType m_direction_;
	d33itkMatrixType m_inverseDirection_;
	double* m_imageActorOrigin_;

	// DEBUG timer
#ifdef BOOST_BUILD
	boost::timer m_timer;
	double _StartTime;
#endif // BOOST_BUILD
	void _ElapsedTime(std::string tag, std::string msg);
	void _StartCountPoint();
	void _Time(std::string tag, std::string msg);


	// Private Flags
	bool M_DIRECTION_FLAG;
	bool M_INDEX_TO_POINT;
	bool M_POINT_TO_INDEX;
	bool M_MODIFIED;
	bool M_TRANSFORMED;

	// Private Functions
	vtkSmartPointer<vtkTransform> _GetWorldToITKIndexTransform();
	vtkSmartPointer<vtkTransform> _GetWorldToITKModelTransform();
	d44vtkMatrixType _GetIndexToPointMatrix();
	d44vtkMatrixType _GetPointToIndexMatrix();
	void _buildIndexVsPointConversionMatrix();


};



template<typename TInputImage>
itkvtkConvert<TInputImage>::itkvtkConvert()
{

	m_additionalTransform = vtkSmartPointer<vtkTransform>::New();
	m_resliceTransform = vtkSmartPointer<vtkTransform>::New();
	m_resliceBaseTransform = vtkSmartPointer<vtkTransform>::New();
	m_usedTransform = vtkSmartPointer<vtkTransform>::New();
	m_meshTransform = vtkSmartPointer<vtkTransform>::New();
	m_obliqueResliceTransform = vtkSmartPointer<vtkTransform>::New();
	m_obliqueTransformHolder_ = vtkSmartPointer<vtkTransform>::New();
	m_indexToPoint_ = d44vtkMatrixType::New();
	m_pointToIndex_ = d44vtkMatrixType::New();
	m_invertAdditionalTransform_ = d44vtkMatrixType::New();
	m_invertResliceBaseTransform_ = d44vtkMatrixType::New();
	m_invertResliceTransform_ = d44vtkMatrixType::New();
	m_invertTranslation_ = d44vtkMatrixType::New();
	m_invertImActAdjustTransform_ = d44vtkMatrixType::New();

	for (int i = 0; i < 3; i++)
	{
		m_reslicers[i] = vtkSmartPointer<vtkImageSlice>::New();
		m_resliceMapper[i] = vtkSmartPointer<vtkImageResliceMapper>::New();
		m_reslicePlanes[i] = vtkSmartPointer<vtkPlane>::New();
		m_reslicePlanesInversed[i] = vtkSmartPointer<vtkPlane>::New();
		m_reslicePlanes[i]->SetTransform(m_obliqueResliceTransform);
		m_reslicePlanesInversed[i]->SetTransform(m_obliqueResliceTransform->GetLinearInverse());
		m_curpos[i] = 0;
	}

	M_INDEX_TO_POINT = false;
	M_POINT_TO_INDEX = false;
	M_MODIFIED = true;
	M_TRANSFORMED = false;
	M_DIRECTION_FLAG = true;
}

//template<typename TInputImage>
//itkvtkConvert<TInputImage> * itkvtkConvert<TInputImage>::New()
//{
//	return new itkvtkConvert<TInputImage>();
//}
template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::UpdateConnector() {
	m_connector->Modified();
	m_connector->Update();
	m_vtkImageData = m_connector->GetOutput();
};

template<typename TInputImage>
inline vtkSmartPointer<vtkTransform> itkvtkConvert<TInputImage>::GetInverseResliceTransform()
{
	vtkSmartPointer<vtkTransform> inverseResliceTransform = vtkSmartPointer<vtkTransform>::New();
	d44vtkMatrixType m = d44vtkMatrixType::New();
	vtkMatrix4x4::Invert(m_resliceTransform->GetMatrix(), m);
	inverseResliceTransform->SetMatrix(m);
	return inverseResliceTransform;
}

// Return the transform that transform vtkImageData, which has no direction info,  to ITK's
// space according to the direction and origin in the header of the ITK Image.
// This transform can be added to the actor of the mesh or other corresponding object
// that has the same origin as the vtkImageData.
template<typename TInputImage>
inline vtkSmartPointer<vtkTransform> itkvtkConvert<TInputImage>::GetMeshTransform()
{
	return m_meshTransform;
}

template<typename TInputImage>
inline vtkSmartPointer<vtkTransform> itkvtkConvert<TInputImage>::GetObliqueTransform()
{
	return m_obliqueResliceTransform;
}

template<typename TInputImage>
inline vtkSmartPointer<vtkImageSlice> itkvtkConvert<TInputImage>::GetViewProp(int index)
{
	return m_reslicers[index];
}

template<typename TInputImage>
inline vtkImageData * itkvtkConvert<TInputImage>::GetVtkImage()
{
	return m_vtkImageData;
}


template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::DirectionOff()
{
	// if direction flag is on, turn off direction by applying reverse slice
	if (M_DIRECTION_FLAG) {
		m_obliqueTransformHolder_->GetMatrix()->DeepCopy(m_obliqueResliceTransform->GetMatrix());
		m_obliqueResliceTransform->GetMatrix()->DeepCopy(m_resliceBaseTransform->GetMatrix());
		M_DIRECTION_FLAG = false;
		for (int i = 0; i < 3; i++)
		{
			m_reslicePlanes[i]->SetTransform(m_obliqueResliceTransform);
			m_reslicers[i]->Update();
		}

	}
}

template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::DirectionOn()
{
	// if direction flag is off, turn on direction by copying the stored matrix to the transform
	if (!M_DIRECTION_FLAG) {
		m_obliqueResliceTransform->GetMatrix()->DeepCopy(m_obliqueTransformHolder_->GetMatrix());
		M_DIRECTION_FLAG = true;
		for (int i = 0; i < 3; i++)
		{
			m_reslicePlanes[i]->SetTransform(m_obliqueResliceTransform);
			m_reslicers[i]->Update();
		}
	}
}

template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::SetObliqueNormal(double * inNormal)
{
	this->SetObliqueNormal(inNormal[0], inNormal[1], inNormal[2]);
}

template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::SetObliqueNormal(double x1, double x2, double x3)
{
	double* inNormal = (double*)malloc(sizeof(double) * 4);
	inNormal[0] = x1;
	inNormal[1] = x2;
	inNormal[2] = x3;
	inNormal[3] = 1;

	// Set transform for image actor first
	vtkMath::Normalize(inNormal);
	double reslicerNormal[4] = { 0,0,1,1 };
	//memcpy(reslicerNormal, m_reslicePlanes[2]->GetNormal(), sizeof(double) * 3);
	vtkMath::Normalize(reslicerNormal);

	double* bisector = (double*)malloc(sizeof(double) * 3);
	double* rotationalAxis = (double*)malloc(sizeof(double) * 3);
	double rotationAngle;


	vtkMath::Add(inNormal, reslicerNormal, bisector);
	vtkMath::Normalize(bisector);

	vtkMath::Cross(reslicerNormal, inNormal, rotationalAxis);
	rotationAngle = vtkMath::DegreesFromRadians(
		acos(vtkMath::Dot(inNormal, bisector)) +
		acos(vtkMath::Dot(bisector, reslicerNormal))
		);

	m_obliqueResliceTransform->Identity();
	m_obliqueResliceTransform->PostMultiply();
	m_obliqueResliceTransform->RotateWXYZ(rotationAngle, rotationalAxis);

	for (int i = 0; i < 3; i++)
	{
		//m_reslicePlanes[i]->SetTransform(m_obliqueResliceTransform);
		m_reslicers[i]->Update();
	}

	free(bisector);
	free(rotationalAxis);
}

template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::SetInput(itk::SmartPointer<TInputImage> imIn)
{
	m_inItkImage = imIn;
}

/*
Get the additional transform applied to the volume
*/
template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::SetAdditionalTransform(vtkSmartPointer<vtkTransform> additionalTransform)
{
	m_additionalTransform = additionalTransform;
}

/*
Be warned that setting this function you must also set the user transform of each planes manually.
If you wish to change the matrix of the oblique matrix, you should use GetObliqueTransform()->DeepCopy()
instead.
*/
template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::SetObliqueTransform(vtkSmartPointer<vtkTransform> obliqueTransform)
{
	m_obliqueResliceTransform = obliqueTransform;
}


/*
Use this to set the position of the imageActor cursor, the position follows
world coordinate of the renderer. When call the image actor and reslice will
snap to the picked point.

Note that you should choose the movingUnitVector carefully so that it follows
the normal of the resliced surface, it is usually referred to the third colume
of the reslicer's transformation without the additionalTransformation.

In this case, m_resliceAxes is used because the reslicer and image actors are
transform back to follow the x-y-z coordinate system, so resliceAxes is used
where the third col of the matrix indicates the reslice normal. However, if
you choose to keep your slicers and image actor in the model space, you should
first multiply the usermatrix and the resliceAxes matrix then take the thrid
colume of the product matrix.
*/
template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::SetPosition(double newPos[3], bool anchor)
{


	double *modelCoord = (double*)malloc(sizeof(double) * 4);
	modelCoord[3] = 1;

	if (anchor) {
		for (int i = 0; i < 3; i++)
		{
			double* l_normal = (double*)malloc(sizeof(double) * 4);
			l_normal[3] = 1;
			memcpy(l_normal, m_reslicePlanesInversed[i]->GetNormal(), sizeof(double) * 3);
			memcpy(l_normal, m_obliqueResliceTransform->GetMatrix()->MultiplyDoublePoint(l_normal), sizeof(double) * 3);
			if (abs(m_reslicePlanesInversed[i]->FunctionValue(newPos)) < 1) {
				vtkMath::MultiplyScalar(l_normal, m_reslicePlanesInversed[i]->FunctionValue(newPos));
				vtkMath::Subtract(newPos, l_normal, newPos);
			}
			free(l_normal);
		}
	}

	memcpy(modelCoord, newPos, sizeof(double) * 3);
	memcpy(modelCoord, m_obliqueResliceTransform->GetLinearInverse()->GetMatrix()->MultiplyDoublePoint(modelCoord), sizeof(double) * 3);

	this->_StartCountPoint();
	for (int i = 0; i < 3; i++)
	{
		m_reslicePlanes[i]->SetOrigin(modelCoord);
		m_reslicePlanesInversed[i]->SetOrigin(modelCoord);
		m_resliceMapper[i]->SetSlicePlane(m_reslicePlanes[i]);
	}
	//char msg[200];
	//sprintf(msg, "Clicked pos: (%.03f, %.03f, %.03f)\tTransed:  (%.03f, %.03f, %.03f)",
	//	newPos[0], newPos[1], newPos[2],
	//	modelCoord[0], modelCoord[1], modelCoord[2]);
	//std::cout << msg << endl;
	//this->_Time("DEBUG", msg);
	//free(modelCoord);
	memcpy(m_curpos, newPos, sizeof(double) * 3);
}



template<typename TInputImage>
void itkvtkConvert<TInputImage>::ImageWheelForward()
{
	std::cerr << "Functio ImageWheelForward removed" << endl;
}

template<typename TInputImage>
void itkvtkConvert<TInputImage>::ImageWheelBackward()
{
	std::cerr << "Functio ImageWheelBackward removed" << endl;
}

template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::ForceSaveCurrentObliqueTransform()
{
	m_obliqueTransformHolder_->DeepCopy(m_obliqueResliceTransform);
}


template<typename TInputImage>
inline itk::SmartPointer<TInputImage> itkvtkConvert<TInputImage>::GetInputImage()
{
	return m_inItkImage;
}


template<typename TInputImage>
inline vtkSmartPointer<vtkPlane> itkvtkConvert<TInputImage>::GetReslicePlane(int index)
{
	return m_reslicePlanes[index];
}

template<typename TInputImage>
inline vtkSmartPointer<vtkPlane>* itkvtkConvert<TInputImage>::GetResliceObliqueInversedPlanes()
{
	return m_reslicePlanesInversed;
}

template<typename TInputImage>
vtkSmartPointer<vtkTransform> itkvtkConvert<TInputImage>::GetResliceTransform()
{
	return m_resliceTransform;
}

template<typename TInputImage>
inline vtkSmartPointer<vtkTransform> itkvtkConvert<TInputImage>::GetResliceBaseTransform()
{
	return m_resliceBaseTransform;
}



template<typename TInputImage>
inline vtkTransform * itkvtkConvert<TInputImage>::GetAdditionalTransform()
{
	return m_additionalTransform;
}



//Obtain the index of the pixel under the world coordinate. Note that this function
//will not identify if the ijk index is out of bound or not, you should check it before
//you use it.
template<typename TInputImage>
void itkvtkConvert<TInputImage>::GetITKIndexFromVTKImageActorPoint(double * pointOnImageActor, int* out)
{
	// Make sure the forth element of the input is 1
	pointOnImageActor[3] = 1.;

	// Apply reverse Transformation
	double *doubleijk;
	int dummyijk[3];

	/*
	The reverse transformation is done in the following fashion :
	1. Undo m_additionalTransform while leaving slice wheel transform untouched.
	2. Translate origin of image to 0, 0, 0
	3. Apply point to index matrix

	For more details please refer to _buildIndexVsPointConversionMatrix() function.
	*/
	d44vtkMatrixType pointToIndex = _GetWorldToITKIndexTransform()->GetMatrix();
	doubleijk = pointToIndex->MultiplyDoublePoint(pointOnImageActor);

	// Round the result
	for (int i = 0; i < 3; i++)
	{
		dummyijk[i] = vtkMath::Round(doubleijk[i]);
	}
	memcpy(out, dummyijk, sizeof(int) * 3);
}

// Obtain the coordinates of the pixel in model space. Note that this function will not identify
//if the output coordinate is out of bound or not, you should check it before you use it.
template<typename TInputImage>
void itkvtkConvert<TInputImage>::GetITKPointFromVTKImageActorPoint(double * pointOnImageActor, double* out)
{
	// Make sure the forth element of the input is 1
	pointOnImageActor[3] = 1.;

	// Create a 3 element double for output, remember to release this after use;
	double* itkPoint;

	// Everything same as GetITKIndexFromVTKImageActorPoint except no poin to index matrix applied
	d44vtkMatrixType pointToITKPoint = _GetWorldToITKModelTransform()->GetMatrix();
	itkPoint = pointToITKPoint->MultiplyDoublePoint(pointOnImageActor);

	memcpy(out, itkPoint, sizeof(double) * 3);
}

template<typename TInputImage>
void itkvtkConvert<TInputImage>::GetVTKPointFromVTKImageActorPoint(double * pointOnImageActor, double* out)
{
	// This function is under construction
	return;

	// Make sure the forth element of the input is 1
	pointOnImageActor[3] = 1.;

	// Create a 3 element double for output, remember to release this after use;
	double* vtkPoint;

	// Everything same as GetITKIndexFromVTKImageActorPoint except no poin to index matrix applied
	vtkMath::Subtract(pointOnImageActor, m_vtkImageData->GetOrigin(), pointOnImageActor);
	vtkPoint = m_resliceBaseTransform->GetLinearInverse()->GetMatrix()->MultiplyDoublePoint(pointOnImageActor);
	vtkMath::Add(vtkPoint, m_vtkImageData->GetOrigin(), vtkPoint);

	memcpy(out, vtkPoint, sizeof(double) * 3);
}

template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::GetCurrentPos(double* curpos)
{
	memcpy(curpos, m_curpos, sizeof(double) * 3);
}

template<typename TInputImage>
int * itkvtkConvert<TInputImage>::GetITKPointFromITKIndex(int * indexInITKImage)
{
#ifdef _WIN64
	RaiseException ERROR_CALL_NOT_IMPLEMENTED;
#endif
}

template<typename TInputImage>
int * itkvtkConvert<TInputImage>::GetITKIndexFromITKPoint(double * pointInITKImage)
{
#ifdef _WIN64
	RaiseException ERROR_CALL_NOT_IMPLEMENTED;
#endif
}

template<typename TInputImage>
int * itkvtkConvert<TInputImage>::GetVTKIndexFromITKIndex(int * indexInITKImage)
{
#ifdef _WIN64
	RaiseException ERROR_CALL_NOT_IMPLEMENTED;
#endif
}

template<typename TInputImage>
double * itkvtkConvert<TInputImage>::GetVTKPointFromITKIndex(double * indexInItkImage)
{
#ifdef _WIN64
	RaiseException ERROR_CALL_NOT_IMPLEMENTED;
#endif
}


template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::Update()
{
	// If not modified, skip the function
	if (M_MODIFIED)
	{
		double vtkImageOrigin[3];
		// Allocate UserTransformMatrix and adjustImageActorTransform
		d44vtkMatrixType userTransformMatrix = d44vtkMatrixType::New();
		d44vtkMatrixType imActorAdjustTransform = d44vtkMatrixType::New();

		// flip use this filter if you are bothered by the y-axis flip
		//typedef itk::FlipImageFilter<TInputImage> FlipImageFilterType;
		//FlipImageFilterType::Pointer flipfilter = FlipImageFilterType::New();
		//bool axes[3] = { false, true, false };
		//flipfilter->SetInput(m_inItkImage);
		//flipfilter->SetFlipAxes(axes);
		//flipfilter->Update();

		// Convert the image to vtkImageData
		if (m_connector.IsNull()) {
			// Create the connector if it is not New() outside the class
			m_connector = itkConnectorType::New();
			m_connector->SetInput(m_inItkImage);
		}
		m_connector->Update();
		m_vtkImageData = m_connector->GetOutput();
		memcpy(vtkImageOrigin, m_inItkImage->GetOrigin().GetDataPointer(), sizeof(double) * 3);

		// Set up usermatrix for reslicer
		m_additionalTransform->Identity();
		m_direction_ = m_inItkImage->GetDirection();
		m_inverseDirection_ = m_inItkImage->GetInverseDirection();
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				userTransformMatrix->SetElement(i, j, m_direction_(i, j));
			}
			// Translation components and bottom row allocation
			userTransformMatrix->SetElement(i, 3, 0);
			userTransformMatrix->SetElement(3, i, 0.);
		}
		userTransformMatrix->SetElement(3, 3, 1);
		m_resliceBaseTransform->Identity();
		m_resliceBaseTransform->PostMultiply();
		m_resliceBaseTransform->Concatenate(userTransformMatrix);
		m_resliceBaseTransform->Update();

		// Prepare Transform pipeline for reslicer
		// Note that step 1 and 3 are performed here
		m_resliceTransform->Identity();
		m_resliceTransform->PostMultiply();
		m_resliceTransform->Translate(-vtkImageOrigin[0], -vtkImageOrigin[1], -vtkImageOrigin[2]);
		m_resliceTransform->Concatenate(m_resliceBaseTransform);
		m_resliceTransform->Translate(vtkImageOrigin);
		m_resliceTransform->Concatenate(m_additionalTransform);

		m_resliceNormals = d33vtkMatrixType::New();
		m_resliceNormals->Identity();
		for (int i = 0; i < 3; i++)
		{
			double normal[3];
			normal[0] = m_resliceNormals->GetElement(i, 0);
			normal[1] = m_resliceNormals->GetElement(i, 1);
			normal[2] = m_resliceNormals->GetElement(i, 2);

			m_reslicePlanes[i]->SetNormal(normal);
			m_reslicePlanes[i]->SetOrigin(0, 0, 0);
			m_reslicePlanesInversed[i]->SetNormal(normal);
			m_reslicePlanesInversed[i]->SetOrigin(0, 0, 0);

			m_resliceMapper[i]->SetInputData(m_vtkImageData);
			m_resliceMapper[i]->SetSlicePlane(m_reslicePlanes[i]);
			m_resliceMapper[i]->AutoAdjustImageQualityOn();

			m_reslicers[i]->SetMapper(m_resliceMapper[i]);
			m_reslicers[i]->GetProperty()->SetInterpolationTypeToCubic();
			m_reslicers[i]->SetUserTransform(m_resliceTransform);
		}

		// Prepare the mesh transform for vtkImageData from itkImageToVTKFilter Output
		m_meshTransform->Identity();
		m_meshTransform->PostMultiply();
		m_meshTransform->Translate(-vtkImageOrigin[0], -vtkImageOrigin[1], -vtkImageOrigin[2]);
		m_meshTransform->Concatenate(m_resliceBaseTransform);
		m_meshTransform->Translate(vtkImageOrigin);
		m_meshTransform->Concatenate(m_additionalTransform);

		// Handles reverse trasnfromation
		vtkMatrix4x4::Invert(m_additionalTransform->GetMatrix(), m_invertAdditionalTransform_);
		vtkMatrix4x4::Invert(m_resliceBaseTransform->GetMatrix(), m_invertResliceBaseTransform_);
		vtkMatrix4x4::Invert(m_resliceTransform->GetMatrix(), m_invertResliceTransform_);
		m_invertTranslation_->Identity();
		for (int i = 0; i < 3; i++) m_invertTranslation_->SetElement(i, 3, -m_vtkImageData->GetOrigin()[i]);

		M_MODIFIED = false;

		// free used variables
	}

	if (M_TRANSFORMED) {
		// Extra stuff to do when transformed
	}
}


template<typename TInputImage>
itk::SmartPointer<TInputImage> itkvtkConvert<TInputImage>::GetOutputImage()
{
	if (M_TRANSFORMED) {
		// Set up preliminary information of original image header
		d44vtkMatrixType outputTransformMatrix = d44vtkMatrixType::New();
		d44vtkMatrixType headerTransformMatrix = d44vtkMatrixType::New();
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				headerTransformMatrix->SetElement(i, j, m_direction_(i, j));
			}
			// Translation components and bottom row allocation
			headerTransformMatrix->SetElement(i, 3, m_inItkImage->GetOrigin()[i]);
			headerTransformMatrix->SetElement(3, i, 0.);
		}
		headerTransformMatrix->SetElement(3, 3, 1);
		vtkMatrix4x4::Multiply4x4(
			m_additionalTransform->GetMatrix(),
			headerTransformMatrix,
			outputTransformMatrix);

		// renew header
		typedef itk::ChangeInformationImageFilter<TInputImage> InfoChangeFilterType;
#ifdef _WIN64
		InfoChangeFilterType::Pointer infoChangeFilter = InfoChangeFilterType::New();
#else
		typename InfoChangeFilterType::Pointer infoChangeFilter = InfoChangeFilterType::New();
#endif
		infoChangeFilter->SetInput(m_inItkImage);
		infoChangeFilter->SetChangeOrigin(1);
		infoChangeFilter->SetChangeDirection(1);
		double newOrigin[3];
		d33itkMatrixType newDirection;
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				newDirection(i, j) = outputTransformMatrix->GetElement(i, j);
			}
			newOrigin[i] = outputTransformMatrix->GetElement(i, 3);
		}
		infoChangeFilter->SetOutputDirection(newDirection);
		infoChangeFilter->SetOutputOrigin(newOrigin);
		infoChangeFilter->UpdateOutputInformation();
		infoChangeFilter->Update();

		// Place output to class variable
		m_outItkImage = infoChangeFilter->GetOutput();
	}
	return m_outItkImage;
}

template<typename TInputImage>
inline vtkSmartPointer<vtkPlane> itkvtkConvert<TInputImage>::GetResliceObliqueInversedPlane(int index)
{
	return m_reslicePlanesInversed[index];
}

template<typename TInputImage>
inline vtkSmartPointer<vtkImageResliceMapper>* itkvtkConvert<TInputImage>::GetReslicerMappers()
{
	return m_resliceMapper;
}

template<typename TInputImage>
inline vtkSmartPointer<vtkImageResliceMapper> itkvtkConvert<TInputImage>::GetReslicerMapper(int index)
{
	return m_resliceMapper[index];
}

template<typename TInputImage>
inline vtkSmartPointer<vtkImageSlice>* itkvtkConvert<TInputImage>::GetReslicers()
{
	return m_reslicers;
}

template<typename TInputImage>
inline vtkSmartPointer<vtkImageSlice> itkvtkConvert<TInputImage>::GetReslicer(int index)
{
	return m_reslicers[index];
}

template<typename TInputImage>
inline vtkSmartPointer<vtkPlane>* itkvtkConvert<TInputImage>::GetReslicePlanes()
{
	return m_reslicePlanes;
}

template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::Modified()
{
	M_MODIFIED = true;
}

template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::Transformed()
{
	M_TRANSFORMED = true;
}

#ifdef BOOST_BUILD
template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::_ElapsedTime(std::string tag, std::string msg)
{
	char outMsg[200];
	sprintf(outMsg, "[%s] %.5f - %s", tag.c_str(), m_timer.elapsed(), msg.c_str());
	cout << outMsg << endl;
}

template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::_StartCountPoint()
{
	_StartTime = m_timer.elapsed();
}

template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::_Time(std::string tag, std::string msg)
{
	double timePast = m_timer.elapsed() - _StartTime;
	char outMsg[200];
	sprintf(outMsg, "[%s] %.5f - %s", tag.c_str(), timePast, msg.c_str());
	cout << outMsg << endl;
}
#else // If no boost these function just spend one clock cycle
template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::_ElapsedTime(std::string tag, std::string msg)
{
	return;
}

template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::_StartCountPoint()
{
	return;
}

template<typename TInputImage>
inline void itkvtkConvert<TInputImage>::_Time(std::string tag, std::string msg)
{
	return;
}
#endif // BOOST_BUILD


template<typename TInputImage>
inline vtkSmartPointer<vtkTransform> itkvtkConvert<TInputImage>::_GetWorldToITKIndexTransform()
{
	if (!M_POINT_TO_INDEX) {
		_buildIndexVsPointConversionMatrix();
	}
	return m_worldToITKIndex_;
}

template<typename TInputImage>
inline vtkSmartPointer<vtkTransform> itkvtkConvert<TInputImage>::_GetWorldToITKModelTransform()
{
	if (!M_INDEX_TO_POINT) {
		_buildIndexVsPointConversionMatrix();
	}
	return m_worldToITKCoord_;
}

template<typename TInputImage>
d44vtkMatrixType itkvtkConvert<TInputImage>::_GetIndexToPointMatrix()
{
	if (!M_INDEX_TO_POINT) {
		_buildIndexVsPointConversionMatrix();
	}
	return m_indexToPoint_;
}




template<typename TInputImage>
d44vtkMatrixType itkvtkConvert<TInputImage>::_GetPointToIndexMatrix()
{
	if (!M_POINT_TO_INDEX) {
		_buildIndexVsPointConversionMatrix();
	}
	return m_pointToIndex_;
}

template<typename TInputImage>
void itkvtkConvert<TInputImage>::_buildIndexVsPointConversionMatrix()
{
	double* spacing;
	spacing = m_vtkImageData->GetSpacing();
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			m_indexToPoint_->SetElement(i, j, spacing[j] * m_direction_(i, j));
			m_indexToPoint_->SetElement(i, 3, 0.);
			m_indexToPoint_->SetElement(3, i, 0.);
		}
	}

	m_worldToITKIndex_ = vtkSmartPointer<vtkTransform>::New();
	m_worldToITKIndex_->Identity();
	m_worldToITKIndex_->PostMultiply();
	m_worldToITKIndex_->Concatenate(m_additionalTransform->GetLinearInverse());
	m_worldToITKIndex_->Translate(-m_vtkImageData->GetOrigin()[0], -m_vtkImageData->GetOrigin()[1], -m_vtkImageData->GetOrigin()[2]);

	m_worldToITKCoord_ = vtkSmartPointer<vtkTransform>::New();
	m_worldToITKCoord_->DeepCopy(m_worldToITKIndex_);


	M_INDEX_TO_POINT = true;
	vtkMatrix4x4::Invert(m_indexToPoint_, m_pointToIndex_);
	m_worldToITKIndex_->Concatenate(m_pointToIndex_);
	M_POINT_TO_INDEX = true;
}

#endif //ITKVTKCONVERT_H