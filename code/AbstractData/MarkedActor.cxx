#include "MarkedActor.h"

#ifdef _WIN64
vtkStandardNewMacro(MarkedActor);
#else
template <typename TInImage>
vtkStandardNewMacro(myStyle<TInImage>);
#endif

MarkedActor::MarkedActor()
{
	SourceData = NULL;
}

MarkedActor::~MarkedActor()
{
}
