/*
Author:		Wong, Matthew Lun
Date:		26th, May 2016
Occupation:	Chinese University of Hong Kong,
Department of Imaging and Inteventional Radiology,
Junior Research Assistant


The abstract data class is designed to hold data structure typically used in a medical viewer.
This class is parent of all image-related data.


Wong Matthew Lun
Copyright (C) 2016
*/

#include "AbstractImage.h"


AbstractImage::AbstractImage()
{
	m_i2vConverter = itkvtkConvert<ImageType>::New();
	m_imageType = NONE_TYPE;
}

AbstractImage::~AbstractImage()
{
}

template< typename TImage > void AbstractImage::ReadNiiImage(QString niiFilePath)
{
	if (m_imageType == LABEL_MAP) {
		// Do nothing 
	}
	else if (m_imageType == FLOAT_IMAGE) {
		typedef TImage										   TImageType;
		typedef itk::ImageFileReader< TImageType >			   TImageReaderType;
		typedef itk::OrientImageFilter< TImageType, ImageType > TOrienterType;

		TImageReaderType::Pointer reader = TImageReaderType::New();
		reader->SetFileName(niiFilePath.toStdString().c_str());
		reader->Update();

		TOrienterType::Pointer orienter = TOrienterType::New();
		orienter->UseImageDirectionOn();
		orienter->SetDesiredCoordinateOrientation(itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_RAI);
		orienter->SetInput(reader->GetOutput());
		orienter->Update();

		m_itkImage->Graft(orienter->GetOutput());
	}
	else if (m_imageType == DOUBLE_IMAGE) {
		// do nothing 
	}
}

template< typename TImage > void AbstractImage::ReadDcmImage(QStringList* dcmFileList)
{
	if (m_imageType == NONE_TYPE) {
		std::cerr << "AbstractImage class cannot be used directly without a derived class";
		throw IMAGE_TYPE_ERROR;
	}

	if (m_imageType == LABEL_MAP) {
		// Do nothing 
	}
	else if (m_imageType == FLOAT_IMAGE) {
		typedef TImage										   TImageType;
		typedef itk::ImageSeriesReader< TImageType >		   TImageReaderType;
		typedef itk::OrientImageFilter< TImageType, ImageType > TOrienterType;
		//typedef itk::FlipImageFilter< TImageType>			   TFlipImageType;

		typedef std::vector<std::string>    FileNamesContainer;
		FileNamesContainer fileNames;

		for (int i = 0; i < dcmFileList->size(); i++)
			fileNames.push_back(dcmFileList->at(i).toStdString());

		//Read DICOM image
		TImageReaderType::Pointer reader = TImageReaderType::New();
		reader->SetFileNames(fileNames);
		reader->Update();

		TOrienterType::Pointer orienter = TOrienterType::New();
		orienter->UseImageDirectionOn();
		orienter->SetDesiredCoordinateOrientation(itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_RAI);
		orienter->SetInput(reader->GetOutput());
		orienter->Update();

		m_itkImage->Graft(orienter->GetOutput());
	}
	else if (m_imageType == DOUBLE_IMAGE) {
		// do nothing 
	}
}

void AbstractImage::SetImage(QString niiFilePath)
{
	itk::ImageIOBase::Pointer imageIO = itk::ImageIOFactory::CreateImageIO(niiFilePath.toStdString().c_str(), itk::ImageIOFactory::ReadMode);

	imageIO->SetFileName(niiFilePath.toStdString().c_str());
	imageIO->ReadImageInformation();

	typedef itk::ImageIOBase::IOComponentType IOComponentType;
	const IOComponentType componentType = imageIO->GetComponentType();

	switch (componentType)
	{
	default:
		std::cerr << "Unknown and unsupported component type!" << std::endl;
		throw INPUT_FILE_PATH_ERROR;
	case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
		std::cerr << "Unknown and unsupported component type!" << std::endl;
		throw INPUT_FILE_PATH_ERROR;

	case itk::ImageIOBase::UCHAR: {
		typedef unsigned char PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadNiiImage< TImageType >(niiFilePath);
		break;
	}
	case itk::ImageIOBase::CHAR: {
		typedef char PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadNiiImage< TImageType >(niiFilePath);
		break;
	}
	case itk::ImageIOBase::USHORT: {
		typedef unsigned short PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadNiiImage< TImageType >(niiFilePath);
		break;
	}
	case itk::ImageIOBase::SHORT: {
		typedef short PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadNiiImage< TImageType >(niiFilePath);
		break;
	}
	case itk::ImageIOBase::UINT:{
		typedef unsigned int PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadNiiImage< TImageType >(niiFilePath);
		break; 
	}
	case itk::ImageIOBase::INT: {
		typedef int PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadNiiImage< TImageType >(niiFilePath);
		break;
	}
	case itk::ImageIOBase::ULONG: {
		typedef unsigned long PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadNiiImage< TImageType >(niiFilePath);
		break;
	}
	case itk::ImageIOBase::LONG: {
		typedef long PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadNiiImage< TImageType >(niiFilePath);
		break;
	}
	case itk::ImageIOBase::FLOAT: {
		typedef float PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadNiiImage< TImageType >(niiFilePath);
		break;
	}
	case itk::ImageIOBase::DOUBLE: {
		typedef double PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadNiiImage< TImageType >(niiFilePath);
		break;
	}
	}

	this->InstallPipeline();
}

void AbstractImage::SetImage(QStringList* dcmFileList)
{
	itk::ImageIOBase::Pointer imageIO = itk::ImageIOFactory::CreateImageIO(dcmFileList->at(0).toStdString().c_str(), itk::ImageIOFactory::ReadMode);

	imageIO->SetFileName(dcmFileList->at(0).toStdString().c_str());
	imageIO->ReadImageInformation();

	typedef itk::ImageIOBase::IOComponentType IOComponentType;
	const IOComponentType componentType = imageIO->GetComponentType();

	switch (componentType)
	{
	default:
		std::cerr << "Unknown and unsupported component type!" << std::endl;
		throw INPUT_FILE_PATH_ERROR;
		break;
	case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
		std::cerr << "Unknown and unsupported component type!" << std::endl;
		throw INPUT_FILE_PATH_ERROR;
		break;
	case itk::ImageIOBase::UCHAR: {
		typedef unsigned char PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadDcmImage< TImageType >(dcmFileList);
		break;
	}
	case itk::ImageIOBase::CHAR: {
		typedef char PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadDcmImage< TImageType >(dcmFileList);
		break;
	}
	case itk::ImageIOBase::USHORT: {
		typedef unsigned short PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadDcmImage< TImageType >(dcmFileList);
		break;
	}
	case itk::ImageIOBase::SHORT: {
		typedef short PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadDcmImage< TImageType >(dcmFileList);
		break;
	}
	case itk::ImageIOBase::UINT: {
		typedef unsigned int PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadDcmImage< TImageType >(dcmFileList);
		break;
	}
	case itk::ImageIOBase::INT: {
		typedef int PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadDcmImage< TImageType >(dcmFileList);
		break;
	}
	case itk::ImageIOBase::ULONG: {
		typedef unsigned long PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadDcmImage< TImageType >(dcmFileList);
		break;
	}
	case itk::ImageIOBase::LONG: {
		typedef long PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadDcmImage< TImageType >(dcmFileList);
		break;
	}
	case itk::ImageIOBase::FLOAT: {
		typedef float PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadDcmImage< TImageType >(dcmFileList);
		break;
	}
	case itk::ImageIOBase::DOUBLE: {
		typedef double PixelType;
		typedef itk::Image< PixelType, 3 > TImageType;
		ReadDcmImage< TImageType >(dcmFileList);
		break;
	}
	}

	this->InstallPipeline();
}

void AbstractImage::SetOpacity(double opacity)
{
	for (int i = 0; i<3; i++)
	{
		m_viewProp[i]->GetProperty()->SetOpacity(opacity);
	}
}

void AbstractImage::SetVisibility(bool b)
{
	for (int i = 0; i<3; i++)
	{
		m_viewProp[i]->SetVisibility(b);
	}
}

void AbstractImage::SetPosition(double *pos, int anchor)
{
	m_i2vConverter->SetPosition(pos, anchor);
}

void AbstractImage::SaveImageAsNifti(QString niiPath)
{
	m_i2vConverter->Transformed();

	NiftiWriterType::Pointer writer = NiftiWriterType::New();
	writer->SetFileName(niiPath.toStdString().c_str());
	writer->SetInput(m_i2vConverter->GetOutputImage());
	writer->SetUseInputMetaDataDictionary(true);
	writer->UpdateLargestPossibleRegion();
}

void AbstractImage::SaveImageAsDicom(QString dir, PatientInfo info)
{
	m_i2vConverter->Transformed();

	typedef itk::Image<short, 3>							 Image3DType;
	typedef itk::Image<short, 2>							 Image2DType;
	//typedef itk::ShiftScaleImageFilter<ImageType, ImageType> ShiftScaleType;
	typedef itk::CastImageFilter<ImageType, Image3DType>	 CastImagetype;
	typedef itk::ImageFileWriter<Image2DType>                DcmWriterType;
	typedef itk::GDCMImageIO                                 ImageIOType;

	CastImagetype::Pointer castFilter = CastImagetype::New();
	castFilter->SetInput(m_itkImage);
	castFilter->Update();

	Image3DType::Pointer savingImage = castFilter->GetOutput();

	Image3DType::SpacingType   spacing = savingImage->GetSpacing();
	Image3DType::DirectionType oMatrix = savingImage->GetDirection();

	typedef itk::MetaDataDictionary DictionaryType;
	unsigned int numberOfSlices = savingImage->GetLargestPossibleRegion().GetSize()[2];

	ImageIOType::Pointer gdcmIO = ImageIOType::New();
	DictionaryType       dictionary;

	float progress = 1.0 / (float)numberOfSlices;
	for (unsigned int i = 0; i < numberOfSlices; i++)
	{
		std::ostringstream value;
		Image3DType::PointType    origin;
		Image3DType::IndexType    index;
		index.Fill(0);
		index[2] = i;
		savingImage->TransformIndexToPhysicalPoint(index, origin);

		// Set all required DICOM fields
		value.str("");
		value << origin[0] << "\\" << origin[1] << "\\" << origin[2];
		itk::EncapsulateMetaData<std::string>(dictionary, "0020|0032", value.str());

		value.str("");
		value << i + 1;
		itk::EncapsulateMetaData<std::string>(dictionary, "0020|0013", value.str());

		itk::EncapsulateMetaData<std::string>(dictionary, "0008|0008", std::string("ORIGINAL\\PRIMARY\\AXIAL"));  // Image Type
		itk::EncapsulateMetaData<std::string>(dictionary, "0008|0016", std::string("1.2.840.10008.5.1.4.1.1.2")); // SOP Class UID
																												  //itk::EncapsulateMetaData<std::string>(dictionary, "0010|0030", std::string("20060101"));                  //Patient's Birthdate
																												  //itk::EncapsulateMetaData<std::string>(dictionary, "0008|0020", std::string("20050101"));                  // Study Date
																												  //itk::EncapsulateMetaData<std::string>(dictionary, "0008|0030", std::string("010100.000000"));             // Study Time
		itk::EncapsulateMetaData<std::string>(dictionary, "0008|0050", std::string("1"));                         //Accession Number
		itk::EncapsulateMetaData<std::string>(dictionary, "0018|5100", std::string("HFS"));                       //Patient Position 
		value.str("");
		value << oMatrix[0][0] << "\\" << oMatrix[1][0] << "\\" << oMatrix[2][0] << "\\";
		value << oMatrix[0][1] << "\\" << oMatrix[1][1] << "\\" << oMatrix[2][1];
		itk::EncapsulateMetaData<std::string>(dictionary, "0020|0037", value.str()); // Image Orientation (Patient)
		value.str("");
		value << spacing[2];
		itk::EncapsulateMetaData<std::string>(dictionary, "0018|0050", value.str()); // Slice Thickness

																					 // Parameters from the command line
		if (info.name.size()>0)
		{
			itk::EncapsulateMetaData<std::string>(dictionary, "0010|0010", info.name.toStdString().c_str()); //Patient Name
		}
		if (info.id.size()>0)
		{
			itk::EncapsulateMetaData<std::string>(dictionary, "0010|0020", info.id.toStdString().c_str()); //Patient ID
		}
		if (info.sex.size() > 0)
		{
			itk::EncapsulateMetaData<std::string>(dictionary, "0010|0040", info.sex.toStdString().c_str()); //Patient Sex
		}
		if (info.age.size() > 0)
		{
			itk::EncapsulateMetaData<std::string>(dictionary, "0010|1010", info.age.toStdString().c_str()); //Patient Age
		}
		//if (patientComments.size() > 0)
		//{
		//	itk::EncapsulateMetaData<std::string>(dictionary, "0010|4000", patientComments);
		//}
		//if (studyID.size() > 0)
		//{
		//	itk::EncapsulateMetaData<std::string>(dictionary, "0020|0010", studyID);
		//}
		//if (studyDate.size() > 0)
		//{
		//	itk::EncapsulateMetaData<std::string>(dictionary, "0008|0020", studyDate);
		//}
		if (info.description.size() > 0)
		{
			itk::EncapsulateMetaData<std::string>(dictionary, "0032|4000", info.description.toStdString().c_str());  //Study Comment
		}
		if (true)
		{
			itk::EncapsulateMetaData<std::string>(dictionary, "0008|1030", "Brachytherapy-TPNS");   //Study Description
		}
		//if (modality.size() > 0)
		//{
		//	itk::EncapsulateMetaData<std::string>(dictionary, "0008|0060", modality);
		//}
		//if (manufacturer.size() > 0)
		//{
		//	itk::EncapsulateMetaData<std::string>(dictionary, "0008|0070", manufacturer);
		//}
		//if (model.size() > 0)
		//{
		//	itk::EncapsulateMetaData<std::string>(dictionary, "0008|1090", model);
		//}
		//if (seriesNumber.size() > 0)
		//{
		//	itk::EncapsulateMetaData<std::string>(dictionary, "0020|0011", seriesNumber);
		//}
		if (m_uniqueName.toStdString().size() > 0)
		{
			itk::EncapsulateMetaData<std::string>(dictionary, "0008|103e", m_uniqueName.toStdString().c_str());  //Series Description
		}

		// Always set the rescale interscept and rescale slope (even if
		// they are at their defaults of 0 and 1 respectively).
		// value.str("");
		// value << rescaleIntercept;
		// itk::EncapsulateMetaData<std::string>(dictionary, "0028|1052", value.str());
		// value.str("");
		// value << rescaleSlope;
		// itk::EncapsulateMetaData<std::string>(dictionary, "0028|1053", value.str());

		Image3DType::RegionType extractRegion;
		Image3DType::SizeType   extractSize;
		Image3DType::IndexType  extractIndex;
		extractSize = savingImage->GetLargestPossibleRegion().GetSize();
		extractIndex.Fill(0);
		extractIndex[2] = i;
		extractSize[2] = 0;
		extractRegion.SetSize(extractSize);
		extractRegion.SetIndex(extractIndex);

		typedef itk::ExtractImageFilter<Image3DType, Image2DType> ExtractType;
		ExtractType::Pointer extract = ExtractType::New();
		extract->SetDirectionCollapseToGuess();  // ITKv3 compatible, but not recommended
		extract->SetInput(savingImage);
		extract->SetExtractionRegion(extractRegion);
		extract->GetOutput()->SetMetaDataDictionary(dictionary);
		extract->Update();

		itk::ImageRegionIterator<Image2DType> it(extract->GetOutput(), extract->GetOutput()->GetLargestPossibleRegion());
		Image2DType::PixelType                minValue = itk::NumericTraits<Image2DType::PixelType>::max();
		Image2DType::PixelType                maxValue = itk::NumericTraits<Image2DType::PixelType>::min();
		for (it.GoToBegin(); !it.IsAtEnd(); ++it)
		{
			Image2DType::PixelType p = it.Get();
			if (p > maxValue)
			{
				maxValue = p;
			}
			if (p < minValue)
			{
				minValue = p;
			}
		}

		Image2DType::PixelType windowCenter = (minValue + maxValue) / 2;
		Image2DType::PixelType windowWidth = (maxValue - minValue);

		value.str("");
		value << windowCenter;
		itk::EncapsulateMetaData<std::string>(dictionary, "0028|1050", value.str());
		value.str("");
		value << windowWidth;
		itk::EncapsulateMetaData<std::string>(dictionary, "0028|1051", value.str());

		DcmWriterType::Pointer writer = DcmWriterType::New();
		char                imageNumber[BUFSIZ];
		std::string dicomNumberFormat = "%04d";
		std::string dicomPrefix;
		dicomPrefix.append(m_uniqueName.toStdString());
		dicomPrefix.append("_");

#if WIN32
#define snprintf sprintf_s
#endif
		snprintf(imageNumber, BUFSIZ, dicomNumberFormat.c_str(), i + 1);
		value.str("");
		value << dir.toStdString() << "/" << dicomPrefix << imageNumber << ".dcm";
		writer->SetFileName(value.str().c_str());

		writer->SetInput(extract->GetOutput());
		writer->SetUseCompression(false);
		try
		{
			writer->SetImageIO(gdcmIO);
			writer->Update();
		}
		catch (itk::ExceptionObject & excp)
		{
			std::cerr << "Exception thrown while writing the file " << std::endl;
			std::cerr << excp << std::endl;
			return;
		}
	}
}

int AbstractImage::GetImageType()
{
	return m_imageType;
}

void AbstractImage::GetPosition(double* pos)
{
	double curPos[3];
	m_i2vConverter->GetCurrentPos(curPos);

	pos[0] = curPos[0];
	pos[1] = curPos[1];
	pos[2] = curPos[2];

}

double * AbstractImage::GetPosition()
{
	double* pos = (double*)malloc(sizeof(double) * 3);
	m_i2vConverter->GetCurrentPos(pos);
	return pos;
}


vtkMatrix4x4* AbstractImage::GetMatrix()
{
	return m_i2vConverter->GetResliceTransform()->GetMatrix();
}

void AbstractImage::SetMatrix(vtkMatrix4x4* matrix)
{
	m_i2vConverter->GetResliceTransform()->GetMatrix()->DeepCopy(matrix);
}

vtkMatrix4x4* AbstractImage::GetInverseMatrix()
{
	return m_i2vConverter->GetInverseResliceTransform()->GetMatrix();
}

vtkTransform* AbstractImage::GetTransform()
{
	return m_i2vConverter->GetAdditionalTransform();
}


bool AbstractImage::GetVisibility()
{
	return m_i2vConverter->GetViewProp(0)->GetVisibility();
}


vtkTransform* AbstractImage::GetObliqueTransform()
{
	return m_i2vConverter->GetObliqueTransform();
}

double AbstractImage::GetOpacity()
{
	return m_i2vConverter->GetViewProp(0)->GetProperty()->GetOpacity();
}

double* AbstractImage::GetSpacing()
{
	return m_i2vConverter->GetVtkImage()->GetSpacing();
}


vtkProp* AbstractImage::GetImageViewProp(int n)
{
	return m_i2vConverter->GetViewProp(n);
}

itkvtkConvert<ImageType>::Pointer AbstractImage::GetReslicer()
{
	return m_i2vConverter;
}

void AbstractImage::InstallPipeline()
{
	if (m_imageType == FLOAT_IMAGE) {
		m_i2vConverter->SetInput(m_itkImage);
		m_i2vConverter->Update();

		vtkImageData* imageData = m_i2vConverter->GetVtkImage();

		int extent[6];
		double spacing[3];
		double origin[3];

		imageData->GetExtent(extent);
		imageData->GetSpacing(spacing);
		imageData->GetOrigin(origin);

		int centerSlice[3];
		centerSlice[0] = (extent[0] + extent[1]) / 2;
		centerSlice[1] = (extent[2] + extent[3]) / 2;
		centerSlice[2] = (extent[4] + extent[5]) / 2;

		double center[3];
		center[0] = origin[0] + spacing[0] * centerSlice[0];
		center[1] = origin[1] + spacing[1] * centerSlice[1];
		center[2] = origin[2] + spacing[2] * centerSlice[2];

		//Set Center as initial
		m_i2vConverter->SetPosition(center);

		for (int i = 0; i < 3; i++)
		{
			m_i2vConverter->GetViewProp(i)->Update();
			m_viewProp[i] = m_i2vConverter->GetViewProp(i);
		}

	}
	else {
		throw IMAGE_TYPE_ERROR;
	}
}

