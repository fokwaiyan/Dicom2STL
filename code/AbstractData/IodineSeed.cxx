/*
This programe is a class of IodineSeed which is a componenet of the TPS Module

Copyright (C) 2015	Wong Matthew Lun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "IodineSeed.h"
#include <itkImageFileReader.h>

#ifndef ARMERSHAM_6711
#define ARMERSHAM_6711


/************************************************
Amersham Health model 6711 Data for interpolation
************************************************/
// 2D Anistropy Function F(r,theta)
const double g_thetaTableF[66] = { 0.333, 0.370, 0.442, 0.488, 0.520, 0.550,
									0.400, 0.429, 0.497, 0.535, 0.561, 0.587,
									0.519, 0.537, 0.580, 0.609, 0.630, 0.645,
									0.716, 0.705, 0.727, 0.743, 0.752, 0.760,
									0.846, 0.834, 0.842, 0.846, 0.848, 0.852,
									0.926, 0.925, 0.926, 0.926, 0.928, 0.928,
									0.972, 0.972, 0.970, 0.969, 0.969, 0.969,
									0.991, 0.991, 0.987, 0.987, 0.987, 0.987,
									0.996, 0.996, 0.996, 0.995, 0.995, 0.995,
									1.000, 1.000, 1.000, 0.999, 0.999, 0.999,
									1.000, 1.000, 1.000, 1.000, 1.000, 1.000 };

const double g_thetaF[11] = { 0, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90 };

const double g_rF[6] = { 5, 10, 20, 30, 40, 50 };

// Line Source Dose-rate Constant cGy/(hU)
const double g_doseRateConstant = 0.965;

// Line Source Radial-dose function g(r)
const double g_radialDoseTableg[16] = { 1.055, 1.078, 1.082, 1.071, 1.042,
										1,0.908, 0.814, 0.632, 0.496, 0.364,
										0.270,0.199, 0.148, 0.109,0.0803 };

const double g_rg[16] = { 1, 1.5, 2.5, 5, 7.5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100 };

// Some Constant
const double g_Pi = 3.14159265359;
const double g_halfLife = 59.4; // in days
const double g_kermaTomCiConvertionConstant = 0.787; 

/***********************************************/
#endif // !ARMERSHAM_6711

IodineSeed::IodineSeed() : Landmark()
{
	_ISINIT_g = FALSE;
	_ISINIT_F = FALSE;
	_BASE_VOLUME = FALSE;
	_IS_UPDATE = FALSE;

	double coord[3] = { 0,0,0 };
	double spacing[3] = { 0.1,0.1,0.1 };
	double orientation[3] = { 1,0,0 };
	double seedActivity = 1.;
	double seedLength = 3.;
	//SetPosition(m_coord);
	memcpy(m_worldPosition, m_normal, sizeof(double)*3);
	memcpy(_spacing, spacing, sizeof(double) * 3);
	m_seedActivity = seedActivity;
	m_seedLength = seedLength;
	m_seedInsertSequence = 0;
	m_decayTimeStamp = 0;
	m_halfLife = g_halfLife;
	m_uTomCiConvertionConstant = g_kermaTomCiConvertionConstant;
	this->m_shape = Landmark::Ellipsode;
//#if _DEBUG
	//std::string templateSeedPath = "../../Project/resources/iodineSeed/seed_base_volume_80_5.nii.gz";
//#else
//	std::string templateSeedPath = "../../../Project/resources/iodineSeed/seed_base_volume_80_5.nii.gz";
//	//std::string templateSeedPath = "C:/Users/Mat/Desktop/Brachytherapy-TPNS/Project/resources/iodineSeed/templateSeed_5.nii.gz";
//#endif
//	typedef itk::Image<float, 3> ImageType;
//	typedef itk::ImageFileReader< ImageType>			   ImageReaderType;
//
//	ImageReaderType::Pointer reader = ImageReaderType::New();
//	reader->SetFileName(templateSeedPath.c_str());
//	reader->Update();
//
//	SetBaseVolume(reader->GetOutput());
}

IodineSeed::IodineSeed(double* coord, double* orientation, double seedActivity, double seedLength, int seedInsertSequence) : Landmark()
{
	memcpy(m_worldPosition, coord, sizeof(double)*3);
	memcpy(m_normal, orientation, sizeof(double) * 3);
	m_seedActivity = seedActivity;
	m_seedLength = seedLength;
	m_seedInsertSequence = seedInsertSequence;
	m_decayTimeStamp = 0;
	m_halfLife = g_halfLife;
	m_uTomCiConvertionConstant = g_kermaTomCiConvertionConstant;
	this->m_shape = Landmark::Ellipsode;

	return;
}

void IodineSeed::Modified()
{
	_IS_UPDATE = FALSE;
}

void IodineSeed::SetPosition(double coord[3])
{
	Landmark::SetPosition(coord);
}

void IodineSeed::SetPosition(double x, double y, double z)
{
	double pos[3] = { x, y, z };
	this->SetPosition(pos);
}

int IodineSeed::SetOrientation(double xyz[3])
{
	this->SetNormal(xyz);
	return 0;
}

int IodineSeed::SetOrientation(double x, double y, double z)
{
	this->SetNormal(x, y, z);
	return this->SetOrientation(x, y, z);
}

void IodineSeed::SetNormal(double orientation[3])
{
	Landmark::SetNormal(orientation);
}

void IodineSeed::SetNormal(double x, double y, double z)
{
	double orientation[3] = { x, y, z };
	this->SetNormal(orientation);
}

int IodineSeed::SetSpacing(double spacing[3])
{
	for (int i = 0; i < 3; i++)
	{
		_spacing[i] = spacing[i];
	}
	return 0;
}

int IodineSeed::SetSpacing(double x, double y, double z)
{
	double spacing[3] = { x, y, z };
	return this->SetSpacing(spacing);
}

int IodineSeed::SetBounds(double bound[3])
{
	for (int i = 0; i < 3; i++)
	{
		m_bounds[i] = bound[i];
	}
	return 0;
}

int IodineSeed::SetBounds(double x, double y, double z)
{
	double bounds[3] = { x, y, z };
	return this->SetBounds(bounds);
}

int IodineSeed::SetSeedActivity(double activity)
{
	m_seedActivity = activity;
	return 0;
}

int IodineSeed::SetSeedLength(double seedLength)
{
	m_seedLength = seedLength;
	return 0;
}

int IodineSeed::SetSeedInsertSequence(int insertSequence)
{
	m_seedInsertSequence = insertSequence;
	return 0;
}

int IodineSeed::SetBaseVolume(ImageType::Pointer basevolume)
{
	m_baseVolume = basevolume.GetPointer();
	_BASE_VOLUME = TRUE;
	return 0;
}

int IodineSeed::SetDecayTimeStamp(double t)
{
	this->m_decayTimeStamp = t;
	return 1;
}

int IodineSeed::SetHalfLife(double t)
{
	this->m_halfLife = t;
	return 1;
}

double * IodineSeed::GetPosition()
{
	return Landmark::GetPosition();
}

void IodineSeed::Copy(IodineSeed *templateSeed)
{
	SetPosition(templateSeed->GetPosition());
	SetOrientation(templateSeed->GetOrientation());
	SetSeedActivity(templateSeed->GetSeedActivity());
	SetSeedLength(templateSeed->GetSeedLength());
	SetBounds(templateSeed->GetBounds());
	return;
}

double * IodineSeed::GetOrientation()
{
	return this->GetNormal();
}

double * IodineSeed::GetSpacing()
{
	return _spacing;
}

const double * IodineSeed::GetBaseOrientation()
{
	return m_initialNormal;
}

double * IodineSeed::GetBounds()
{
	return m_bounds;
}

double IodineSeed::GetSeedActivity()
{
	return m_seedActivity;
}

double IodineSeed::GetDecayTimeStamp()
{
	return this->m_decayTimeStamp;
}

double IodineSeed::GetHalfLife()
{
	return this->m_halfLife;
}

double IodineSeed::GetUtomCiConversionConstant()
{
	return this->m_uTomCiConvertionConstant;
}

double IodineSeed::GetSeedLength()
{
	return m_seedLength;
}

ImageType::Pointer IodineSeed::GetDoseVolume()
{
	return m_doseVolume;
}

ImageType::Pointer IodineSeed::GetBaseVolume()
{
	return m_baseVolume;
}

ImageType::Pointer IodineSeed::GetOutput()
{
	if (_IS_UPDATE) {
		return m_doseVolume;
	}
	else {
		std::cerr << "[Error] Cannot get output before update or update is unsuccessful";
		throw ERROR_APP_DATA_NOT_FOUND;
	}
}

int IodineSeed::GetSeedInsertSequence()
{
	return m_seedInsertSequence;
}

void IodineSeed::UpdateSeed()
{
	if (_IS_UPDATE) {
		return;
	}

	if (_BASE_VOLUME == FALSE) {
		std::cerr << "Base Volume not loaded.\n";
		throw BASE_VOLUME_MISSING_ERROR;
	}
	if (_GenerateOutVolume()) {
		std::cerr << "[Error] Generate output volume error";
		throw OUT_VOLUME_GENERATE_FAILED;
	}
	_IS_UPDATE = TRUE;
}

void IodineSeed::InstallPipeline()
{
	Landmark::InstallPipeline();
}

void IodineSeed::Create()
{
	Landmark::Create();
}

void IodineSeed::Update()
{
	// Update Source
	Landmark::Update();
}

/*
_GeometricFunctionLine

Purpose: 
	Convert the radius and degree information to model space of seed template and obtain the dose
	information at that particular point.

Parameters:
	:Param [double] radius	The distance between center and calculating point
	:Param [double] theta	The acute angle in radian between the vector from center to calculating 
							point and the vector along line source. **IN RAD**
*/
double IodineSeed::_GeometricFunctionLine(double radius, double theta)
{
	double x, y;
	x = radius * cos(theta);
	y = radius * sin(theta);

	double side1, side2;
	side1 = sqrt(pow(x - m_seedLength / 2., 2.) + pow(y, 2.));
	side2 = sqrt(pow(x + m_seedLength / 2., 2.) + pow(y, 2.));

	double dose;
	double beta;
	if (theta >= 0.00001) {
		beta = acos(
			(pow(m_seedLength, 2.) - pow(side1, 2.) - pow(side2, 2.)) / (-2 * side1 * side2)
			);
		dose = beta / (m_seedLength * radius * sin(theta));
	}
	else {
		if (radius <= m_seedLength / 2.) {
			dose = -1.;
		}
		else {
			dose = 1 / (pow(radius, 2.) - pow(m_seedLength, 2.) / 4.);
		}
	}

	return dose;
}


/*
_GenerateOutVolume

Purpose:
	Generate the volume for output

Details:

	 **Obtaining the transformation**

		 m_a = angle between projected vector on 01-plane (xy) and the 0-axis (x)
		 m_b = angle between new direction vector and original 2-axis (z)
		 Then the rotational matrix to the new coordinate is given by the following:
			m_R = T2(a).T1(b)
		 Where T2 is the rotational matrix about 2-axis

		 Since ITK use right-hand coordinate system, both a and b represents rotation 
		 in counter-clockwise direction.

	 NOTE 
		This exploit the rotational simmetry of the radio active source, it
		is not a general rotation scheme. For more, visit Euler angle wiki page.

Parameters:
	NULL

*/
int IodineSeed::_GenerateOutVolume()
{
	// Check if base volume exsit
	if (_BASE_VOLUME == ITK_NULLPTR) {
		std::cerr << "[Error] Must generate base volume first";
		return EXIT_FAILURE;
	}

	// Update position and direction
	double* pos = this->GetPosition();
	double* ori = this->GetOrientation();
	char msg[200];
	sprintf_s(msg, "pos: [%.3f, %.3f, %.3f] ori: [%.3f, %.3f, %.3f]\n", 
		pos[0], pos[1], pos[2], 
		ori[0], ori[1], ori[2]);
	//cout << msg;

	//vtkMath::Normalize(m_normal);

	//double cross[3], bisector[3], angle, dot;
	//vtkMath::Cross(m_initialNormal, m_normal, cross);
	//vtkMath::Add(m_normal, m_initialNormal, bisector);
	//vtkMath::Normalize(bisector);

	//angle = acos(vtkMath::Dot(m_initialNormal, bisector)) + acos(vtkMath::Dot(bisector, m_normal));
	//angle = vtkMath::DegreesFromRadians(angle);

	//vtkSmartPointer<vtkTransform> trans = vtkSmartPointer<vtkTransform>::New();
	//trans->RotateWXYZ(angle, cross);

	//typedef itk::Matrix<double, 3, 3>  m_matrixType;
	//m_matrixType m_rotationalMatrix;

	// Convert vtk matrix to itk matrix
	vtkSmartPointer<vtkTransform> trans = vtkSmartPointer<vtkTransform>::New();
	trans->GetMatrix()->DeepCopy(this->m_userTransform->GetMatrix());
	trans->GetMatrix()->SetElement(0, 3, 0);
	trans->GetMatrix()->SetElement(1, 3, 0);
	trans->GetMatrix()->SetElement(2, 3, 0);

	typedef itk::Matrix<double, 3, 3>  MatrixType;
	MatrixType m_rotationalMatrix;

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			m_rotationalMatrix(i, j) = trans->GetMatrix()->GetElement(i,j);
		}
	}


	typedef itk::ResampleImageFilter<ImageType, ImageType, double, double> m_resampleType;
	typedef itk::AffineTransform<double, 3> m_transformType;
	typedef itk::Point<double, 3> m_pointType;
	typedef itk::Vector<double, 3> m_translationType;
	m_translationType translation;
	m_pointType finalPoint;
	m_resampleType::Pointer resamplar = m_resampleType::New();
	m_transformType::Pointer transform = m_transformType::New();
	finalPoint.SetElement(0, m_worldPosition[0]);
	finalPoint.SetElement(1, m_worldPosition[1]);
	finalPoint.SetElement(2, m_worldPosition[2]);
	translation.SetElement(0, -m_worldPosition[0]);
	translation.SetElement(1, -m_worldPosition[1]);
	translation.SetElement(2, -m_worldPosition[2]);
	transform->SetTranslation(translation);		// Translate the center first, then rotate
	transform->SetMatrix(m_rotationalMatrix);
	transform->SetCenter(finalPoint);
	double m_origin[3];
	m_origin[0] = m_worldPosition[0] - m_bounds[0] / 2.;
	m_origin[1] = m_worldPosition[1] - m_bounds[1] / 2.;
	m_origin[2] = m_worldPosition[2] - m_bounds[2] / 2.;

	/* Apply the transform with the resamplar */
	resamplar->SetInput(m_baseVolume);
	resamplar->SetTransform(transform);
	resamplar->SetSize(m_baseVolume->GetLargestPossibleRegion().GetSize());
	resamplar->SetOutputSpacing(m_baseVolume->GetSpacing());
	resamplar->SetOutputOrigin(m_origin);
	resamplar->Update();
	
	m_doseVolume = resamplar->GetOutput();
	return EXIT_SUCCESS;
}


double IodineSeed::_GetSmallerDouble(double v1, double v2)
{
	if (v1 < v2) {
		return v1;
	}
	else {
		return v2;
	}
}

double IodineSeed::_GetLargerDouble(double v1, double v2)
{
	if (v1 > v2) {
		return v1;
	}
	else {
		return v2;
	}
}

double* IodineSeed::GetCoordinate()
{
	return m_worldPosition;
}
