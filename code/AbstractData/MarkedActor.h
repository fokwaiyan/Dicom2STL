/*
Author:		Wong, Matthew Lun
Date:		26th, May 2016
Occupation:	Chinese University of Hong Kong,
Department of Imaging and Inteventional Radiology,
Junior Research Assistant

This class is designed to source abstract data from actor alone.

Wong Matthew Lun
Copyright (C) 2016
*/


#ifndef MARKED_ACTOR_H
#define MARKED_ACTOR_H

//#ifndef ABSTRACT_POLYDATA_H
//#endif

#include <vtkActor.h>
#include <vtkSmartPointer.h>
#include <vtkObjectFactory.h>

class AbstractPolyData;

class MarkedActor : public vtkActor
{
public:
	vtkTypeMacro(MarkedActor, vtkActor);
	static MarkedActor *New();

	vtkGetMacro(SourceData, AbstractPolyData*);
	vtkSetMacro(SourceData, AbstractPolyData*);

protected:
	MarkedActor();
	~MarkedActor();

	AbstractPolyData* SourceData;
private:
	MarkedActor(const MarkedActor&);  // Not implemented.
	void operator=(const MarkedActor&);  // Not implemented.
};



#endif