/*
Author:		Wong, Matthew Lun
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
			Department of Imaging and Inteventional Radiology,
			Junior Research Assistant

Author:		Lok, Ka Hei Jason
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
			Department of Imaging and Inteventional Radiology,
			M.Phil Student

This class is an interactor modified from TrackBallActor. It allows interactions with the data type
Seed and controls the orientation/position of it.


Wong Matthew Lun, Lok Ka Hei
Copyright (C) 2016
*/


#include "InteractorStyle3DPickSeed.h"
#include "MainWindow.h"

vtkStandardNewMacro(InteractorStyle3DPickSeed);

void InteractorStyle3DPickSeed::OnKeyPress()
{
	// Get the keypress
	vtkRenderWindowInteractor *rwi = this->Interactor;
	std::string key = rwi->GetKeySym();

	MainWindow* mainWnd = MainWindow::GetMainWindow();

	if (key == "n") {
		// New seed at cursor position
		double pos[3];
		int maxSeedNumber = 0;
		mainWnd->GetCursorPosition(pos);
		QList<IodineSeed*> list = mainWnd->GetPlanner()->GetActualSeedList();

		for (int i = 0; i < list.count(); i++)
		{
			if (list.at(i)->GetUniqueName().remove(0, 1).toInt() > maxSeedNumber)
				maxSeedNumber = list.at(i)->GetUniqueName().remove(0, 1).toInt();
		}

		QString uniqueName;
		uniqueName = "A";
		uniqueName = uniqueName + QString::number(maxSeedNumber + 1);

		mainWnd->GetPostSurgicalAnalysisWidget()->SetActualSeed(uniqueName, pos);
		mainWnd->UpdateSeedTreeWidget();
		mainWnd->RenderAllViewer();
	}
	else if (key == "Delete") {
		// Delete selected seed
		if (m_selectedSeed) {
			// Remove actor first
			for (int i = 0; i < 3; i++) mainWnd->GetViewers(i)->RemoveAnnotationActor(m_selectedSeed->Get2DActor(i));
			mainWnd->GetViewers(3)->RemoveActor(m_selectedSeed->GetActor());

			mainWnd->GetPlanner()->RemoveSeed(m_selectedSeed);
			mainWnd->UpdateSeedTreeWidget();
			m_selectedSeed = NULL;
		}
	}
}

void InteractorStyle3DPickSeed::OnMouseMove()
{
	// Do his only when button is not pressed
	if (!m_leftFunctioning && !m_rightFunctioning && !m_middleFunctioning) {
		MainWindow* mainWnd = MainWindow::GetMainWindow();

		int x = this->Interactor->GetEventPosition()[0];
		int y = this->Interactor->GetEventPosition()[1];
		vtkSmartPointer<vtkCoordinate> coordinate =
			vtkSmartPointer<vtkCoordinate>::New();
		coordinate->SetCoordinateSystemToDisplay();
		coordinate->SetValue(x, y, 0);

		vtkActor* dataActor = this->PickActor(x, y);

		if (dataActor) {
			// Test if a mesh is picked, if so, return
			for (int i = 0; i < mainWnd->GetPlanner()->GetMeshList()->size(); i++)
			{
				if (dataActor == mainWnd->GetPlanner()->GetMeshList()->at(i)->GetActor().GetPointer()) {
					return;
				}
			}
			
			// Find which seed correspond to that actor
			if (m_selectedSeed) {
				// skip if it is the old one
				// otherwise, deselect the old one and select the new one if button is not pressed
				if (dataActor == m_selectedSeed->GetActor().GetPointer()) {
					if (!m_leftFunctioning && !m_middleFunctioning && !m_rightFunctioning) {
						m_selectedSeed->SetActorState(IodineSeed::IDLE);
						for (int i = 0; i < mainWnd->GetPlanner()->GetSeedList()->length(); i++) {
							if (mainWnd->GetPlanner()->GetSeedList()->at(i)->GetActor().GetPointer() == dataActor) {
								m_selectedSeed = mainWnd->GetPlanner()->GetSeedList()->at(i);
								// Change state to hovered if find a seed
								m_selectedSeed->SetActorState(IodineSeed::HOVERED);
								break;
							}
						}
					}
				}
			}
			// if there are no previous selected seed
			else {
				for (int i = 0; i < mainWnd->GetPlanner()->GetSeedList()->length(); i++) {
					if (mainWnd->GetPlanner()->GetSeedList()->at(i)->GetActor().GetPointer() == dataActor) {
						m_selectedSeed = mainWnd->GetPlanner()->GetSeedList()->at(i);
						// Change state to hovered if find a seed
						m_selectedSeed->SetActorState(IodineSeed::HOVERED);
						break;
					}
				}
			}
		}
		else {
			// Resume state and unselect the seed if nothing under the cursor
			if (m_selectedSeed) {
				m_selectedSeed->SetActorState(IodineSeed::IDLE);
				m_selectedSeed = NULL;
			}
		}
	}

	
	vtkInteractorStyleTrackballActor::OnMouseMove();

	//if (m_leftFunctioning || m_rightFunctioning || m_middleFunctioning) {
		//this->UpdateTransform();
	//}

	this->Interactor->Render();
}

void InteractorStyle3DPickSeed::OnLeftButtonDown()
{
	vtkInteractorStyleTrackballActor::OnLeftButtonDown();
	AbstractInteractorStyle::OnLeftButtonDown();
}

void InteractorStyle3DPickSeed::OnRightButtonDown()
{
	vtkInteractorStyleTrackballActor::OnRightButtonDown();
	AbstractInteractorStyle::OnRightButtonDown();
}

void InteractorStyle3DPickSeed::OnMiddleButtonDown()
{
	vtkInteractorStyleTrackballActor::OnMiddleButtonDown();
	AbstractInteractorStyle::OnMiddleButtonDown();
}

void InteractorStyle3DPickSeed::OnLeftButtonUp()
{
	this->UpdateTransform();
	this->CurrentRenderer->Render();
	vtkInteractorStyleTrackballActor::OnLeftButtonUp();
	AbstractInteractorStyle::OnLeftButtonUp();
}

void InteractorStyle3DPickSeed::OnRightButtonUp()
{
	this->UpdateTransform();
	this->CurrentRenderer->Render();
	vtkInteractorStyleTrackballActor::OnRightButtonUp();
	AbstractInteractorStyle::OnRightButtonUp();
}

void InteractorStyle3DPickSeed::OnMiddleButtonUp()
{
	this->UpdateTransform();
	this->CurrentRenderer->Render();
	vtkInteractorStyleTrackballActor::OnMiddleButtonUp();
	AbstractInteractorStyle::OnMiddleButtonUp();
}

InteractorStyle3DPickSeed::InteractorStyle3DPickSeed()
{
	m_clientData = NULL;
	m_selectedSeed = NULL;
}

InteractorStyle3DPickSeed::~InteractorStyle3DPickSeed()
{
}

void InteractorStyle3DPickSeed::UpdateTransform()
{
	// Error Check
	if (!m_selectedSeed)
		return;

	m_selectedSeed->AlignTransform();
	m_selectedSeed->Update();
}

vtkActor * InteractorStyle3DPickSeed::PickActor(int x, int y)
{
	vtkSmartPointer<vtkPropPicker> picker = vtkSmartPointer<vtkPropPicker>::New();
	if (this->Interactor) {
		picker->Pick(x, y, 0, this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer());
	}
	if (picker->GetActor()) {
		return picker->GetActor();
	}
	else {
		return nullptr;
	}
}