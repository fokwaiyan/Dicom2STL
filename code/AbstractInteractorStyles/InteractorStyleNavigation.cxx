/*
Author:		Wong, Matthew Lun
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
			Department of Imaging and Inteventional Radiology,
			Junior Research Assistant

Author:		Lok, Ka Hei Jason
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
			Department of Imaging and Inteventional Radiology,
			M.Phil Student

This class allows interaction related to reslice position and orientation on images.

Wong Matthew Lun, Lok Ka Hei
Copyright (C) 2016
*/

#include "InteractorStyleNavigation.h"

vtkStandardNewMacro(InteractorStyleNavigation);

InteractorStyleNavigation::InteractorStyleNavigation()
{
}

InteractorStyleNavigation::~InteractorStyleNavigation()
{
}

void InteractorStyleNavigation::OnMouseMove()
{
	if (m_rightFunctioning) {
		this->SynchronizedZooming();
	}
	else if (m_leftFunctioning) {
		this->CalculateIndex();
	}

	AbstractInteractorStyleImage::OnMouseMove();
}


void InteractorStyleNavigation::OnLeftButtonUp()
{
	this->CalculateIndex();
	AbstractInteractorStyleImage::OnLeftButtonUp();
}



void InteractorStyleNavigation::SynchronizedZooming()
{

	double scale = m_viewers[m_orientation]->GetDataRenderer()->GetActiveCamera()->GetParallelScale();

	for (int i = 0; i<3; i++)
	{
		m_viewers[i]->GetDataRenderer()->GetActiveCamera()->SetParallelScale(scale);
		m_viewers[i]->Render();
	}
}

void InteractorStyleNavigation::CalculateIndex()
{
	MainWindow *mainWnd = MainWindow::GetMainWindow();

	/// Obtain coordinate from mouse cursor
	if (!this->GetInteractor()) {
		// return if swithc is not prepared
		return;
	}

 	this->GetInteractor()->GetPicker()->Pick(
		this->GetInteractor()->GetEventPosition()[0],
		this->GetInteractor()->GetEventPosition()[1],
		0,
		this->m_viewers[this->m_orientation]->GetDataRenderer()
		);

	/// Transform coordinate to world position
	double* picked = this->GetInteractor()->GetPicker()->GetPickPosition();


	/// Set cursor position 
	if (picked[0] == 0.0 && picked[1] == 0.0) {
		return;
	}
	
	mainWnd->slotChangeCursorPosition(picked[0], picked[1], picked[2], m_orientation);
	//mainWnd->RenderAllViewer();
}

