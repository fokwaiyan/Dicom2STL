/*
Author:		Wong, Matthew Lun
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
Department of Imaging and Inteventional Radiology,
Junior Research Assistant


The abstract interactor class used in medical viewers.
This class contains methods of image related processes, including changing slice position,
zoomming, dragging...etc.


Wong Matthew Lun
Copyright (C) 2016
*/

#include "AbstractInteractorStyleImage.h"
#include "MainWindow.h"

vtkStandardNewMacro(AbstractInteractorStyleImage);

AbstractInteractorStyleImage::AbstractInteractorStyleImage() : AbstractInteractorStyle(), vtkInteractorStyleImage()
{
	double defVal[] = { 0,0,0 };
	memcpy(m_currentPos, defVal, sizeof(double) * 3);
	memcpy(m_previousPosition, defVal, sizeof(int) * 2);
}

AbstractInteractorStyleImage::~AbstractInteractorStyleImage()
{
}

double * AbstractInteractorStyleImage::GetCurrentPos()
{
	return m_currentPos;
}

int * AbstractInteractorStyleImage::GetPreviousPosition()
{
	return m_previousPosition;
}

void AbstractInteractorStyleImage::OnMouseWheelForward()
{
	this->MoveSliceForward();
	//vtkInteractorStyleImage::OnMouseWheelForward();
}

void AbstractInteractorStyleImage::OnMouseWheelBackward()
{
	this->MoveSliceBackward();
	//vtkInteractorStyleImage::OnMouseWheelBackward();
}

void AbstractInteractorStyleImage::OnLeftButtonDown()
{
	AbstractInteractorStyle::OnLeftButtonDown();
	vtkInteractorStyleImage::OnLeftButtonDown();
}

void AbstractInteractorStyleImage::OnLeftButtonUp()
{
	AbstractInteractorStyle::OnLeftButtonUp();
	vtkInteractorStyleImage::OnLeftButtonUp();
}

void AbstractInteractorStyleImage::OnRightButtonDown()
{
	AbstractInteractorStyle::OnRightButtonDown();
	vtkInteractorStyleImage::OnRightButtonDown();

}

void AbstractInteractorStyleImage::OnRightButtonUp()
{
	AbstractInteractorStyle::OnRightButtonUp();
	vtkInteractorStyleImage::OnRightButtonUp();
}

void AbstractInteractorStyleImage::OnMiddleButtonDown()
{
	AbstractInteractorStyle::OnMiddleButtonDown();
	vtkInteractorStyleImage::OnMiddleButtonDown();
}

void AbstractInteractorStyleImage::OnMiddleButtonUp()
{
	AbstractInteractorStyle::OnMiddleButtonUp();
	vtkInteractorStyleImage::OnMiddleButtonUp();
}

void AbstractInteractorStyleImage::OnMouseMove()
{
	vtkInteractorStyleImage::OnMouseMove();
}

void AbstractInteractorStyleImage::OnChar()
{
	vtkInteractorStyleImage::OnChar();
}

void AbstractInteractorStyleImage::OnKeyPressed()
{
	vtkInteractorStyleImage::OnKeyPress();
}

void AbstractInteractorStyleImage::MoveSliceForward()
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	QList<Image*>* imageList = mainWnd->GetPlanner()->GetImageList();

	if (imageList->size() == 0)
		return;

	//Get most updated current value
	mainWnd->GetCursorPosition(m_currentPos);

	//Get the smallest spacing of all images
	double smallestSpacing = INT_MAX;
	for (int i = 0; i<imageList->size(); i++)
	{
		if (imageList->at(i)->GetSpacing()[m_orientation]<smallestSpacing)
			smallestSpacing = imageList->at(i)->GetSpacing()[m_orientation];
	}

	//Get the viewers normal 
	double normal[4] = { 0,0,0,1 };
	memcpy(normal, this->m_viewers[m_orientation]->GetPlane()->GetNormal(), sizeof(double) * 3);;

	// Transform this normal from model space to slice space
	Image* baseImage = mainWnd->GetPlanner()->GetImageByUniqueName("CT");
	if (!baseImage) 
		baseImage = mainWnd->GetPlanner()->GetImageByUniqueName("MRI");
	if (!baseImage) 
		baseImage = mainWnd->GetPlanner()->GetImageByUniqueName("CT+MRI");
	if (!baseImage)
		// if nothing found, return
		return;
	memcpy(normal, baseImage->GetReslicer()->GetObliqueTransform()->GetMatrix()->MultiplyDoublePoint(normal), sizeof(double) * 3);

	//Update the current pos
	m_currentPos[0] = m_currentPos[0] + normal[0] * smallestSpacing;
	m_currentPos[1] = m_currentPos[1] + normal[1] * smallestSpacing;
	m_currentPos[2] = m_currentPos[2] + normal[2] * smallestSpacing;

	int index[3];
	baseImage->GetReslicer()->GetITKIndexFromVTKImageActorPoint(m_currentPos, index);

	//char msg[200];
	//sprintf(msg, "index: %i, %i, %i\n", index[0], index[1], index[2]);
	//cout << msg;

	//Set Spin Box value
	mainWnd->slotChangeCursorPosition(m_currentPos[0], m_currentPos[1], m_currentPos[2]);
	//m_posDoubleSpinBox[0]->setValue(m_currentPos[0]);
	//m_posDoubleSpinBox[1]->setValue(m_currentPos[1]);
	//m_posDoubleSpinBox[2]->setValue(m_currentPos[2]);
	//if(m_slice < m_maxSlice) 
	//{
	//	m_slice += 1;
	//	m_sliceSplinBox[m_orientation]->setValue(m_slice);

	//}	
}

void AbstractInteractorStyleImage::MoveSliceBackward()
{
	MainWindow* mainWnd = MainWindow::GetMainWindow();
	QList<Image*>* imageList = mainWnd->GetPlanner()->GetImageList();

	if (imageList->size() == 0)
		return;

	//Get most updated current value
	mainWnd->GetCursorPosition(m_currentPos);

	//Get the smallest spacing of all images
	double smallestSpacing = INT_MAX;
	for (int i = 0; i<imageList->size(); i++)
	{
		if (imageList->at(i)->GetSpacing()[m_orientation]<smallestSpacing)
			smallestSpacing = imageList->at(i)->GetSpacing()[m_orientation];
	}

	//Get the viewers normal 
	double normal[4] = { 0,0,0,1 };
	memcpy(normal, this->m_viewers[m_orientation]->GetPlane()->GetNormal(), sizeof(double) * 3);;

	// Transform this normal from model space to slice space
	Image* baseImage = mainWnd->GetPlanner()->GetImageByUniqueName("CT");
	if (!baseImage)
		baseImage = mainWnd->GetPlanner()->GetImageByUniqueName("MRI");
	if (!baseImage)
		baseImage = mainWnd->GetPlanner()->GetImageByUniqueName("CT+MRI");
	if (!baseImage)
		// if nothing found, return
		return;
	memcpy(normal, baseImage->GetReslicer()->GetObliqueTransform()->GetMatrix()->MultiplyDoublePoint(normal), sizeof(double) * 3);
	vtkMath::Normalize(normal);

	//Update the current pos
	m_currentPos[0] = m_currentPos[0] - normal[0] * smallestSpacing;
	m_currentPos[1] = m_currentPos[1] - normal[1] * smallestSpacing;
	m_currentPos[2] = m_currentPos[2] - normal[2] * smallestSpacing;

	//Set Spin Box value
	mainWnd->slotChangeCursorPosition(m_currentPos[0], m_currentPos[1], m_currentPos[2]);
	//m_posDoubleSpinBox[0]->setValue(m_currentPos[0]);
	//m_posDoubleSpinBox[1]->setValue(m_currentPos[1]);
	//m_posDoubleSpinBox[2]->setValue(m_currentPos[2]);
	//if(m_slice > m_minSlice) 
	//{
	//	m_slice -= 1;
	//	m_sliceSplinBox[m_orientation]->setValue(m_slice);

	//}
}

