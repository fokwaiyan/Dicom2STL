/*
Author:		Wong, Matthew Lun
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
Department of Imaging and Inteventional Radiology,
Junior Research Assistant


This class is based on vtkInteractorStyleSwitch, written to allow easy 
switching between 2D interactors. 

Wong Matthew Lun
Copyright (C) 2016
*/


#include <vtkImageInterpolator.h>
#include <vtkRendererCollection.h>
#include "InteractorStyleSwitch.h"
#include <vtkCommand.h>
#include <vtkCallbackCommand.h>
#include "ui_MainWindow.h"

vtkStandardNewMacro(InteractorStyleSwitch);

InteractorStyleSwitch::InteractorStyleSwitch()
{
	WindowLevel = InteractorStyleWindowLevel::New();
	Navigation = InteractorStyleNavigation::New();
	//Transform = InteractorStyleTransform::New();
	PolygonDraw = InteractorStylePolygonDraw::New();
	PaintBrush = InteractorStylePaintBrush::New();

	this->m_additionalTransformHolder = vtkSmartPointer<vtkTransform>::New();


	this->CurrentStyle = 0;
}

InteractorStyleSwitch::~InteractorStyleSwitch()
{
	WindowLevel->Delete();
	WindowLevel = 0;

	Navigation->Delete();
	Navigation = 0;

	//Transform->Delete();
	//Transform = 0;

	PolygonDraw->Delete();
	PolygonDraw = 0;

	PaintBrush->Delete();
	PaintBrush = 0;
}

void InteractorStyleSwitch::ImageDirectionOn()
{
	MainWindow* mainwnd = MainWindow::GetMainWindow();

	// Call direction off from planner
	mainwnd->GetPlanner()->ImageDirectionOn();
	mainwnd->GetPlanner()->SetInterpolationToCublic();

	/// Adjust camera view up to align xyz axis with camera view angle
	// identify base image first
	Image* baseImage = mainwnd->GetPlanner()->GetImageByUniqueName("Image");
	if (!baseImage)
		// return if nothing found
		return;

	// resume additional transform
	// BaseImage must has no additional transform
	if (!this->CheckIdentityMatrix(this->m_additionalTransformHolder->GetMatrix())) {
		baseImage->GetTransform()->Concatenate(this->m_additionalTransformHolder->GetMatrix());
		this->m_additionalTransformHolder->Identity();
		//for (int i = 0; i < mainwnd->GetPlanner()->GetImageList()->size(); i++)
		//{
		//	Image* l_im = mainwnd->GetPlanner()->GetImageList()->at(i);
		//	if (l_im->GetUniqueName() == baseImage->GetUniqueName()) {
		//		continue;
		//	}
		//	else {
		//		// apply inverse additional transform to every other images to align them
		//		l_im->GetTransform()->Concatenate(invAdditionalTrans);
		//	}

		//}
	}


	if (m_viewers[this->m_orientation]) {
		// Get reslice matrix
		vtkSmartPointer<vtkMatrix4x4> directionMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
		directionMatrix->DeepCopy(baseImage->GetReslicer()->GetResliceTransform()->GetMatrix());
		directionMatrix->SetElement(0, 3, 0);
		directionMatrix->SetElement(1, 3, 0);
		directionMatrix->SetElement(2, 3, 0);

		// Recalculate the camera position but keeping the distance to the focal point unchanged
		vtkSmartPointer<vtkCamera> camera = this->m_viewers[m_orientation]->GetDataRenderer()->GetActiveCamera();
		double* cameraPos = camera->GetPosition();
		double* cameraFocal = camera->GetFocalPoint();
		double posVector[4] = { 0,0,0,1 };
		vtkMath::Subtract(cameraPos, cameraFocal, posVector);
		double cameraDistance = vtkMath::Norm(posVector);

		double planeNormal[4] = { 0,0,0,1 };
		memcpy(planeNormal, baseImage->GetReslicer()->GetReslicePlane(this->m_orientation)->GetNormal(), sizeof(double) * 3);
		memcpy(planeNormal, baseImage->GetReslicer()->GetObliqueTransform()->GetMatrix()->MultiplyDoublePoint(planeNormal), sizeof(double) * 3);
		vtkMath::Normalize(planeNormal);
		vtkMath::MultiplyScalar(planeNormal, cameraDistance);
		vtkMath::Add(cameraFocal, planeNormal, planeNormal);
		camera->SetPosition(planeNormal);
		

	}

	// apply the new view up
	switch (m_orientation)
	{
	case 0:
		this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->GetActiveCamera()->SetViewUp(0, 0, 1);
		break;
	case 1:
		this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->GetActiveCamera()->SetViewUp(0, 0, 1);
		break;
	case 2:
		this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->GetActiveCamera()->SetViewUp(0, -1, 0);
		break;
	default:
		break;
	}
}

void InteractorStyleSwitch::ImageDirectionOff()
{
	MainWindow* mainwnd = MainWindow::GetMainWindow();

	// Call direction off from planner
	mainwnd->GetPlanner()->ImageDirectionOff();
	mainwnd->GetPlanner()->SetInterpolationToNearest();

	/// Adjust camera view up to align xyz axis with camera view angle
	// identify base image first
	Image* baseImage = mainwnd->GetPlanner()->GetImageByUniqueName("Image");
	if (!baseImage)
		// return if nothing found
		return;

	// BaseImage must has no additional transform
	if (!this->CheckIdentityMatrix(baseImage->GetTransform()->GetMatrix())) {
		vtkSmartPointer<vtkTransform> invAdditionalTrans = vtkSmartPointer<vtkTransform>::New();
		invAdditionalTrans->GetMatrix()->DeepCopy(baseImage->GetTransform()->GetMatrix());
		invAdditionalTrans->GetMatrix()->Invert();
		
		m_additionalTransformHolder->GetMatrix()->DeepCopy(baseImage->GetTransform()->GetMatrix());
		baseImage->GetTransform()->GetMatrix()->Identity();

		for (int i = 0; i < mainwnd->GetPlanner()->GetImageList()->size(); i++)
		{
			Image* l_im = mainwnd->GetPlanner()->GetImageList()->at(i);
			if (l_im->GetUniqueName() == baseImage->GetUniqueName()) {
				continue;
			}
			else {
				// apply inverse additional transform to every other images to align them
				l_im->GetTransform()->Concatenate(invAdditionalTrans);
			}

		}
	}


	// apply the new view up
	if (m_viewers[this->m_orientation]) {
		// Get reslice matrix
		vtkSmartPointer<vtkMatrix4x4> directionMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
		directionMatrix->DeepCopy(baseImage->GetReslicer()->GetResliceTransform()->GetMatrix());
		directionMatrix->SetElement(0, 3, 0);
		directionMatrix->SetElement(1, 3, 0);
		directionMatrix->SetElement(2, 3, 0);

		// Recalculate the camera position but keeping the distance to the focal point unchanged
		vtkSmartPointer<vtkCamera> camera = this->m_viewers[m_orientation]->GetDataRenderer()->GetActiveCamera();
		double* cameraPos = camera->GetPosition();
		double* cameraFocal = camera->GetFocalPoint();
		double posVector[4] = { 0,0,0,1 };
		vtkMath::Subtract(cameraPos, cameraFocal, posVector);
		double cameraDistance = vtkMath::Norm(posVector);

		double planeNormal[4] = { 0,0,0,1 };
		memcpy(planeNormal, baseImage->GetReslicer()->GetReslicePlane(this->m_orientation)->GetNormal(), sizeof(double) * 3);
		memcpy(planeNormal, baseImage->GetReslicer()->GetObliqueTransform()->GetMatrix()->MultiplyDoublePoint(planeNormal), sizeof(double) * 3);
		vtkMath::Normalize(planeNormal);
		vtkMath::MultiplyScalar(planeNormal, cameraDistance);
		vtkMath::Add(cameraFocal, planeNormal, planeNormal);
		camera->SetPosition(planeNormal);

		// Transform original xyz axis to image space
		double y[4] = { 0,-1,0,1 }, z[4] = { 0,0,1,1 };
		memcpy(y, directionMatrix->MultiplyDoublePoint(y), sizeof(double) * 4);
		memcpy(z, directionMatrix->MultiplyDoublePoint(z), sizeof(double) * 4);

		switch (m_orientation)
		{
		case 0:
			m_viewers[m_orientation]->GetDataRenderer()->GetActiveCamera()->SetViewUp(z);
			break;
		case 1:
			m_viewers[m_orientation]->GetDataRenderer()->GetActiveCamera()->SetViewUp(z);
			break;
		case 2:
			m_viewers[m_orientation]->GetDataRenderer()->GetActiveCamera()->SetViewUp(y);
			break;
		default:
			break;
		}
	}
}

void InteractorStyleSwitch::InternalUpdate()
{
	MainWindow* mainwnd = MainWindow::GetMainWindow();

	if (this->CurrentStyle != this->PolygonDraw) {
		this->PolygonDraw->SetPolygonModeEnabled(false);
	}

	if (this->CurrentStyle == this->PolygonDraw || this->CurrentStyle == this->PaintBrush) {
		this->ImageDirectionOff();
	}
	else {
		this->ImageDirectionOn();
	}

	if (this->m_viewers[this->m_orientation]->GetRenderWindow()) {
		this->m_viewers[this->m_orientation]->Render();
	}

	if (this->CurrentStyle != this->PaintBrush)
		this->PaintBrush->SetPaintBrushModeEnabled(false);
}

void InteractorStyleSwitch::SetAutoAdjustCameraClippingRange(int value)
{
	if (value == this->AutoAdjustCameraClippingRange)
	{
		return;
	}

	if (value < 0 || value > 1)
	{
		vtkErrorMacro("Value must be between 0 and 1 for" <<
			" SetAutoAdjustCameraClippingRange");
		return;
	}

	this->AutoAdjustCameraClippingRange = value;
	this->Navigation->SetAutoAdjustCameraClippingRange(value);
	this->WindowLevel->SetAutoAdjustCameraClippingRange(value);
	//this->Transform->SetAutoAdjustCameraClippingRange(value);
	this->PolygonDraw->SetAutoAdjustCameraClippingRange(value);
	this->PaintBrush->SetAutoAdjustCameraClippingRange(value);
	this->Modified();
}

bool InteractorStyleSwitch::CheckIdentityMatrix(vtkMatrix4x4 *mat)
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (i == j) {
				if (mat->GetElement(i, j) != 1.)
					return false;
			}
			else {
				if (mat->GetElement(i, j) != 0.) {
					return false;
				}
			}
		}
	}
	return true;
}

void InteractorStyleSwitch::SetDefaultRenderer(vtkRenderer* renderer)
{
	this->vtkInteractorStyle::SetDefaultRenderer(renderer);
	this->Navigation->SetDefaultRenderer(renderer);
	this->WindowLevel->SetDefaultRenderer(renderer);
	this->WindowLevel->SetDefaultRenderer(renderer);
	this->PolygonDraw->SetDefaultRenderer(renderer);
	this->PaintBrush->SetDefaultRenderer(renderer);
}

void InteractorStyleSwitch::SetCurrentRenderer(vtkRenderer* renderer)
{
	this->vtkInteractorStyle::SetCurrentRenderer(renderer);
	this->Navigation->SetCurrentRenderer(renderer);
	this->WindowLevel->SetCurrentRenderer(renderer);
	//this->Transform->SetCurrentRenderer(renderer);
	this->PolygonDraw->SetCurrentRenderer(renderer);
	this->PaintBrush->SetCurrentRenderer(renderer);
}

void InteractorStyleSwitch::SetViewers(MyViewer **viewers)
{
	this->Navigation->SetViewers(viewers);
	this->WindowLevel->SetViewers(viewers);
	//this->Transform->SetViewers(viewers);
	this->PolygonDraw->SetViewers(viewers);
	this->PaintBrush->SetViewers(viewers);
	AbstractInteractorStyle::SetViewers(viewers);
}

void InteractorStyleSwitch::SetOrientation(int orientation)
{
	this->Navigation->SetOrientation(orientation);
	this->WindowLevel->SetOrientation(orientation);
	//this->Transform->SetOrientation(orientation);
	this->PolygonDraw->SetOrientation(orientation);
	this->PaintBrush->SetOrientation(orientation);
	AbstractInteractorStyle::SetOrientation(orientation);
}

void InteractorStyleSwitch::SetInteractor(vtkRenderWindowInteractor *iren)
{
	if (iren == this->Interactor)
	{
		return;
	}
	// if we already have an Interactor then stop observing it
	if (this->Interactor)
	{
		this->Interactor->RemoveObserver(this->EventCallbackCommand);
	}
	this->Interactor = iren;
	// add observers for each of the events handled in ProcessEvents
	if (iren)
	{
		iren->AddObserver(vtkCommand::CharEvent,
			this->EventCallbackCommand,
			this->Priority);

		iren->AddObserver(vtkCommand::DeleteEvent,
			this->EventCallbackCommand,
			this->Priority);
	}
}