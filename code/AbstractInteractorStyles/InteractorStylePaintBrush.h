/*
Author:		Wong, Matthew Lun
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
Department of Imaging and Inteventional Radiology,
Junior Research Assistant

Author:		Lok, Ka Hei Jason
Date:		16th, June 2016
Occupation:	Chinese University of Hong Kong,
Department of Imaging and Inteventional Radiology,
M.Phil Student

This class allows interactive segmentation on images.

Wong Matthew Lun, Lok Ka Hei
Copyright (C) 2016
*/

#ifndef INTERACTOR_STYLE_PAINTBRUSH_H
#define INTERACTOR_STYLE_PAINTBRUSH_H

#include <vtkInteractorStyleImage.h>
#include <vtkRenderWindowInteractor.h>
#include "AbstractInteractorStyleImage.h"
#include "MainWindow.h"

//Extra
#include <QtWidgets/QComboBox>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QCheckBox>
#include "vtkImageCanvasSource2D.h"
#include "vtkBorderWidget.h"
#include "vtkProperty2D.h"
#include "vtkBorderRepresentation.h"
#include "../data/vtkCircleBorderRepresentation.h"

// Our codes
#include "MyViewer.h"
//#include "ImageSegmentationWidget.h"

class MainWindow;

class InteractorStylePaintBrush : public AbstractInteractorStyleImage
{
public:
	vtkTypeMacro(InteractorStylePaintBrush, AbstractInteractorStyleImage);
	static InteractorStylePaintBrush* New();
	void SetPaintBrushModeEnabled(bool b);
	void SetDrawColor(int r, int g, int b);
	void SetDrawColor(int* rgb);
	void SetDrawOpacity(int opacity);
	

protected:
	InteractorStylePaintBrush();
	~InteractorStylePaintBrush();

	virtual void OnLeave();
	virtual void OnMouseMove();
	virtual void OnLeftButtonUp();
	virtual void OnLeftButtonDown();
	virtual void OnRightButtonDown();
	virtual void OnRightButtonUp();

private:
	void DrawCircle(int x0, int y0, int x1, int y1, double radius);
	void DrawLine3D(int x0, int y0, int x1, int y1);
	void FillBox3D();
	void Draw(bool b);
	void UpdateBorderWidgetPosition();
	void Write2ImageData();
	void ReadfromImageData();
	void Render();

	bool CheckValidPick(double*);

	ImageType::Pointer m_overlay;
	vtkImageData* m_imageData;
	vtkImageActor* m_imageActor;

	BrushProperty					m_brushProperty;
	vtkBorderWidget*				m_borderWidget;
	vtkBorderRepresentation*		m_retangleRep;
	vtkCircleBorderRepresentation*	m_circleRep;
	vtkImageCanvasSource2D* m_brush;
	vtkImageActor* m_brushActor;

	double m_origin[3];
	double m_spacing[3];
	int m_extent[6];
	int m_draw_index_old[3];
	int m_color_r, m_color_g, m_color_b;
	int m_opacity;
	bool m_isDraw;
	bool PAINT_BRUSH_MODE_ENABLED;
};


#endif // INTERACTOR_STYLE_PAINTBRUSH_H