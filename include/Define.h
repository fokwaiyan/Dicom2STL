#ifndef DEFINE_H
#define DEFINE_H

//Preprocessor definition
//_CRT_SECURE_NO_WARNINGS
//_SCL_SECURE_NO_WARNINGS

//QT
#include <QString>

//ITK
#include "itkImage.h"
#include "itkImageFileWriter.h"
#include "itkSmartPointer.h"
#include "itkGDCMSeriesFileNames.h"
#include "itkImageSeriesReader.h"
#include "itkGDCMImageIO.h"
#include "itkNiftiImageIO.h"
#include "itkImageToVTKImageFilter.h"
#include "itkOrientImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageDuplicator.h"
#include "itkCastImageFilter.h"
#include "itkChangeInformationImageFilter.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkFlipImageFilter.h"

typedef float										 PixelType;
const static unsigned int							 ImageDimension = 3;
typedef itk::Image< PixelType, ImageDimension >		 ImageType;		
typedef itk::GDCMSeriesFileNames					 NamesGeneratorType;
typedef itk::ImageSeriesReader< ImageType >			 ReaderType;
typedef itk::ImageFileReader<ImageType>				 NiftiReaderType;
typedef itk::GDCMImageIO							 ImageIOType;
typedef itk::ImageToVTKImageFilter<ImageType>		 ConnectorType;
typedef itk::OrientImageFilter<ImageType,ImageType>  OrienterType;
typedef itk::ImageFileWriter<ImageType>				 NiftiWriterType;
typedef itk::NiftiImageIO							 NiftiImageIOType;
typedef itk::ImageDuplicator< ImageType >			 DuplicatorType;
typedef itk::CastImageFilter< ImageType, ImageType>  CastFilterType;
typedef itk::ChangeInformationImageFilter<ImageType> ChangeImageFilterType;
typedef itk::IntensityWindowingImageFilter<ImageType>IntensityWindowFilter;
typedef itk::MinimumMaximumImageCalculator<ImageType>MaxMinCaluclatorFilter;

//VTK
#include "vtkSmartPointer.h"
#include "vtkImageData.h"
#include "vtkImageProperty.h"
#include "vtkImageReslice.h"
#include "vtkImageActor.h"
#include "vtkMatrix4x4.h"
#include "vtkPolyData.h"
#include "vtkLookupTable.h"
#include "vtkTransform.h"
#include "vtkTransformFilter.h"

#include "ImageColorMap.h"

struct PatientInfo
{
	QString name;
	QString sex;
	QString age;
	QString id;
	QString description;
	double	referenceDose;
};

struct BrushProperty
{
	int		labelID;
	int		shape;
	double	size;
	bool	isVolumetric;
};


#endif //DEFINE_H