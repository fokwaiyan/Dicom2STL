
cmake_minimum_required(VERSION 2.8)
 
if(POLICY CMP0020)
  cmake_policy(SET CMP0020 NEW)
endif()

PROJECT(MeshGen)

#include VMTK
find_package(VMTK REQUIRED)
include(${VMTK_USE_FILE})

#include ITK
find_package(ITK REQUIRED)
include(${ITK_USE_FILE})

#include VTK
find_package(VTK REQUIRED)
include(${VTK_USE_FILE})

if(${VTK_VERSION} VERSION_GREATER "6" AND VTK_QT_VERSION VERSION_GREATER "4")
  #Instruct CMake to run moc automatically when needed.
  set(CMAKE_AUTOMOC ON)
  find_package(Qt5Widgets REQUIRED QUIET)
else()
  find_package(Qt4 REQUIRED)
  include(${QT_USE_FILE})
endif()

#Subdirectory
add_subdirectory(${PROJECT_SOURCE_DIR}/code/AbstractData)
add_subdirectory(${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles)
add_subdirectory(${PROJECT_SOURCE_DIR}/code/utilities)
add_subdirectory(${PROJECT_SOURCE_DIR}/code/AbstractFilter)
add_subdirectory(${PROJECT_SOURCE_DIR}/code/AbstractWidget)
add_subdirectory(${PROJECT_SOURCE_DIR}/code/Widget)
add_subdirectory(${PROJECT_SOURCE_DIR}/code/EncryptionAuthentication)

file(GLOB UI_FILES ${PROJECT_SOURCE_DIR}/code/Core/*.ui)
file(GLOB QT_WRAP *.h)
file(GLOB CXX_FILES *.cxx)

include_directories(
	${PROJECT_SOURCE_DIR}/include
	${PROJECT_SOURCE_DIR}/build
	${PROJECT_SOURCE_DIR}/code/Core
	${PROJECT_SOURCE_DIR}/code/filters
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles
	${PROJECT_SOURCE_DIR}/code/AbstractWidget
	${PROJECT_SOURCE_DIR}/code/data
	${PROJECT_SOURCE_DIR}/lib/AbstractData/include
)

link_directories(
	${PROJECT_SOURCE_DIR}/lib 
	${PROJECT_SOURCE_DIR}/lib/AbstractData
	)

SET(MAIN_INCLUDE_DIRECTORIES
  ${PROJECT_SOURCE_DIR}/build
  ${PROJECT_SOURCE_DIR}/include
  ${PROJECT_SOURCE_DIR}/include/AbstractData
  ${PROJECT_SOURCE_DIR}/code/Core
  ${PROJECT_SOURCE_DIR}/code/data
  ${PROJECT_SOURCE_DIR}/code/filters
  ${PROJECT_SOURCE_DIR}/code/EncryptionAuthentication


  # Directories of Utiltities
  ${PROJECT_SOURCE_DIR}/code/utilities

  #Widgets 
  ${PROJECT_SOURCE_DIR}/code/Widget/Clipping

  # Directories of Abstract headers
  ${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles
  ${PROJECT_SOURCE_DIR}/code/AbstractData
  ${PROJECT_SOURCE_DIR}/code/AbstractWidget
  ${PROJECT_SOURCE_DIR}/code/AbstractFilter
  
  )

include_directories(  
  ${MAIN_INCLUDE_DIRECTORIES}
)
SET( Qt_Filter_INC
	${PROJECT_SOURCE_DIR}/code/filters/SurfaceCreator.h
	${PROJECT_SOURCE_DIR}/code/filters/AbstractFilter.h	
	${PROJECT_SOURCE_DIR}/code/filters/MeshToImage.h
	${PROJECT_SOURCE_DIR}/code/filters/vtkPolyDataToitkMesh.h
	${PROJECT_SOURCE_DIR}/code/data/vtkEllipsoidSource.h
	${PROJECT_SOURCE_DIR}/code/data/vtkCircleBorderRepresentation.h
	)

SET( Qt_Filter_SRC
	${PROJECT_SOURCE_DIR}/code/filters/SurfaceCreator.cxx
	${PROJECT_SOURCE_DIR}/code/filters/AbstractFilter.cxx
	${PROJECT_SOURCE_DIR}/code/filters/MeshToImage.cxx
	${PROJECT_SOURCE_DIR}/code/filters/vtkPolyDataToitkMesh.cxx
	${PROJECT_SOURCE_DIR}/code/data/vtkEllipsoidSource.cxx
	${PROJECT_SOURCE_DIR}/code/data/vtkCircleBorderRepresentation.cxx
	)

SET(PROJECT_INC
	  ${PROJECT_SOURCE_DIR}/code/Core/MainWindow.h
	  ${PROJECT_SOURCE_DIR}/code/Core/MyViewer.h
	  ${PROJECT_SOURCE_DIR}/code/Core/Planner.h
)

SET(PROJECT_SRC
	  ${PROJECT_SOURCE_DIR}/code/Core/main.cxx
	  ${PROJECT_SOURCE_DIR}/code/Core/MainWindow.cxx
	  ${PROJECT_SOURCE_DIR}/code/Core/MyViewer.cxx
	  ${PROJECT_SOURCE_DIR}/code/Core/Planner.cxx
	)

SET(INTERACTORS_INC
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStyle3DNavigation.h
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStyle3DTrackBallCamera.h
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStyleNavigation.h
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStylePaintBrush.h
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStylePolygonDraw.h
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStyleSwitch.h
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStyleSwitch3D.h
	#${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStyleTransform.h
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStyleWindowLevel.h
)

SET(INTERACTORS_SRC
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStyle3DNavigation.cxx
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStyle3DTrackBallCamera.cxx
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStyleNavigation.cxx
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStylePaintBrush.cxx
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStylePolygonDraw.cxx
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStyleSwitch.cxx
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStyleSwitch3D.cxx
	#${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStyleTransform.cxx
	${PROJECT_SOURCE_DIR}/code/AbstractInteractorStyles/InteractorStyleWindowLevel.cxx
)


set(QT_WRAP 
      ${QT_INC}
      ${CORE_INC}
      ${QT_RCS}
      ${Qt_Filter_INC}
      ${Qt_Data_INC}
      ${INTERACTORS_INC}
      )

SET(QT_RSRS  ${PROJECT_SOURCE_DIR}/resources/MeshGen.qrc)
#SET(QT_RCS   ${PROJECT_SOURCE_DIR}/resources/MeshGen.rc)

if(${VTK_VERSION} VERSION_GREATER "6" AND VTK_QT_VERSION VERSION_GREATER "4")
  qt5_wrap_ui(UISrcs ${UI_FILES} )

qt5_add_resources(RESOURCES_ADDED ${QT_RSRS})

# CMAKE_AUTOMOC in ON so the MocHdrs will be automatically wrapped.
  add_executable(MeshGen 
		MACOSX_BUNDLE 
		${PROJECT_SRC} 
		${PROJECT_INC}
		${UISrcs}
		${UI_FILES}
		${QT_WRAP}
		${Qt_Filter_SRC}
		${Qt_Filter_INC}
		${INTERACTORS_SRC}
		${INTERACTORS_INC}
		${RESOURCES_ADDED}
		${QT_RSRS}
		)
  qt5_use_modules(MeshGen 
		Core 
		Gui
		Widgets
		Xml
		)
target_link_libraries(MeshGen 
    					optimized AbstractFilter
    					optimized AbstractInteractorStyles
						optimized AbstractData
						optimized AbstractWidget
						optimized Utitlities
						Widget_Clipping
						EncryptionAuthentication
						${VTK_LIBRARIES} 
						${ITK_LIBRARIES}
						${VMTK_LIBRARIES}
		)

target_link_libraries(MeshGen
						debug AbstractData
    					debug AbstractFilter
    					debug AbstractInteractorStyles
    					debug AbstractWidget
						debug Utitlities
						Widget_Clipping
						EncryptionAuthentication
						${VTK_LIBRARIES} 
						${ITK_LIBRARIES}
						${VMTK_LIBRARIES}
		)
else()

set(QT_WRAP 
      ${QT_INC}
      ${CORE_INC}
      ${QT_RCS}
      ${Qt_Filter_INC}
      ${Qt_Data_INC}
      ${INTERACTORS_INC}
      )

  QT4_WRAP_UI(UISrcs ${UI_FILES})
  QT4_WRAP_CPP(MOCSrcs ${QT_WRAP})

  #add_executable(MarchingCubesGUI MACOSX_BUNDLE ${CXX_FILES} ${UISrcs} ${MOCSrcs})
  add_executable(MeshGen 
		MACOSX_BUNDLE 
		${PROJECT_SRC} 
		${PROJECT_INC}
		${Qt_Filter_INC}
		${Qt_Filter_SRC}
		${QT_WRAP}
		${UISrcs}
		${UI_FILES}
		${MOCSrcs}
		)
endif()

 if(VTK_LIBRARIES)
    if(${VTK_VERSION} VERSION_LESS "6")
      target_link_libraries(MeshGen ${VTK_LIBRARIES} ${ITK_LIBRARIES} ${QT_LIBRARIES} ${VMTK_LIBRARIES} AbstractData)
    else()
      target_link_libraries(MeshGen ${VTK_LIBRARIES} ${ITK_LIBRARIES} ${QT_LIBRARIES} ${VMTK_LIBRARIES} AbstractData)
    endif()
  else()
    target_link_libraries(MeshGen vtkHybrid QVTK vtkViews ${QT_LIBRARIES} AbstractData)
  endif()

set_target_properties(MeshGen PROPERTIES LINKER_LANGUAGE CXX)


