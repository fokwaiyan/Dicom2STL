/*
Author:		Wong, Matthew Lun
Date:		26th, May 2016
Occupation:	Chinese University of Hong Kong,
Department of Imaging and Inteventional Radiology,
Junior Research Assistant


The abstract data class is designed to hold data structure typically used in a medical viewer.
This class is parent of all image-related data.


Wong Matthew Lun
Copyright (C) 2016
*/

#ifndef ABSTRACT_IMAGE
#define ABSTRACT_IMAGE

#include <QStringList>
#include <itkExtractImageFilter.h>
#include <vtkSmartPointer.h>
#include <vtkMatrix4x4.h>
#include "Define.h"
#include "AbstractData.h"
#include "itkvtkConvert.hxx"

class AbstractImage : public AbstractData
{
public:
	AbstractImage();
	~AbstractImage();

	virtual void SetImage(QString niiFilePath);
	virtual void SetImage(QStringList* dcmFileList);
	virtual void SetOpacity(double opacity);
	virtual void SetVisibility(bool b);
	virtual void SetPosition(double* pos, int anchor = -1);
	virtual void SaveImageAsNifti(QString niiPath);
	virtual void SaveImageAsDicom(QString dir, PatientInfo info);

	virtual vtkProp*		GetImageViewProp(int n);
	vtkMatrix4x4*		GetMatrix();
	vtkMatrix4x4*		GetInverseMatrix();
	vtkTransform*		GetTransform();
	vtkTransform*		GetObliqueTransform();
	void				SetMatrix(vtkMatrix4x4* matrix);

	int				GetImageType();
	void			GetPosition(double* pos);
	double*			GetPosition();
	double*			GetSpacing();
	double			GetOpacity();
	bool			GetVisibility();
	itkvtkConvert<ImageType>::Pointer GetReslicer();

	//virtual void UpdateImage();

	enum IMAGE_TYPE {
		NONE_TYPE = 0, 
		LABEL_MAP = 1, 
		FLOAT_IMAGE = 2,
		DOUBLE_IMAGE = 3
	};

	enum ABSTRACT_IMAGE_ERROR {
		IMAGE_TYPE_ERROR = 1,
		INPUT_FILE_PATH_ERROR = 2
	};

protected:
	// Call after m_itkImage is prepared
	virtual void InstallPipeline();

	int m_imageType;
	ImageType::Pointer					m_itkImage;
	itkvtkConvert<ImageType>::Pointer	m_i2vConverter;
	vtkImageSlice*						m_viewProp[3];

	template< typename TImage > void ReadNiiImage(QString		niiFilePath);
	template< typename TImage > void ReadDcmImage(QStringList* dcmFileList);
};

#endif