#include "itkMesh.h"
#include "itkMeshFileReader.h"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkCastImageFilter.h"
#include "itkTriangleMeshToBinaryImageFilter.h"

int main(int argc, char* argv[])
{
	//Input Parameter
	int value = 2;
	const char * inputImageName = "C:/Users/Sam/Desktop/VesselSim/Project/test_data/CTA_Output/Label.nii.gz";
	const char * inputMeshName = "C:/Users/Sam/Desktop/VesselSim/Project/test_data/CTA-surface/right_clipped_triangulated.vtk";
	const char * outputImageName = "C:/Users/Sam/Desktop/VesselSim/Project/test_data/CTA_Output/Label_C.nii.gz";
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	const unsigned int Dimension = 3;
	typedef float MeshPixelType;

	typedef itk::Mesh< MeshPixelType, Dimension > MeshType;

	typedef itk::MeshFileReader< MeshType >  MeshReaderType;
	MeshReaderType::Pointer meshReader = MeshReaderType::New();
	meshReader->SetFileName(inputMeshName);

	typedef float			                        InputPixelType;
	typedef itk::Image< InputPixelType, Dimension > InputImageType;
	typedef itk::ImageFileReader< InputImageType >  ImageReaderType;

	ImageReaderType::Pointer imageReader = ImageReaderType::New();
	imageReader->SetFileName(inputImageName);

	typedef float			                          OutputPixelType;
	typedef itk::Image< OutputPixelType, Dimension >  OutputImageType;

	typedef itk::CastImageFilter< InputImageType, OutputImageType > CastFilterType;
	CastFilterType::Pointer cast = CastFilterType::New();
	cast->SetInput(imageReader->GetOutput());

	typedef itk::TriangleMeshToBinaryImageFilter< MeshType, OutputImageType > FilterType;
	FilterType::Pointer filter = FilterType::New();
	filter->SetInput(meshReader->GetOutput());
	filter->SetInfoImage(cast->GetOutput());
	filter->SetInsideValue(value);
	try
	{
		filter->Update();
	}
	catch (itk::ExceptionObject & error)
	{
		std::cerr << "Error: " << error << std::endl;
		return EXIT_FAILURE;
	}

	typedef itk::ImageFileWriter< OutputImageType > WriterType;
	WriterType::Pointer writer = WriterType::New();
	writer->SetFileName(outputImageName);
	writer->SetInput(filter->GetOutput());
	try
	{
		writer->Update();
	}
	catch (itk::ExceptionObject & error)
	{
		std::cerr << "Error: " << error << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}